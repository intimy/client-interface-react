# Changelog
All notable changes to this project will be documented in this file.

## 0.0.1 - 2017-06-10
### Added
- Compass scss.
- Sign up in the popup log in.
- Forgot password in the popup log in.
- Home page.
- Post page to post an item to sell.
- Item page.
- Profil page.
- Contact page.
- Terms and conditions.
- 404 page.
- Header.
- Footer.
- PropTypes.

### Changed
- Primary color.