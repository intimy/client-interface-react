//React
import React, { Component } from 'react'

class Faq extends Component {

    componentDidMount() {
        document.title = "FAQ - intimy.shop"
    }

    render() {
        return (
            <div className="content">
                <div className="faq-container">
                    <h2 className="profile-items-title">FAQ</h2>
                    <h3 className="faq-question">How to start selling?</h3>
                    <p className="faq-content">It’s pretty easy to start selling on intimy.shop. Click on “log in”, click on “sign up”, then  “I want to sell”, fill the fields. To ensure the quality of the platform we will ask an ID verification to prove that is really you. That’s it! You are ready to sell!</p>
                    <h3 className="faq-question">How to start buying?</h3>
                    <p className="faq-content">It’s pretty easy to start buying on intimy.shop. Click on “log in”, click on “sign up”, then  “I want to buy”, fill the fields. That’s it! You are ready to buy!</p>
                    <h3 className="faq-question">What can I sell on Intimy?</h3>
                    <p className="faq-content">
                        You can actually sell any wearable items, like panties, bras, dildos, socks.<br />
                        We actually offer one kind of package, padded envelope Size: 9-1/2 in x 12-1/2. Make sure it can fit. We will add more options soon.
                    </p>
                    <h3 className="faq-question">From what countries can I sell?</h3>
                    <p className="faq-content">
                        You can actually sell from USA.<br />
                        We will add more countries soon.<br />
                        Peoples can buy from any countries.<br />
                    </p>
                </div>
            </div>
        )
    }
}

export default Faq