//React
import React, { Component } from 'react'
//Router
import { Link } from 'react-router-dom'

//Components
import axios from 'axios'
import PopUpHeaderMenuConnectedUser from './PopUp/PopUpHeaderMenuConnectedUser'
import PopUpHeaderMenuConnectedUserMobile from './PopUp/PopUpHeaderMenuConnectedUserMobile'
import Notifications from './Notifications'
import UserConnexionLoader from './UserConnectionLoader'
import PopUpPostItem from './PopUp/PopUpPostItem'
import SearchFormMobile from './SearchFormMobile'
import PopUpSuccessItemPosted from './PopUp/PopUpSuccessItemPosted'

class HeaderConnected extends Component {

  constructor(props) {
    super(props)
    this.state = {
      menuConnectedUser: false,
      showNotifs: false,
      notifsNumber: 0,
      username: "",
      bagNumber: 0,
      tokens: 0,
      profilPicture: "",
      verifiedUser: "",
      showMobileMenu: false,
      showMobileSearch: false,
      showPostItem: false,
      successPopUp: false
    }
    this.self = React.createRef()
  }

  componentDidMount() {
    this.getUsername()
    this.getNotifsNumber()
    this.getitemsfrommyshoppingbag()
    this.props.socket.on('reconnect_attempt', () => {
      this.props.socket.io.opts.transports = ['websocket', 'polling']
    })
    this.props.socket.on('New notification shopping bag', (data) => {
      const bagNumber = this.state.bagNumber
      if (data == 'Add') {
        const newBagNumber = bagNumber + 1
        this.setState({
          bagNumber: newBagNumber
        })
      }
      if (data == 'Remove') {
        const newBagNumber = bagNumber - 1
        this.setState({
          bagNumber: newBagNumber
        })
      }
      if (data == 'Checkout') {
        this.setState({
          bagNumber: 0
        })
      }
    })
    this.props.socket.on('Tokens added', (data) => {
      const tokensQuantity = this.state.tokens
      let newTokensQuantity = tokensQuantity + data
      this.setState({
        tokens: newTokensQuantity
      })
    })
    this.props.socket.on('Tokens substract', (data) => {
      const tokensQuantity = this.state.tokens
      let newTokensQuantity = tokensQuantity - data
      this.setState({
        tokens: newTokensQuantity
      })
    })
    this.props.socket.on('Seller tokens added', (data) => {
      this.getTokens()
    })
    this.props.socket.on('New notification', (data) => {
      this.getNotifsNumber()
    })
  }

  getUsername() {
    axios.get(`${process.env.REACT_APP_URI}/api/getUsername`, { withCredentials: true })
      .then((response) => {
        if (response.data) {
          this.setState({
            username: response.data.username,
            tokens: response.data.tokens,
            profilPicture: response.data.profilPicture,
            verifiedUser: response.data.verifiedUser
          })
        }
        else {
          const { cookies } = this.props
          cookies.remove('authtoken', { path: '/' })
          window.location.reload()
        }
      })
  }

  getTokens = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getTokens`, { withCredentials: true })
      .then((response) => {
        if (response.data) {
          this.setState({
            tokens: response.data.tokens
          })
        }
      })
  }

  getNotifsNumber = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getnotifsnumber`, { withCredentials: true })
      .then((response) => {
        if (response.data) {
          this.setState({
            notifsNumber: response.data.notifsNumber
          })
        }
      })
  }

  getitemsfrommyshoppingbag() {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemsfrommyshoppingbag`, { withCredentials: true })
      .then((response) => {
        this.setState({
          bagNumber: response.data.length
        })
      })
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => {
    this.setState({
      menuConnectedUser: false,
      showNotifs: false,
      showMobileMenu: false,
      showMobileSearch: false
    })
  }

  toggleMenuConnectedUser = (e) => {
    this.setState({
      menuConnectedUser: !this.state.menuConnectedUser
    })
  }

  notificationsDropdown = e => {
    e.stopPropagation()
    e.preventDefault()
    this.setState({
      showNotifs: !this.state.showNotifs
    })
  }

  notificationsDropdown2 = e => {
    this.setState({
      showNotifs: !this.state.showNotifs
    })
  }

  showMobileMenu = e => {
    if (!this.state.showMobileMenu) document.body.classList.add('noScrollBody')
    else document.body.classList.remove('noScrollBody')
    this.setState({
      showMobileMenu: !this.state.showMobileMenu
    })
  }

  showMobileSearch = e => {
    this.setState({
      showMobileSearch: !this.state.showMobileSearch
    }, () => {
      this.props.mobileSearchOn(this.state.showMobileSearch)
    })
  }

  togglePostItem = (content) => {
    this.setState({ showPostItem: content })
  }

  closePopUpPostItem = () => {
    this.setState({ showPostItem: false })
  }

  successPopUp = () => {
    this.setState({ successPopUp: true })
  }

  closeSuccessPopUp = () => {
    this.setState({ successPopUp: false })
  }

  render() {
    const { handleOutsideClick } = this.props
    return (
      <div>
        <div className="UserConnectedRightContainer">
          <div className="mobile-menu-icon"><i onClick={this.showMobileSearch} className="fas fa-search mobile-search-icon" ref={this.self}></i><i onClick={this.showMobileMenu} className="fas fa-bars"></i></div>
          <SearchFormMobile showSearch={this.state.showMobileSearch} showMobileSearch={this.showMobileSearch} self={this.self.current} />
          <div className="container-connected-menu-header">
            <Link to="/tokens" className="token-icon-header">Tokens<span className="tokens-number-header">{this.state.tokens}</span></Link>
            <Link to="/live" className="live-button-header">Live </Link>
            <span onClick={this.notificationsDropdown2} className="notifications-bubble">
              <i className="fas fa-bell notif-circle-header"></i>
              {this.state.notifsNumber > 0 && <span className="notification-number">{this.state.notifsNumber}</span>}
            </span>
            {this.state.showNotifs && <Notifications toggleDropdown={this.notificationsDropdown2} outsideClickIgnoreClass={'toggle2'} notifsNumber={this.state.notifsNumber} />}
            <Link to="/shoppingBag" className="container-shop-bag-link"><i className="fas fa-shopping-bag ShoppingBagIcon"></i>{this.state.bagNumber > 0 && <span className="notification-number-shopping-bag">{this.state.bagNumber}</span>}</Link>
            <span onClick={this.toggleMenuConnectedUser} className="UserNameConnected">{this.state.username} <span className='user-menu-chevron toggle' ><i className="fas fa-chevron-down fa-lg ChevronUserConnected"></i></span></span>
            {this.state.menuConnectedUser && <PopUpHeaderMenuConnectedUser verifiedUser={this.state.verifiedUser} togglePostItem={this.togglePostItem} outsideClickIgnoreClass={'toggle'} menuConnectedUser={this.toggleMenuConnectedUser} />}
          </div>
        </div>
        <PopUpHeaderMenuConnectedUserMobile verifiedUser={this.state.verifiedUser} togglePostItem={this.togglePostItem} profilPicture={this.state.profilPicture} show={this.state.showMobileMenu} showMobileMenu={this.showMobileMenu} notifsNumber={this.state.notifsNumber} bagNumber={this.state.bagNumber} />
        {this.state.showPostItem &&
          <PopUpPostItem userStatus={this.props.userStatus} showPostItem={this.state.showPostItem} closePopUpPostItem={this.closePopUpPostItem} successPopUp={this.successPopUp} />
        }
        {this.state.successPopUp && <PopUpSuccessItemPosted closeSuccessPopUp={this.closeSuccessPopUp} />}
      </div>
    )
  }
}

export default UserConnexionLoader(HeaderConnected)