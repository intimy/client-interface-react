import React, { Component } from 'react';


class Datas extends Component {
    render() {
        const content = this.props.data.map((post) =>
        <div key={post._id}>
          <h3>{post.name}</h3>
          <p>{post.img}</p>
        </div>
      );
          return (
            <div>

              {content}
            </div>
          );
    }
}
export default Datas