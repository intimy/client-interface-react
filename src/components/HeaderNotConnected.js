//React
import React from 'react'
//Router
import { withRouter, Link } from 'react-router-dom'

import PopUpLogIn from './PopUp/PopUpLogIn'
import onClickOutside from 'react-onclickoutside'
import axios from 'axios'

//Components
import PopUpHeaderMenuConnectedUser from './PopUp/PopUpHeaderMenuConnectedUser'
import SearchFormMobile from './SearchFormMobile'
import PopUpHeaderMenuNotConnectedUserMobile from './PopUp/PopUpHeaderMenuNotConnectedUserMobile'
import { observer } from 'mobx-react'
import ShoppingBagStore from "./stores/ShoppingBagStore"

class HeaderNotConnected extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      menuConnectionUser: false,
      bagNumber: 0,
      showMobileMenu: false,
      showMobileSearch: false
    }
    this.self = React.createRef()
  }

  componentDidMount() {
    if (ShoppingBagStore.shoppingBag.length > 0) this.getItemsFromMyShoppingBagNotConnected(ShoppingBagStore.shoppingBag)
  }

  getItemsFromMyShoppingBagNotConnected = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemsfrommyshoppingbagnotconnected`, { params: { itemsId: e } })
      .then((response) => {
        this.setState({
          bagNumber: response.data.length
        })
      })
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    menuConnectionUser: false
  })

  toggleMenuConnectionUser = (e) => {
    e.stopPropagation()
    e.preventDefault()
    this.setState({
      menuConnectionUser: !this.state.menuConnectionUser
    })
  }

  showMobileMenu = e => {
    if (!this.state.showMobileMenu) document.body.classList.add('noScrollBody')
    else document.body.classList.remove('noScrollBody')
    this.setState({
      showMobileMenu: !this.state.showMobileMenu
    })
  }

  showMobileSearch = e => {
    this.setState({
      showMobileSearch: !this.state.showMobileSearch
    }, () => {
      this.props.mobileSearchOn(this.state.showMobileSearch)
    })
  }

  render() {
    const { handleClickOutside } = this.props
    return (
      <div>
        <div className="UserConnectedRightContainer">
          <div className="mobile-menu-icon"><i onClick={this.showMobileSearch} className="fas fa-search mobile-search-icon" ref={this.self}></i><i onClick={this.showMobileMenu} className="fas fa-bars"></i></div>
          <SearchFormMobile showSearch={this.state.showMobileSearch} showMobileSearch={this.showMobileSearch} self={this.self.current} />
          <div className="container-connected-menu-header">
            <Link to="/shoppingBag" className="container-shop-bag-link"><i className="fas fa-shopping-bag ShoppingBagIcon"></i><span className="notification-number-shopping-bag">{ShoppingBagStore.shoppingBag.length > 0 && ShoppingBagStore.shoppingBag.length}</span></Link>
            <Link to="/tokens" className="token-icon-header">Tokens<span className="tokens-number-header"></span></Link>
            <Link to="/live" className="live-button-header">Live </Link>
            <a href="#" onClick={this.toggleMenuConnectionUser} className="toggle align-middle">Log in</a>
          </div>
          <PopUpHeaderMenuNotConnectedUserMobile toggleMenuConnectionUser={this.toggleMenuConnectionUser} ShoppingBagStore={ShoppingBagStore} show={this.state.showMobileMenu} showMobileMenu={this.showMobileMenu} bagNumber={this.state.bagNumber} />
        </div>
        {this.state.menuConnectionUser &&
              <PopUpLogIn handleClickOutside={this.handleClickOutside} outsideClickIgnoreClass={'toggle'} toggleMenuConnectionUser={this.toggleMenuConnectionUser} />
            }
      </div>
    )
  }
}

export default onClickOutside(observer(HeaderNotConnected))