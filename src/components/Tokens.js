import React from 'react'
//Components
import UserConnexionLoader from './UserConnectionLoader'
import axios from 'axios'
import PopUpLogIn from './PopUp/PopUpLogIn'
import PopUpSuccessTokens from './PopUp/PopUpSuccessTokens'
import LogoImg from '../images/intimy.jpg'
import PaymentsAccepted from '../images/payments.png'

class Tokens extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dataItems: [],
            totalPrice: 0,
            menuConnectedUser: false,
            radio: "990",
            description: "99 Tokens",
            showPopUpLogIn: false,
            popUpSuccessTokens: false
        }
    }

    componentDidMount() {
        document.title = "Tokens - intimy.shop"
        const script = document.createElement("script")
        script.src = "https://checkout.stripe.com/checkout.js"
        document.body.appendChild(script)
        const self = this

        if (this.props.connected) {
            this.handler = window.StripeCheckout.configure({
                key: 'pk_live_CzdSkm4GLNrzNc0hSzFttaG3',
                image: LogoImg,
                locale: 'auto',
                zipCode: true,
                token: function (token) {
                    axios.post(`${process.env.REACT_APP_URI}/api/charge`, { token: token.id, price: self.state.radio, description: self.state.description, email: token.email }, { withCredentials: true })
                        .then(response => {
                            if (response.statusText === "OK") {
                                self.setState({
                                    popUpSuccessTokens: true
                                })
                                window.gtag('event', `Tokens bought`, {
                                    'event_category': 'user',
                                    'event_label': `${self.state.radio}`
                                })
                            }
                        })
                }
            })

            document.getElementById('customButton').addEventListener('click', function (e) {
                // Open Checkout with further options:
                self.handler.open({
                    name: 'Intimy',
                    description: self.state.description,
                    amount: parseInt(self.state.radio)
                })
                e.preventDefault()
            })
        }
    }

    componentWillUnmount() {
        if (this.props.connected) {
            this.handler.close()
        }
    }

    handleRadio = (e, data) => {
        this.setState({
            radio: data.price,
            description: data.description
        })
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => this.setState({
        showPopUpLogIn: false
    })

    toggleMenuConnectionUser = (e) => {
        e.stopPropagation()
        e.preventDefault()

        this.setState({
            showPopUpLogIn: !this.state.showPopUpLogIn
        })
    }

    togglePopUpTokens = (e) => {
        e.stopPropagation()
        e.preventDefault()

        this.setState({
            popUpSuccessTokens: !this.state.popUpSuccessTokens
        })
    }

    clickOnPurchase = e => {
        if (this.props.connected) {
            window.gtag('event', `Purchase button click`, {
                'event_category': 'user',
                'event_label': 'Member'
            })
        }
        else {
            window.gtag('event', `Purchase button click`, {
                'event_category': 'user',
                'event_label': 'Guest'
            })
        }
    }

    render() {
        return <div className="content">
            <div className="buy-tokens-left">
                <h3 className="buy-token-title ShoppingBagTitle">Purchase tokens</h3>
                <p className="buy-tokens-title-2">Tokens are used to buy the items members are selling, which allows us to give you the best experience possible.</p>
                <p className="buy-tokens-title-3">We act as a third party between you and the sellers. Therefore, any problem occurs during the process, we could assist you on canceling the order or refunding if needed.</p>
            </div>
            <div className="select-a-pack-left">
                <h4>Select a pack:</h4>
            </div>
            <div className="tokens-container">
                <div className="tokens-container-left">
                    <div>
                        <div className="tokens-prices">
                            <div onClick={(e) => this.handleRadio(e, { price: "990", description: "99 Tokens" })} className={`tokens-packs${this.state.radio === "990" ? " active" : ""}`}><span className="tokens-quantity"> 99 Tokens</span><span className="tokens-price"> $9.90</span></div>
                            <div onClick={(e) => this.handleRadio(e, { price: "1990", description: "199 Tokens" })} className={`tokens-packs${this.state.radio === "1990" ? " active" : ""}`}><span className="token-quantity"> 199 Tokens</span><span className="tokens-price"> $19.90</span></div>
                            <div onClick={(e) => this.handleRadio(e, { price: "4990", description: "499 Tokens" })} className={`tokens-packs${this.state.radio === "4990" ? " active" : ""}`}><span className="token-quantity"> 499 Tokens</span><span className="tokens-price"> $49.90</span></div>
                            <div onClick={(e) => this.handleRadio(e, { price: "9990", description: "999 Tokens" })} className={`tokens-packs${this.state.radio === "9990" ? " active" : ""}`}><span className="token-quantity"> 999 Tokens</span><span className="tokens-price"> $99.90</span></div>
                            <div onClick={(e) => this.handleRadio(e, { price: "39990", description: "3999 Tokens" })} className={`tokens-packs${this.state.radio === "39990" ? " active" : ""}`}><span className="token-quantity"> 3999 Tokens</span><span className="tokens-price"> $399.90</span></div>
                        </div>
                        {this.props.connected ? <button id="customButton" className="customButton" onClick={this.clickOnPurchase}>Purchase</button>
                            :
                            <div>
                                <button onClick={this.toggleMenuConnectionUser} id="customButtonN" className="customButton">Purchase</button>
                                {this.state.showPopUpLogIn &&
                                    <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to buy tokens" />
                                }
                            </div>
                        }
                        {this.state.popUpSuccessTokens && <PopUpSuccessTokens togglePopUpTokens={this.togglePopUpTokens} />}
                    </div>
                </div>
                <div className="tokens-container-right">
                    <div className="payment-infos">
                        <img className="payments-accepted" src={PaymentsAccepted} alt="Payments accepted" />
                    </div>
                </div>
            </div>
        </div>
    }
}

export default UserConnexionLoader(Tokens)