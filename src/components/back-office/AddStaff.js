//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'

class AddStaff extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            formAddStaff: {
                username: "",
                password: ""
            },
            message: ""
        }
    }

    handleChange = e => {
        const { name, value } = e.target
        const { formAddStaff } = this.state
        this.setState({
            formAddStaff: {
                ...formAddStaff,
                [name]: value
            }
        })
    }

    submit = e => {
        e.preventDefault()
        axios.post(`${process.env.REACT_APP_URI}/api/addstaff`, this.state.formAddStaff, {
            withCredentials: true
        })
            .then(response => {
                if (response.data === "Ok") {
                    this.setState({
                        message: "Success",
                        formAddStaff: {
                            username: "",
                            password: ""
                        }
                    })
                }
            })
    }

    render() {
        return (
            <div>
                <h3 className="text-center">Add staff</h3>
                <form onSubmit={this.submit} className="back-connect-form">
                    <input className="back-username-input" value={this.state.formAddStaff.username} onChange={this.handleChange} type="text" name="username" id="username-back" placeholder="Username" required />
                    <br />
                    <input className="back-password-input" value={this.state.formAddStaff.password} onChange={this.handleChange} type="password" name="password" id="password" placeholder="Password" required />
                    <br />
                    <button className="customButton">Ok</button>
                </form>
                <div className="add-staff-message">{this.state.message}</div>
            </div>
        )
    }
}

export default AddStaff