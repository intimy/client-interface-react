//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'

class IdVerificationRequests extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: false,
            idVerificationRequestsData: [],
            verifForm: {
                reason: "",
                selectedOption: undefined
            },
            message: ""
        }
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_URI}/api/getIdVerificationRequests`, { userId: this.props.userId })
            .then(data => {
                this.setState({
                    idVerificationRequestsData: data.data,
                    loaded: true
                })
            })
    }

    handleChange = e => {
        const { name, value } = e.target
        const { verifForm } = this.state
        this.setState({
            verifForm: {
                ...verifForm,
                [name]: value
            }
        })
    }

    submit(data, e) {
        e.preventDefault()
        axios.put(`${process.env.REACT_APP_URI}/api/idRequestValidation`, { userId: data.userId, validation: this.state.verifForm })
            .then(response => {
                let newIdVerificationRequestsData = this.state.idVerificationRequestsData
                newIdVerificationRequestsData.splice(data.index, 1)
                this.setState({
                    idVerificationRequestsData: newIdVerificationRequestsData
                })
            })
    }

    render() {
        if (!this.props.connected) {
            return <Redirect to="/" />
        }
        if (this.state.loaded === true) {
            const usersUnverified = this.state.idVerificationRequestsData.map((user, index) =>
                <div key={index} className="id-request-user-container">
                    <ul className="id-request-user">
                        <li><Link to={`/user/${user._id}`}>{user._id}</Link><img src={user.idVerif.idPhoto} alt="Id photo" /><img src={user.idVerif.idFace} alt="Id face" /></li>
                    </ul>
                    <form className="verif-video-no" onSubmit={(e) => this.submit({ index: index, userId: user._id }, e)}>
                        <input type="radio" className="verif-video-yes" name="selectedOption" value="yes" onChange={this.handleChange} required />Yes
                            <br />
                        <input type="radio" className="verif-video-no" name="selectedOption" value="no" onChange={this.handleChange} required />No
                            <br />
                        <label htmlFor="reason">Reason</label>
                        <br />
                        <textarea name="reason" id="reason" className="textarea-reason" cols="30" rows="3" value={this.state.verifForm.reason} onChange={this.handleChange}></textarea>
                        <br />
                        <button type="submit">Ok</button>
                    </form>
                </div>
            )
            return (
                <div>
                    <h3 className="text-center">Id verification requests</h3>
                    {usersUnverified}
                </div>
            )
        }
        else return null
    }
}

export default IdVerificationRequests