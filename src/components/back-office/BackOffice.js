//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import StaffConnexionLoader from '../StaffConnectionLoader'
import IdVerificationRequests from './IdVerificationRequests'
import WithdrawRequests from './WithdrawRequests'
import AddStaff from './AddStaff'

class BackOffice extends Component {

    constructor(props) {
        super(props)
        this.state = {
            formConnection: {
                username: "",
                password: ""
            },
            redirect: false,
            loaded: true,
            containerOn: null,
            idVerif: {
                idPhoto: "",
                idFace: ""
            },
            message: ""
        }
    }

    setContainer = container => {
        this.setState({
            containerOn: container
        })
    }

    handleChange = e => {
        const { name, value } = e.target
        const { formConnection } = this.state
        this.setState({
            formConnection: {
                ...formConnection,
                [name]: value
            }
        })
    }

    submit = e => {
        e.preventDefault()
        axios.post(`${process.env.REACT_APP_URI}/api/backconnection`, this.state.formConnection, {
            withCredentials: true
        })
            .then(response => {
                if (response.data === "Ok") {
                    window.location.reload()
                }
            })
    }

    render() {
        if (!this.props.connected) {
            return <div className="content">
                <h4 className="backoffice-title">Backoffice</h4>
                <form onSubmit={this.submit} className="back-connect-form">
                    <input className="back-username-input" value={this.state.formConnection.username} onChange={this.handleChange} type="text" name="username" id="username-back" placeholder="Username" required />
                    <br />
                    <input className="back-password-input" value={this.state.formConnection.password} onChange={this.handleChange} type="password" name="password" id="password" placeholder="Password" required />
                    <br />
                    <button className="customButton">Connect</button>
                </form>
            </div>
        }
        else {
            return (
                <div className="content">
                    <h2 className="text-center">Back office</h2>
                    <div className="back-office-left-menu">
                        <span className="back-office-left-menu-item" onClick={() => this.setContainer("idVerif")}>ID verification requests</span>
                        <span className="back-office-left-menu-item" onClick={() => this.setContainer("withdrawRequests")}>Withdraw requests</span>
                        <span className="back-office-left-menu-item" onClick={() => this.setContainer("addStaff")}>Add staff</span>
                    </div>
                    {this.state.containerOn === "idVerif" && <IdVerificationRequests userId={this.props.userId} connected={this.props.connected} />}
                    {this.state.containerOn === "withdrawRequests" && <WithdrawRequests userId={this.props.userId} connected={this.props.connected} />}
                    {this.state.containerOn === "addStaff" && <AddStaff userId={this.props.userId} connected={this.props.connected} />}
                </div>
            )
        }
    }
}

export default StaffConnexionLoader(BackOffice)