//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'

class WithdrawRequest extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: false,
            withdrawRequestsData: [],
            verifForm: {
                reason: "",
                selectedOption: undefined
            },
            message: "",
        }
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_URI}/api/getwithdrawrequests`)
            .then(data => {
                this.setState({
                    withdrawRequestsData: data.data,
                    loaded: true
                })
            })
    }

    submit(data, e) {
        e.preventDefault()
        axios.put(`${process.env.REACT_APP_URI}/api/withdrawRequestValidation`, { userId: data.userId, validation: this.state.verifForm })
            .then(response => {
                let newIdVerificationRequestsData = this.state.withdrawRequestsData
                newIdVerificationRequestsData.splice(data.index, 1)
                this.setState({
                    withdrawRequestsData: newIdVerificationRequestsData
                })
            })
    }

    handleChange = e => {
        const { name, value } = e.target
        const { verifForm } = this.state
        this.setState({
            verifForm: {
                ...verifForm,
                [name]: value
            }
        })
    }

    render() {
        if (this.state.loaded === true) {
            const usersUnverified = this.state.withdrawRequestsData.map((user, index) =>
                <div key={user._id} className="id-request-user-container">
                    <div className="id-request-user">
                        <Link to={`/user/${user.userId}`}>{user.userId}</Link>
                        <p>{user.email}</p>
                        <p className="item-price">{user.amount} <i className="fas fa-coins"></i></p>
                    </div>
                    <form className="verif-video-no" onSubmit={(e) => this.submit({ index: index, userId: user.userId }, e)}>
                        <input type="radio" className="verif-video-yes" name="selectedOption" value="yes" onChange={this.handleChange} required />Yes
                    <br />
                        <input type="radio" className="verif-video-no" name="selectedOption" value="no" onChange={this.handleChange} required />No
                    <br />
                        <label htmlFor="reason">Reason</label>
                        <br />
                        <textarea name="reason" id="reason" className="textarea-reason" cols="30" rows="3" value={this.state.verifForm.reason} onChange={this.handleChange}></textarea>
                        <br />
                        <button type="submit">Ok</button>
                    </form>
                </div>
            )
            return (
                <div>
                    {usersUnverified}
                </div>
            )
        }
        else return null
    }
}

export default WithdrawRequest