//React
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
//Components
import SearchForm from './SearchForm'
import CardHome from './CardHome'
import { setTimeout } from 'timers';

class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            listDataFromChild: [],
            firstTime: false
        }
    }

    componentDidMount() {
        document.title = "Intimy - intimy.shop"
    }

    componentWillUnmount() {

    }

    searchForm = dataFromChild => {
        this.setState({ listDataFromChild: dataFromChild })
    }

    firstTime = () => {
        this.setState({ firstTime: !this.state.firstTime })
    }

    saveData = (stateData) => {
        this.props.saveState(stateData)
    }

    render() {
        return (
            <div>
                <div className="gradient-bloc-home">
                    <p className="gradient-bloc-home-text">
                        The marketplace to buy worn lingerie Easily, Quickly, Safely!
                    </p>
                    <p className="gradient-bloc-home-caution">
                        CAUTION<br />
                        CONTENTS<br />
                        HOT
                    </p>
                    <div className="gradient-bloc-home-first-time-container"><span onClick={this.firstTime} className="gradient-bloc-home-first-time">First time on Intimy?</span></div>
                </div>
                <div className="content content-home">
                    {this.state.firstTime &&
                        <div className="first-time-bloc">
                            1. Select a girl<br />
                            2. Select an item she is selling<br />
                            3. Add it to your bag<br />
                            4. Go to your bag, checkout<br />
                            5. Fill your shipping address (you can save it in your profile to not have to fill it every time)<br />
                            6. That's it! You will receive an email who confirm your purchase<br />
                            <Link className="faq-first-time" to="/faq">FAQ</Link>
                        </div>
                    }
                    <SearchForm searchForm={this.searchForm} />
                    <div className="cards-home-container">
                        <CardHome searchFormProps={this.state.listDataFromChild} searchForm={this.searchForm} saveData={this.saveData} stateData={this.props.stateData} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Home