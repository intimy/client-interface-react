//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import UserConnexionLoader from '../UserConnectionLoader'
import 'webrtc-adapter'
import kurentoUtils from 'kurento-utils'
import Console from 'console'
import io from 'socket.io-client'
import LiveDirectSell from './liveDirectSell'
import LiveAuction from './liveAuction'

class Broadcast extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: true,
            isVerifiedUser: false,
            video: null,
            webRtcPeer: null,
            room: null,
            autoView: true,
            live: false,
            directSellOn: false,
            auctionSellOn: false,
            itemsToSell: "",
            volume: 0,
            showControls: false,
            lengthViewers: 0
        }
        this.socketMediaServer = io(`https://signaling.intimy.shop:8443/`)
        this.blurButton = React.createRef()
    }

    componentWillUnmount() {
        this.stop()
        this.socketMediaServer.close()
        this.socket.close()
    }

    componentDidMount() {
        document.title = "Broadcast - intimy.shop"
        this.socket = io(`${process.env.REACT_APP_URI}/LiveDirectSell`, {
            query: { roomId: this.props.userId }
        })
        this.socket.on('reconnect_attempt', () => {
            this.socket.io.opts.transports = ['websocket', 'polling']
        })
        this.socket.on('Direct sell item sold', (data) => {
            this.setState({
                itemsToSell: ""
            })
        })
        this.socket.on('Auction sell item sold', (data) => {
            this.getItemsToSell()
        })
        this.getItemsToSell()
        this.setState({
            video: document.getElementById('video')
        })

        this.socketMediaServer.on('connect', () => {
            console.log('Connected to socket')
            this.socketMediaServer.emit('subscribeToStream', { room: this.props.userId, username: this.props.userId })
        })

        this.socketMediaServer.on('disconnect', () => {
            console.log('Disconnected from socket')
            this.dispose()
        })

        this.socketMediaServer.on('presenterResponse', (data) => {
            this.presenterResponse(data)
        })

        this.socketMediaServer.on('viewerResponse', (data) => {
            this.viewerResponse(data)
        })

        this.socketMediaServer.on('stopCommunication', (data) => {
            console.log('stopCommunication')
            this.dispose()
        })

        this.socketMediaServer.on('iceCandidate', (data) => {
            this.state.webRtcPeer.addIceCandidate(data.candidate)
        })

        this.socketMediaServer.on('roomJoined', data => {
            this.setState({
                lengthViewers: data
            })
        })
        this.socketMediaServer.on('roomLeaved', data => {
            this.setState({
                lengthViewers: data
            })
        })

        window.onbeforeunload = () => {
            this.stop()
            this.socketMediaServer.close()
            this.socket.close()
        }
    }

    getItemsToSell = async e => {
        const itemsToDirectSell = await axios.get(`${process.env.REACT_APP_URI}/api/getitemstodirectsell`, { params: { sellerId: this.props.userId } })
        if (itemsToDirectSell.data) {
            this.setState({
                itemsToSell: itemsToDirectSell.data
            })
        }
    }

    removeItem = (data) => {
        let sellType
        if (data.endTime) {
            sellType = "Auction"
        }
        else {
            sellType = "Direct"
        }
        axios.put(`${process.env.REACT_APP_URI}/api/deletedirectorauctionsellitem`, { itemId: data._id, sellType: sellType }, {
            withCredentials: true
        })
            .then(data => {
                if (data.data == "Item deleted") {
                    this.setState({
                        itemsToSell: ""
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    itemAdded = (newItem) => {
        this.setState({
            directSellOn: false,
            auctionSellOn: false
        })
        this.getItemsToSell()
    }

    presenterResponse = (message) => {
        if (message.response != 'accepted') {
            var errorMsg = message.message ? message.message : 'Unknow error'
            Console.warn('Call not accepted for the following reason: ' + errorMsg)
            this.dispose()
        } else {
            this.state.webRtcPeer.processAnswer(message.sdpAnswer)
        }
    }

    presenter() {
        if (!this.state.webRtcPeer) {
            this.showSpinner(this.state.video)

            var options = {
                localVideo: this.state.video,
                onicecandidate: this.onIceCandidate,
                configuration: {
                    iceServers: [
                        { "urls": "stun:35.227.61.129:80" },
                        {
                            "urls": "turn:35.227.61.129:80",
                            "username": "Alex",
                            "credential": "pbu3a4"
                        }
                    ]
                }
            }

            const self = this

            this.setState({
                webRtcPeer: kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function (error) {
                    if (error) Console.log(error)

                    this.generateOffer(self.onOfferPresenter)
                }),
                showControls: true
            })
            this.liveStatus(true)
        }
    }

    liveStatus = (status) => {
        axios.put(`${process.env.REACT_APP_URI}/api/setlivestatus`, { live: status, userId: this.props.userId }, {
            withCredentials: true
        })
            .catch(error => {
                console.log(error)
            })
    }

    onOfferPresenter = (error, offerSdp) => {
        if (error) if (error) Console.log(error)

        var message = {
            room: this.props.userId,
            sdpOffer: offerSdp
        }
        this.socketMediaServer.emit('presenter', message)
    }

    onIceCandidate = candidate => {
        //Console.log('Local candidate' + JSON.stringify(candidate))
        this.socketMediaServer.emit('onIceCandidate', { candidate: candidate })
    }

    stop() {
        this.liveStatus(false)
        this.setState({
            autoView: false
        })
        if (this.state.webRtcPeer) {
            this.socketMediaServer.emit('stop')
            this.dispose()
        }
    }

    dispose() {
        if (this.state.webRtcPeer) {
            this.state.webRtcPeer.dispose()
            this.setState({
                webRtcPeer: null
            })
        }
        this.hideSpinner(this.state.video)
        this.setState({
            showControls: false
        })
    }

    sendMessage = (payload) => {
        var event = payload.id,
            message = payload
        console.log('Sending message - ' + event + ': ', message)

        this.socketMediaServer.emit(event, payload)
    }

    showSpinner = (...args) => {
        for (var i = 0; i < args.length; i++) {
            args[i].poster = '/images/transparent-1px.png'
            args[i].style.background = 'center transparent url("/images/spinner.gif") no-repeat'
        }
    }

    hideSpinner = (...args) => {
        for (var i = 0; i < args.length; i++) {
            args[i].src = ''
            args[i].poster = '/images/logo.svg'
            args[i].style.background = ''
        }
    }

    onClickSellType = type => {
        axios.get(`${process.env.REACT_APP_URI}/api/isverifieduser`, { withCredentials: true })
            .then((response) => {
                if (!this.state.directSellOn) {
                    if (response.data === "User verified") {
                        this.setState({
                            directSellOn: type,
                            isVerifiedUser: true
                        })
                    }
                    else if (response.data === "User not verified") {
                        this.setState({
                            directSellOn: type,
                            isVerifiedUser: false
                        })
                    }
                }
                else {
                    this.setState({
                        directSellOn: false
                    })
                }
            })
    }

    handleVolume = e => {
        this.setState({
            volume: e.target.value
        })
        const elem = document.getElementById("video")
        elem.volume = this.state.volume / 100
    }

    fullScreen = () => {
        const elem = document.getElementById("video")
        const requestFullscreen = elem.requestFullscreen || elem.webkitRequestFullscreen || elem.mozRequestFullScreen || elem.msRequestFullscreen
        requestFullscreen.call(elem)
    }

    render() {
        if (this.state.loaded) {
            return (
                <div className="content">
                    <div className="cards-home-container">
                        {this.props.userStatus !== "seller" && <Redirect to="/" />}
                        <h2 className="broadcast-title"><span className="broadcaster-username">My cam </span><span className="length-viewers"><i className="fas fa-users"></i> {this.state.lengthViewers}</span></h2>
                        <div className="col-md-12">
                            <button type="button" id="call" className="glyphicon glyphicon-play btn btn-success" onClick={() => this.presenter()}>Start streaming</button>
                            <button type="button" id="terminate" className="stop-streaming-button glyphicon glyphicon-stop btn btn-danger" onClick={() => this.stop()}>Stop</button>
                        </div>

                        <br />
                        <div>
                            <div id="videoBig">
                                <video id="video" ref="player" autoPlay width="640px" height="480px" poster="../images/logo.svg"></video>
                                {this.state.showControls &&
                                    <div className="controls-container">
                                        <button onClick={() => this.fullScreen()} className="full-screen-live-button">Full screen</button>
                                    </div>
                                }
                            </div>
                            <div className="bidding-history-container">
                                <h3 className="title-bidding-history-container text-center">Item for sale</h3>
                                <span className="text-center">
                                    {this.state.itemsToSell &&
                                        <div className="item-direct-sell">
                                            <img className="directsell-item-picture" src={this.state.itemsToSell.img} alt="directsell item picture" />
                                            <h3 className="title-item-direct-sell">{this.state.itemsToSell.name}</h3>
                                            <p className="item-price-direct-sell">{this.state.itemsToSell.price} <i className="fas fa-coins"></i></p>
                                            <i className="fas fa-times remove-item-direct-sell-button" onClick={() => this.removeItem(this.state.itemsToSell)}></i>
                                        </div>
                                    }
                                </span>
                            </div>
                            <h3 className="text-center sell-product-b-title">Sell a product</h3>
                            <button onClick={() => this.onClickSellType('directSellOn')} ref={this.blurButton} className="directsell-button">Direct sell</button>
                            {this.state.directSellOn &&
                                <div>
                                    {this.state.isVerifiedUser ? (
                                        <LiveDirectSell itemAdded={this.itemAdded} />
                                    ) : (
                                            <p>You need <Link className="blue-link" to="/verificationid">to verify your ID</Link> to sell</p>
                                        )}
                                </div>
                            }
                            {this.state.auctionSellOn &&
                                <div>
                                    {this.state.isVerifiedUser ? (
                                        <LiveAuction itemAdded={this.itemAdded} />
                                    ) : (
                                            <p>You need <Link to="/verificationid" className="blue-link">to verify your ID</Link> to sell</p>
                                        )}
                                </div>
                            }
                        </div>
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(Broadcast)