//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import UserConnexionLoader from '../UserConnectionLoader'
import SearchForm from '../SearchForm'

class Live extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: false,
            broadcasters: null
        }
    }

    componentDidMount() {
        document.title = "Live - intimy.shop"
        this.getData()
    }

    getData = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getbroadcasters`)
            .then((response) => {
                this.setState({
                    broadcasters: response.data,
                    loaded: true
                })
            })
    }

    render() {
        if (this.state.loaded) {
            const broadcasters = this.state.broadcasters.map(data =>
                <div key={data._id}>
                    <Link to={`/viewer/${data._id}`}>
                        <div className="col-md-12">
                        </div>
                        <div className="post-live">
                            <div className="post-home-container-picture">
                                <img src={data.profilPicture} alt="Profil picture" className="pictures-posts-home" />
                            </div>
                            <p className="post-home-pseudo no-margin">{data.username}</p>
                            <p className="text-center">On live</p>
                        </div>
                    </Link>
                </div>
            )
            return (
                <div className="content">
                    {this.state.redirect && <Redirect to='/404' />}
                    <SearchForm searchForm={this.searchFrom} />
                    <div className="cards-home-container">
                        {this.props.userStatus === "seller" && <div className="start-streaming-button-container"><Link to="/broadcast" className="start-streaming-button">Start streaming</Link></div>}
                        <h2 className="text-center">Broadcasters</h2>
                        {this.state.broadcasters.length > 0 ?
                            <div>
                                {broadcasters}
                            </div>
                            :
                            <p className="text-center">No broadcasters</p>
                        }
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(Live)