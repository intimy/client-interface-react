//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import PopUpBilling from '../PopUp/PopUpBilling'
import UserConnexionLoader from '../UserConnectionLoader'
import 'webrtc-adapter'
import kurentoUtils from 'kurento-utils'
import Console from 'console'
import io from 'socket.io-client'
import PopUpBuyMoreTokens from '../PopUp/PopUpBuyMoreTokens'
import PopUpSuccessCheckOut from '../PopUp/PopUpSuccessCheckOut'
import PopUpLogIn from '../PopUp/PopUpLogIn'

class Viewer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: false,
            video: null,
            webRtcPeer: null,
            room: null,
            autoView: true,
            broadcaster: null,
            itemsToSell: [],
            disabledButton: false,
            volume: 50,
            muted: true,
            showControls: false,
            menuConnectedUser: false,
            buyMoreTokens: false,
            popUpSuccessCheckOut: false,
            lengthViewers: 0,
            showPopUpLogIn: false
        }
    }

    componentWillUnmount() {
        this.stop()
    }

    componentDidMount() {
        document.title = "Live - intimy.shop"
        this.getData()
        this.getItemsToSell()
        const socket = io(`${process.env.REACT_APP_URI}/LiveDirectSell`, {
            query: { roomId: this.props.match.params.id },
            transports: ['websocket']
        })
        socket.on('reconnect_attempt', () => {
            socket.io.opts.transports = ['websocket', 'polling']
        })
        socket.on('Direct sell item removed', (data) => {
            this.setState({
                itemsToSell: []
            })
        })
        socket.on('Direct sell item sold', (data) => {
            this.setState({
                itemsToSell: []
            })
        })
        socket.on('Auction sell item removed', (data) => {
            this.setState({
                itemsToSell: []
            })
        })
        socket.on('Auction sell item added', (data) => {
            this.getItemsToSell()
        })
        socket.on('Direct sell item added', (data) => {
            this.getItemsToSell()
        })

        window.onbeforeunload = () => {
            this.stop()
        }
    }

    getData = async e => {
        const sellerData = await axios.get(`${process.env.REACT_APP_URI}/api/getsellerdata`, { params: { id: this.props.match.params.id } })
        this.setState({
            broadcaster: sellerData.data,
            loaded: true
        })

        this.setState({
            video: document.getElementById('video')
        })

        this.socketMediaServer = io(`https://signaling.intimy.shop:8443/`)

        this.socketMediaServer.on('connect', () => {
            console.log('Connected to socket');
            this.socketMediaServer.emit('subscribeToStream', { room: this.props.match.params.id, username: this.props.userId })
        });

        this.socketMediaServer.on('disconnect', () => {
            console.log('Disconnected from socket');
            this.dispose();
        });

        this.socketMediaServer.on('presenterResponse', (data) => {
            this.presenterResponse(data);
        });

        this.socketMediaServer.on('viewerResponse', (data) => {
            this.viewerResponse(data);
        });

        this.socketMediaServer.on('stopCommunication', (data) => {
            console.log('stopCommunication');
            this.dispose();
        });

        this.socketMediaServer.on('iceCandidate', (data) => {
            this.state.webRtcPeer.addIceCandidate(data.candidate)
        });

        this.socketMediaServer.on('streamStarted', data => {
            if (this.state.autoView) {
                this.viewer(this.props.match.params.id);
            }
        })
        this.socketMediaServer.on('roomJoined', data => {
            this.setState({
                lengthViewers: data
            })
        })
        this.socketMediaServer.on('roomLeaved', data => {
            this.setState({
                lengthViewers: data
            })
        })
    }

    getItemsToSell = async e => {
        const itemsToDirectSell = await axios.get(`${process.env.REACT_APP_URI}/api/getitemstodirectsell`, { params: { sellerId: this.props.match.params.id } })
        if (itemsToDirectSell.data) {
            this.setState({
                itemsToSell: [itemsToDirectSell.data],
                disabledButton: false
            })
        }
    }

    checkOut = data => {
        axios.post(`${process.env.REACT_APP_URI}/api/directsellcheckout`, { item: this.state.itemsToSell, data: data }, { withCredentials: true })
            .then(data => {
                if (data.data === 'Done') {
                    this.setState({
                        itemsToSell: [],
                        menuConnectedUser: !this.state.menuConnectedUser,
                        disabledButton: true,
                        popUpSuccessCheckOut: !this.state.popUpSuccessCheckOut
                    })
                }
                else if (data.data === "Not enough tokens") {
                    this.setState({
                        menuConnectedUser: !this.state.menuConnectedUser,
                        buyMoreTokens: !this.state.buyMoreTokens
                    })
                }
                else if (data.data === "Item not available") {
                    this.setState({
                        itemNotAvailable: true,
                        menuConnectedUser: !this.state.menuConnectedUser
                    })
                }
            })
    }

    presenterResponse = (message) => {
        if (message.response != 'accepted') {
            var errorMsg = message.message ? message.message : 'Unknow error';
            Console.warn('Call not accepted for the following reason: ' + errorMsg);
            this.dispose();
        } else {
            console.log(message)
            this.state.webRtcPeer.processAnswer(message.sdpAnswer);
        }
    }

    viewerResponse = (message) => {
        if (message.response != 'accepted') {
            var errorMsg = message.message ? message.message : 'Unknow error';
            Console.warn('Call not accepted for the following reason: ' + errorMsg);
            this.dispose();
        } else {
            this.state.webRtcPeer.processAnswer(message.sdpAnswer);
        }
    }

    viewer = broadcasterId => {
        if (!this.state.webRtcPeer) {
            this.showSpinner(this.state.video)

            var options = {
                remoteVideo: this.state.video,
                onicecandidate: this.onIceCandidate,
                configuration: {
                    iceServers: [
                        { "urls": "stun:35.227.61.129:80" },
                        {
                            "urls": "turn:35.227.61.129:80",
                            "username": "Alex",
                            "credential": "pbu3a4"
                        }
                    ]
                }
            }

            const self = this

            this.setState({
                webRtcPeer: kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function (error) {
                    if (error) Console.log('Error state:' + error)

                    this.generateOffer(self.onOfferViewer)
                }),
                room: broadcasterId,
                showControls: true
            })
        }
    }

    onOfferViewer = (error, offerSdp) => {
        if (error) Console.log(error);

        var message = {
            room: this.props.match.params.id,
            sdpOffer: offerSdp
        }
        this.socketMediaServer.emit('viewer', message);
    }

    onIceCandidate = candidate => {
        //Console.log('Local candidate' + JSON.stringify(candidate));

        this.socketMediaServer.emit('onIceCandidate', { candidate: candidate });
    }

    stop() {
        this.setState({
            autoView: false
        })
        if (this.state.webRtcPeer) {
            this.dispose()
        }
        this.socketMediaServer.emit('stop')
        this.socketMediaServer.disconnect()
    }

    dispose() {
        if (this.state.webRtcPeer) {
            this.state.webRtcPeer.dispose();
            this.setState({
                webRtcPeer: null
            })
        }
        this.hideSpinner(this.state.video)
        this.setState({
            showControls: false
        })
    }

    sendMessage = (payload) => {
        var event = payload.id,
            message = payload;
        console.log('Sending message - ' + event + ': ', message);

        this.socketMediaServer.emit(event, payload);
    }

    showSpinner = (...args) => {
        for (var i = 0; i < args.length; i++) {
            args[i].poster = '/images/transparent-1px.png';
            args[i].style.background = 'center transparent url("/images/spinner.gif") no-repeat';
        }
    }

    hideSpinner = (...args) => {
        for (var i = 0; i < args.length; i++) {
            args[i].src = '';
            args[i].poster = this.state.broadcaster.profilPicture
            args[i].style.background = '';
        }
    }

    handleVolume = e => {
        this.setState({
            volume: e.target.value
        })
        this.refs.player.volume = this.state.volume / 100
    }

    fullScreen = () => {
        const requestFullscreen = this.refs.player.requestFullscreen || this.refs.player.webkitRequestFullscreen || this.refs.player.mozRequestFullScreen || this.refs.player.msRequestFullscreen
        requestFullscreen.call(this.refs.player)
    }

    mute = () => {
        this.setState({
            muted: !this.state.muted
        })
    }

    toggleBuyMoreTokens = (e) => {
        this.setState({
            buyMoreTokens: !this.state.buyMoreTokens
        })
    }

    toggleMenuConnectedUser = (e) => {
        if (this.props.connected) {
            this.setState({
                menuConnectedUser: !this.state.menuConnectedUser
            })
        }
        else {
            this.setState({
                showPopUpLogIn: !this.state.showPopUpLogIn
            })
        }
    }

    togglePopUpSuccessCheckOut = (e) => {
        this.setState({
            popUpSuccessCheckOut: !this.state.popUpSuccessCheckOut
        })
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => this.setState({
        showPopUpLogIn: false
    })

    render() {
        if (this.state.loaded) {
            const items = this.state.itemsToSell.map((item) =>
                <div className="item-direct-sell text-center" key={item._id}>
                    <img className="directsell-item-picture" src={item.img} alt="directsell item picture" />
                    <h3 className="title-item-direct-sell">{item.name}</h3>
                    <p className="item-price-direct-sell">{item.price} <i className="fas fa-coins"></i></p>
                </div>
            )
            return (
                <div className="content">
                    <h2 className="broadcast-title"><span className="broadcaster-username">{this.state.broadcaster.username}'s cam </span><span className="length-viewers"><i className="fas fa-users"></i> {this.state.lengthViewers}</span></h2>
                    <div>
                        <div id="videoBig">
                            <video id="video" ref="player" autoPlay width="640px" height="480px" muted={this.state.muted} poster={this.state.broadcaster.profilPicture}></video>
                            {this.state.showControls &&
                                <div className="controls-container">
                                    <button onClick={() => this.mute()} className="mute-live-button">{this.state.muted ? "Unmute" : "Mute"}</button>
                                    <input type="range" min="0" max="100" className="volumeSlider" id="myRange" onChange={this.handleVolume} value={this.state.volume} />
                                    <button onClick={() => this.fullScreen()} className="full-screen-live-button">Full screen</button>
                                </div>
                            }
                        </div>
                        <div className="bidding-history-container">
                            <h3 className="title-bidding-history-container">Item for sale</h3>
                            {this.state.itemsToSell &&
                                items
                            }
                        </div>
                        <div>
                            {this.state.itemsToSell && !this.state.disabledButton &&
                                <div>
                                    {this.state.itemsToSell.endTime ? (
                                        <div className="auction-sell-button-container">
                                            <div className="test">
                                                <input type="number" min="1" className="form-control auction-amount-input" placeholder="Amount" />
                                                <button className="btn btn-success bid-button" onClick={() => this.checkOut()}>Bid it!</button>
                                            </div>
                                        </div>
                                    ) : (
                                            <button className="btn btn-success float-right direct-sell-buy-button" onClick={this.toggleMenuConnectedUser}>Buy it!</button>
                                        )}
                                </div>
                            }
                            {this.state.menuConnectedUser &&
                                <PopUpBilling toggleMenuConnectedUser={this.toggleMenuConnectedUser} userId={this.props.userId} userStatus={this.props.userStatus} items={this.state.itemsToSell} checkOut={this.checkOut} />
                            }
                            {this.state.popUpSuccessCheckOut && <PopUpSuccessCheckOut togglePopUpSuccessCheckOut={this.togglePopUpSuccessCheckOut} />}
                            {this.state.buyMoreTokens && <PopUpBuyMoreTokens toggleBuyMoreTokens={this.toggleBuyMoreTokens} />}
                            {this.state.showPopUpLogIn &&
                                <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to buy" />
                            }
                        </div>
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(withRouter(Viewer))