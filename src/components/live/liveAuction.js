//React
import React from 'react'
import axios from 'axios'
//Components
import UserConnexionLoader from '../UserConnectionLoader'
import { Redirect } from 'react-router-dom'
import TimePicker from 'rc-time-picker'
import 'rc-time-picker/assets/index.css'
import moment from 'moment'

class LiveAuction extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            formData: {
                name: "",
                price: "",
                endTime: moment(),
                notif: false
            },
            messages: {
                messageStatus: ""
            }
        }
    }

    checkboxChange = e => {
        const { name } = e.target
        const { formData } = this.state
        this.setState({
            formData: {
                ...formData,
                [name]: !formData[name]
            }
        })
    }

    handleChange = e => {
        const { value, name } = e.target
        const { formData } = this.state
        this.setState({
            formData: {
                ...formData,
                [name]: value
            }
        })
    }

    handleChangeEndTime = value => {
        const { formData } = this.state
        this.setState({
            formData: {
                ...formData,
                endTime: value
            }
        })
    }

    createPost = e => {
        e.preventDefault()
        axios.post(`${process.env.REACT_APP_URI}/api/postauctionproduit`, this.state.formData, {
            withCredentials: true
        })
            .then(data => {
                if (data.data === 'Your item is online!') {
                    this.setState({
                        messages: {
                            messageStatus: 'Your item is online!'
                        }
                    })
                    this.props.itemAdded()
                }
                else if (data.data === 'Auction sell already in progress') {
                    this.setState({
                        messages: {
                            messageStatus: 'Auction sell already in progress'
                        }
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        const format = 'h:mm a';
        const { name, description, price, endTime, notif } = this.state.formData
        return (
            <div>
                {!this.props.connected && <Redirect to='/' />}
                {this.props.connected && this.props.userStatus == "seller" &&
                    <div>
                        <div className="posting-container">
                            <form className="post-form form-group" ref={input => this.postForm = input} onSubmit={e => this.createPost(e)}>
                                <div className="input-group">
                                    <input type="text" name="name" className="form-control" value={name} onChange={this.handleChange} placeholder="Name" required />
                                </div>
                                <div className="input-group">
                                    <span className="input-group-addon">$</span>
                                    <input type="number" className="form-control" name="price" step="any" min="0" value={price} onChange={this.handleChange} placeholder="Starting price" aria-label="Amount (to the nearest dollar)" required />
                                </div>
                                <div className="input-group">
                                    <TimePicker
                                        showSecond={false}
                                        defaultValue={endTime}
                                        className="xxx"
                                        name="endTime"
                                        onChange={this.handleChangeEndTime}
                                        format={format}
                                        use12Hours
                                        inputReadOnly />
                                </div>
                                <div className="form-check">
                                    <input type="checkbox" name="notif" id="get-notified-by-email-post" checked={notif} onChange={this.checkboxChange} className="intimy-checkbox" ref={input => this.notif = input} />
                                    <label htmlFor="get-notified-by-email-post"> Get notified by email about the sell</label>
                                </div>
                                <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
                            </form>
                            <br />
                            {this.state.messages.messageStatus}
                        </div>
                    </div>}
            </div>
        )
    }
}

export default UserConnexionLoader(LiveAuction)