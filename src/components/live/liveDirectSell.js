//React
import React from 'react'
import axios from 'axios'
//Components
import UserConnexionLoader from '../UserConnectionLoader'
import { Redirect } from 'react-router-dom'

class LiveDirectSell extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            formData: {
                name: "",
                img: "",
                price: "",
                notif: false,
                shippingOption: "paddedEnvelope"
            },
            messages: {
                messageStatus: ""
            },
            rates: 0,
            showRestriction: false
        }
    }

    componentDidMount() {
        axios.post(`${process.env.REACT_APP_URI}/api/getrates`, {
            withCredentials: true
        })
            .then(data => {
                this.setState({
                    rates: data.data.shippingRates,
                    loaded: true
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    checkboxChange = e => {
        const { name } = e.target
        const { formData } = this.state
        this.setState({
            formData: {
                ...formData,
                [name]: !formData[name]
            }
        })
    }

    handleFileUpload = e => {
        if (e.target.files[0]) {
            const { name } = e.target
            const { formData } = this.state
            let reader = new FileReader()
            let file = e.target.files[0]
            reader.onload = () => {
                this.setState({
                    formData: {
                        ...formData,
                        [name]: reader.result
                    }
                })
            }
            reader.readAsDataURL(file)
        }
    }

    handleChange = e => {
        const { value, name } = e.target
        const { formData } = this.state
        this.setState({
            formData: {
                ...formData,
                [name]: value
            }
        })
    }

    createPost = e => {
        e.preventDefault()
        axios.post(`${process.env.REACT_APP_URI}/api/postdirectsellproduit`, this.state.formData, {
            withCredentials: true
        })
            .then(data => {
                if (data.data === 'Your item is online!') {
                    this.setState({
                        messages: {
                            messageStatus: 'Your item is online!'
                        }
                    })
                    this.props.itemAdded()
                }
                else if (data.data === 'Direct sell already in progress') {
                    this.setState({
                        messages: {
                            messageStatus: 'Direct sell already in progress'
                        }
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
        const { formData } = this.state
        this.postForm.reset()
        this.setState({
            formData: {
                ...formData,
                name: "",
                price: "",
                notif: false
            }
        })
    }

    toggleRestrictions = e => {
        this.setState({
            showRestriction: !this.state.showRestriction
        })
    }

    render() {
        const { name, description, price, notif } = this.state.formData
        if (this.state.loaded) {
            return (
                <div>
                    {!this.props.connected && <Redirect to='/' />}
                    {this.props.connected && this.props.userStatus == "seller" &&
                        <div>
                            <div className="posting-container">
                                <form className="post-form" ref={input => this.postForm = input} onSubmit={e => this.createPost(e)}>
                                    <div className="form-group">
                                        <input type="text" name="name" className="form-control direct-sell-input" value={name} onChange={this.handleChange} placeholder="Name" required />
                                    </div>
                                    <div className="form-group ModifyItemPictureForm">
                                        <label htmlFor="live-direct-sell-picture" className="live-direct-sell-picture-label">Picture</label>
                                        <input type="file" name="img" className="form-control direct-sell-input" id="live-direct-sell-picture" onChange={this.handleFileUpload} accept="image/x-png,image/gif,image/jpeg" required />
                                    </div>
                                    <div className="input-group">
                                        <input type="number" className="form-control direct-sell-input" name="price" step="any" min="0" value={price} onChange={this.handleChange} placeholder="Price in tokens" aria-label="Amount (to the nearest dollar)" required />
                                    </div>
                                    {price && <span>${price / 10}</span>}
                                    <div className="shipping-options">
                                        <h3 className="shipping-options-title">Shipping options</h3>
                                        <p>The final price will include the shipping</p>
                                        <input type="radio" name="paddedEnvelope" id="paddedEnvelope" value={this.state.formData.shippingOption} defaultChecked={this.state.formData.shippingOption} />
                                        <label htmlFor="paddedEnvelope" className="padded-envelope-title"> Flate rate padded envelope </label>
                                        <span> ${this.state.rates[0].amount}</span>
                                        <span onClick={this.toggleRestrictions} className="restriction-padded-envelope"> Restrictions</span>
                                        {this.state.showRestriction &&
                                            <p>
                                                <span>Size: 9-1/2 in x 12-1/2 in</span> <br />
                                                When sealing, the container flaps must be able to close within the normal folds. <br />
                                                Tape may be applied to the flaps and seams to reinforce the container, provided the design of the container is not enlarged by opening the sides and the container is not reconstructed in any way.
                                            </p>
                                        }
                                    </div>
                                    <div className="form-check post-item-form-check">
                                        <input type="checkbox" name="notif" id="get-notified-by-email-post" checked={notif} onChange={this.checkboxChange} className="intimy-checkbox" ref={input => this.notif = input} />
                                        <label htmlFor="get-notified-by-email-post" className="get-notified-item"> Get notified by email</label>
                                    </div>
                                    <button className="btn btn-primary intimy-rectangle-button submit-button-post-item" type="submit">Submit</button>
                                </form>
                                <br />
                                {this.state.messages.messageStatus}
                            </div>
                        </div>}
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(LiveDirectSell)