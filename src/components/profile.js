//React
import React from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import ItemsToSell from './ItemsToSell/ItemsToSell'
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import UserConnexionLoader from './UserConnectionLoader'
import 'react-photoswipe/lib/photoswipe.css'
import Gallery from './gallery/gallery'
import PopUpLogIn from './PopUp/PopUpLogIn'

class Profile extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      profilData: [],
      itemsToSellData: [],
      redirect: false,
      loaded: false,
      imageFull: false,
      srcImageFull: "",
      options: {
        shareEl: false,
        bgOpacity: 0.5
      },
      favorites: null,
      showPopUpLogIn: false
    }
  }

  componentDidMount() {
    this.getData()
    if (this.props.connected) this.getFavorites()
  }

  getData = async e => {
    const sellerData = await axios.get(`${process.env.REACT_APP_URI}/api/getsellerdata`, { params: { id: this.props.match.params.idseller } })
    if (sellerData.data == 'Redirect') {
      this.setState({
        redirect: true
      })
    }
    else {
      this.setState({
        profilData: sellerData.data,
        loaded: true
      })
      document.title = `${sellerData.data.username} - intimy.shop`
    }
  }

  getFavorites = async e => {
    const favoritesData = await axios.get(`${process.env.REACT_APP_URI}/api/getfavorites`, { params: { modelId: this.props.match.params.idseller }, withCredentials: true })
    this.setState({
      favorites: favoritesData.data.favorite
    })
  }

  handleOutsideClick = () => {
    this.setState({
      openedGallery: !this.state.openedGallery
    })
  }

  addFavorites = (e) => {
    if (this.props.connected) {
      axios.post(`${process.env.REACT_APP_URI}/api/addfavorites`, { modelId: this.state.profilData._id, modelUsername: this.state.profilData.username }, { withCredentials: true })
        .then((response) => {
          if (response.data) {
            this.setState({
              favorites: !this.state.favorites
            })
            if (response.data === "Favorite added") {
              window.gtag('event', `favorite added`, {
                'event_category': 'user',
                'event_label': `${this.state.profilData.username}`
              })
            }
            else {
              window.gtag('event', `favorite removed`, {
                'event_category': 'user',
                'event_label': `${this.state.profilData.username}`
              })
            }
          }
        })
    }
    else {
      this.setState({
        showPopUpLogIn: !this.state.showPopUpLogIn
      })
    }
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    showPopUpLogIn: false
  })

  render() {
    if (this.state.loaded) {
      return (
        <div className="content">
          <div className="cards-home-container">
            {this.state.redirect && <Redirect to='/404' />}
            <div className="profile-container">
              <div className="profile-bio-container">
                <div className="profile-picture-container">
                  <img src={this.state.profilData.profilPicture} alt="Profil picture" className="picture-profile" />
                </div>
                {this.state.profilData.pictures.length > 0 &&
                  <div className="pictures-user-container">
                    <Gallery handleOutsideClick={this.handleOutsideClick} items={this.state.profilData} userId={this.props.userId} />
                  </div>
                }
                {this.state.imageFull &&
                  <div className="picture-user-full-container">
                    <img src={this.state.srcImageFull} alt="Picture user full" className="picture-user-full" />
                  </div>
                }
                {this.state.profilData._id !== this.props.userId && <div className="add-favorites"><i onClick={this.addFavorites} className={`add-favorites-icon fas fa-heart ${this.state.favorites ? "active-favorite" : "inactive-favorite"}`}></i></div>}
                <p className={`my-profile-username ${this.state.profilData._id !== this.props.userId ? "username-w-favorite" : ""}`}>{this.state.profilData.username}</p>
                <div className="profile-country-container">
                  <p className="profile-country">{this.state.profilData.aboutMe.country}</p>
                </div>
                <p className="profile-bio">{this.state.profilData.aboutMe.bio}</p>
                <div className="infos-profile-container">
                  <div className="my-profile-left">
                    <div>Age <span className="floats-right">{this.state.profilData.aboutMe.age} years old</span></div>
                    <div>Origins {this.state.profilData.aboutMe.origins ? <span className="floats-right">{this.state.profilData.aboutMe.origins}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Hairs {this.state.profilData.aboutMe.hairs ? <span className="floats-right">{this.state.profilData.aboutMe.hairs}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Height {this.state.profilData.aboutMe.height ? <span className="floats-right">{this.state.profilData.aboutMe.height}cm</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Eyes {this.state.profilData.aboutMe.eyes ? <span className="floats-right">{this.state.profilData.aboutMe.eyes}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Status {this.state.profilData.aboutMe.status ? <span className="floats-right">{this.state.profilData.aboutMe.status}</span> : <span className="floats-right">N/S</span>}</div>
                  </div>
                  <div className="my-profile-right">
                    <div>Interested in {this.state.profilData.aboutMe.interestedIn.length > 0 ? <span className="ilike">{this.state.profilData.aboutMe.interestedIn.join(', ')}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Measurements {this.state.profilData.aboutMe.measurements ? <span className="ilike">{this.state.profilData.aboutMe.measurements}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Tattoos {this.state.profilData.aboutMe.tattoos ? <span className="floats-right">{this.state.profilData.aboutMe.tattoos}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>Piercings {this.state.profilData.aboutMe.piercings ? <span className="floats-right">{this.state.profilData.aboutMe.piercings}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>What I like {this.state.profilData.aboutMe.whatILike ? <span className="idontlike">{this.state.profilData.aboutMe.whatILike}</span> : <span className="floats-right">N/S</span>}</div>
                    <div>What I don't like {this.state.profilData.aboutMe.whatIDontLike ? <span className="idontlike">{this.state.profilData.aboutMe.whatIDontLike}</span> : <span className="floats-right">N/S</span>}</div>
                  </div>
                </div>
              </div>
              <ItemsToSell sellerId={this.props.match.params.idseller} />
              {this.state.showPopUpLogIn &&
                <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to add to your favorites" />
              }
            </div>
          </div>
        </div>
      )
    }
    else return null
  }
}

export default UserConnexionLoader(withRouter(Profile))