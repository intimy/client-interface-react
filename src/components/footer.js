//React
import React, { Component } from 'react';
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <p>
          <Link className="link-footer" to="/contact">Contact</Link>-
          <Link className="link-footer" to="/faq">FAQ</Link>-
          <Link className="link-footer" to="/termsandconditions">Terms and conditions</Link>-
          <Link className="link-footer" to="/privacypolicy">Privacy policy</Link>
        </p>
        <p>Copyright (c) 2018 - Intimy - All Rights Reserved.</p>
        <p>Icons made by <a href="https://www.freepik.com/" title="Freepik" target="_blank">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon" target="_blank">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></p>
      </footer>
    )
  }
}

export default Footer;