//React
import React from 'react';
import { withRouter } from 'react-router-dom';

class ScrollToTop extends React.Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
      window.gtag('event', `On page`, {
        'event_category': 'Page',
        'event_label': `${this.props.location.pathname}`
      })
    }
  }

  render() {
    return this.props.children
  }
}

export default withRouter(ScrollToTop)