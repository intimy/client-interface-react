import React from 'react'
import socketIOClient from 'socket.io-client'

const SocketContext = React.createContext()

export default SocketContext