import React from 'react'
//Components
import ItemsShoppingBag from './ItemsToSell/ItemsShoppingBag'
import PopUpBilling from './PopUp/PopUpBilling'
import UserConnexionLoader from './UserConnectionLoader'
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios'
import PopUpSuccessCheckOut from './PopUp/PopUpSuccessCheckOut'
import PopUpBuyMoreTokens from './PopUp/PopUpBuyMoreTokens'
import { observer } from 'mobx-react'
import ShoppingBagStore from "./stores/ShoppingBagStore"
import PopUpLogIn from './PopUp/PopUpLogIn'

class ShoppingBag extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      dataItems: [],
      price: 0,
      shippingRates: 0,
      fees: 0,
      totalPrice: 0,
      buyMoreTokens: false,
      menuConnectedUser: false,
      popUpSuccessCheckOut: false,
      itemNotAvailable: false
    }
  }

  componentDidMount() {
    document.title = "Shopping bag - intimy.shop"
    if (this.props.connected) this.getShoppingBagData()
    if (!this.props.connected) {
      if (ShoppingBagStore.shoppingBag.length > 0) this.getShoppingBagDataNotConnected(ShoppingBagStore.shoppingBag)
    }
  }

  getShoppingBagData = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemsfrommyshoppingbag`, { withCredentials: true })
      .then(response => {
        if (response.data === "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          const total = response.data.reduce((sum, i) => {
            return sum + i.price
          }, 0)
          this.setState({
            dataItems: response.data,
            price: total
          })
        }
      })
  }

  getShoppingBagDataNotConnected = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemsfrommyshoppingbagnotconnected`, { params: { itemsId: e } })
      .then(response => {
        if (response.data === "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          const total = response.data.reduce((sum, i) => {
            return sum + i.price
          }, 0)
          this.setState({
            dataItems: response.data,
            price: total
          })
        }
      })
  }

  newDataItems = data => {
    const total = data.reduce((sum, i) => {
      return sum + i.price
    }, 0)
    this.setState({
      dataItems: data,
      price: total
    })
  }

  checkOut = data => {
    axios.post(`${process.env.REACT_APP_URI}/api/checkout`, { items: this.state.dataItems, shippingRates: data.shippingRates, fees: data.fees, saveShippingInfos: data.saveShippingInfos, myAddress: data.myAddress }, { withCredentials: true })
      .then(data => {
        if (data.data === 'Done') {
          this.setState({
            dataItems: [],
            price: 0,
            menuConnectedUser: !this.state.menuConnectedUser,
            popUpSuccessCheckOut: !this.state.popUpSuccessCheckOut
          })
          window.gtag('event', `Item bought`, {
            'event_category': 'user',
            'event_label': `Item bought`
          })
        }
        else if (data.data === "Not enough tokens") {
          this.setState({
            menuConnectedUser: !this.state.menuConnectedUser,
            buyMoreTokens: !this.state.buyMoreTokens
          })
        }
        else if (data.data === "Item not available") {
          this.setState({
            itemNotAvailable: true,
            menuConnectedUser: !this.state.menuConnectedUser
          })
        }
      })
  }

  toggleBuyMoreTokens = (e) => {
    this.setState({
      buyMoreTokens: !this.state.buyMoreTokens
    })
  }

  toggleMenuConnectedUser = (e) => {
    if (this.props.connected) {
      this.setState({
        menuConnectedUser: !this.state.menuConnectedUser
      })
    }
    else {
      this.setState({
        popUpLogIn: "to checkout"
      })
    }
  }

  togglePopUpSuccessCheckOut = (e) => {
    this.setState({
      popUpSuccessCheckOut: !this.state.popUpSuccessCheckOut
    })
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    popUpLogIn: false
  })

  render() {
    return <div className="content">
      <div className="cards-home-container">
        <h3 className="ShoppingBagTitle">My shopping bag</h3>
        <div className="ContainerShoppingBagItems">
          <ul className="ShoppingBagMenu">
            <li className="ShoppingBag">Picture</li>
            <li className="ShoppingBagItem">Item</li>
            <li className="ShoppingBagPrice">Price</li>
            <i className="fa fa-times fa-lg RemoveItemShoppingBag-hidden" aria-hidden="true"></i>
          </ul>
          <hr className="ShoppingBagHr" />
          <ItemsShoppingBag dataItems={this.state.dataItems} newDataItems={this.newDataItems} itemNotAvailable={this.state.itemNotAvailable} />
        </div>
        <p className="TotalPriceShoppingBag">Total: {this.state.price} tokens</p>
        <div className="ContainerButtonShoppingBag">
          <button className="btn btn-primary ContinueShoppingButton" onClick={() => this.props.history.push("/")}>Continue shopping</button>
          <button className="btn btn-primary CheckOutButton" onClick={() => this.toggleMenuConnectedUser()}>Check out</button>
        </div>
        {this.state.menuConnectedUser && this.state.dataItems.length > 0 &&
          <PopUpBilling toggleMenuConnectedUser={this.toggleMenuConnectedUser} userId={this.props.userId} userStatus={this.props.userStatus} items={this.state.dataItems} checkOut={this.checkOut} />
        }
        {this.state.popUpSuccessCheckOut && <PopUpSuccessCheckOut togglePopUpSuccessCheckOut={this.togglePopUpSuccessCheckOut} />}
        {this.state.buyMoreTokens && <PopUpBuyMoreTokens toggleBuyMoreTokens={this.toggleBuyMoreTokens} />}
        {this.state.popUpLogIn &&
          <div className="logInHeader">
            <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction={this.state.popUpLogIn} />
          </div>
        }
      </div>
    </div>
  }
}

export default UserConnexionLoader(withRouter(observer(ShoppingBag)))