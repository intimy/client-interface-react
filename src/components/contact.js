import React from 'react'
import socketIOClient from 'socket.io-client'

class Contact extends React.Component {
  renderingStream = false
  componentDidMount() {
    const socket = socketIOClient(`${process.env.REACT_APP_URI}/liveStream`, {
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionAttempts: Infinity,
      transports: ['websocket']
    })
    socket.on('connectLiveStream', data => {
      if (!this.renderingStream) {
        this.renderingStream = true
    
        var img = document.getElementById('my-screenshot');
        img.setAttribute('src', data);
        this.renderingStream = false
      }
    })
  }
  render() {
    return (
      <div className="content">     
        <h2 className="title-contact">A question, a bug, an advice?</h2>
        <form className="form-contact form-group">
          <input type="text" className="form-control" id="email" placeholder="Email" required />
          <input type="text" className="form-control" id="subject" placeholder="Subject" required />
          <textarea id="message" className="form-control" cols="30" rows="10" placeholder="Message" required></textarea>
          <button className="btn btn-primary intimy-rectangle-button contact-button" type="submit">Send</button>
        </form>
      </div>
    )
  }
}

export default Contact