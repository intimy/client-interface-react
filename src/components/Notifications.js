import React from 'react'
//Components
import ItemsShoppingBag from './ItemsToSell/ItemsShoppingBag'
import UserConnexionLoader from './UserConnectionLoader'
import { withRouter, Redirect, Link } from 'react-router-dom'
import axios from 'axios'
import onClickOutside from 'react-onclickoutside'
import ActionLink from './ActionLink'
import classNames from 'classnames'
import moment from 'moment-shortformat'

class Notifications extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            notifications: [],
            notificationsData: [],
            notificationNumber: 0,
            toggleDropdown: false
        }
        this.childRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick, false)
        this.getNotifications()
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClick, false)
    }

    handleClick = e => {
        if (this.childRef.current.contains(e.target)) {
            return
        }
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.props.toggleDropdown()
    }

    getNotifications() {
        axios.get(`${process.env.REACT_APP_URI}/api/getnotifications`, { withCredentials: true })
            .then(response => {
                const { buyers, produits, produitsPictures, produitsVideos, notifs, directSellProducts } = response.data
                const notificationsNumber = notifs.filter(notificationNumber => notificationNumber.seen == false)
                const notificationsData = notifs.map(notification => {
                    let buyer
                    let produitsFound
                    if (notification.notifType === "itemBought") {
                        buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
                        let oldProduitsFound = produits.filter(produit => {
                            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
                        })
                        let directSellProductsFound = directSellProducts.filter(produit => {
                            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
                        })
                        produitsFound = oldProduitsFound.concat(directSellProductsFound)
                    }
                    if (notification.notifType === "itemPicturesBought") {
                        buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
                        let oldProduitsFound = produitsPictures.filter(produit => {
                            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
                        })
                        let directSellProductsFound = directSellProducts.filter(produit => {
                            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
                        })
                        produitsFound = oldProduitsFound.concat(directSellProductsFound)
                    }
                    if (notification.notifType === "itemVideosBought") {
                        buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
                        let oldProduitsFound = produitsVideos.filter(produit => {
                            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
                        })
                        let directSellProductsFound = directSellProducts.filter(produit => {
                            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
                        })
                        produitsFound = oldProduitsFound.concat(directSellProductsFound)
                    }
                    return { ...notification, buyer, produitsFound }
                })
                this.setState({
                    notifications: notifs,
                    notificationNumber: notificationsNumber,
                    notificationsData: notificationsData,
                    loaded: true
                })
            })
    }

    handleClickOutside = e => {
        this.setState({
            toggleDropdown: false
        })
    }

    notificationViewed = e => {
        if (this.state.notificationsData != "") {
            let data = this.state.notificationsData
            let index = data.findIndex(p => p._id == e)
            if (data[index].seen == false) {
                data[index].seen = true
                axios.put(`${process.env.REACT_APP_URI}/api/notificationViewed`, { notifId: e }, { withCredentials: true })
            }
        }
        this.props.toggleDropdown()
    }

    render() {
        if (this.state.loaded) {
            const notifications = this.state.notificationsData.map(items => {
                const notificationStyle = classNames('notification', {
                    'notifications-background': items.seen == false,
                })
                return (
                    <div key={items._id}>
                        {items.notifType === "itemBought" &&
                            <Link to={`/notificationscenter/${items._id}`} onClick={() => this.notificationViewed(items._id)}>
                                <div className={notificationStyle}>
                                    {items.produitsFound.map(produits => <span key={produits._id}>{produits.name} </span>)}
                                    <span>is sold </span>
                                    <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                </div>
                            </Link>
                        }
                        {items.notifType === "itemPicturesBought" &&
                            <Link to={`/notificationscenter/${items._id}`} onClick={() => this.notificationViewed(items._id)}>
                                <div className={notificationStyle}>
                                    {items.produitsFound.map(produits => <span key={produits._id}>{produits.name} </span>)}
                                    <span>is sold </span>
                                    <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                </div>
                            </Link>
                        }
                        {items.notifType === "itemVideosBought" &&
                            <Link to={`/notificationscenter/${items._id}`} onClick={() => this.notificationViewed(items._id)}>
                                <div className={notificationStyle}>
                                    {items.produitsFound.map(produits => <span key={produits._id}>{produits.name} </span>)}
                                    <span>is sold </span>
                                    <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                </div>
                            </Link>
                        }
                        {items.notifType === "verifId" &&
                            <Link to={`/notificationscenter/${items._id}`} onClick={() => this.notificationViewed(items._id)}>

                                {items.verifId.stateVerification ?
                                    <div className={notificationStyle}>
                                        <span>Id approuved </span>
                                        <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                    </div>
                                    :
                                    <div className={notificationStyle}>
                                        <span>Id rejected for reason: {items.verifId.reason}</span>
                                        <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                    </div>
                                }
                            </Link>
                        }
                        {items.notifType === "withdrawRequest" &&
                            <Link to={`/notificationscenter/${items._id}`} onClick={() => this.notificationViewed(items._id)}>

                                {items.withdrawRequest.validated ?
                                    <div className={notificationStyle}>
                                        <span>Withdrawal approuved </span>
                                        <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                    </div>
                                    :
                                    <div className={notificationStyle}>
                                        <span>Withdrawal rejected for reason: {items.withdrawRequest.reason}</span>
                                        <span className="notifs-time-ago">{moment(items.date).short()}</span>
                                    </div>
                                }
                            </Link>
                        }
                    </div>
                )
            }).reverse()
            return <div className="container-notifications-bubble" ref={this.childRef}>
                {this.state.notificationsData.length > 0 ?
                    <div className="ListItemsMenuConnectedUser">
                        {notifications}
                    </div>
                    :
                    <p className="text-center no-notif-yet">No notifications yet</p>
                }
            </div>
        }
        else return null
    }
}

export default UserConnexionLoader(Notifications)