//React
import React from 'react'
//Components
import {instanceOf} from 'prop-types'
import {withCookies, Cookies} from 'react-cookie'

import axios from 'axios'

//Router
import {Router, Route} from 'react-router-dom'

const staffConnexionLoader = (WrappedComponent) => {
    class StaffConnexion extends React.Component {

        constructor(props) {
          super(props)
          this.state = {
            connectedStateLoaded: false,
            connected: false,
            userId: ""
          }
        }
      
        static propTypes = {
          cookies: instanceOf(Cookies).isRequired
        }
      
        componentDidMount() { 
          const { cookies } = this.props
       
          const cookie = cookies.get('staffauthtoken')
      
          if (cookie) {
            axios.get(`${process.env.REACT_APP_URI}/api/staffauthtoken`, {
              withCredentials: true
            })
            .then(data => {   
              if (data.data.status === true) {
                this.setState({
                  connectedStateLoaded: true,
                  connected: true,
                  userId: data.data.token.userId
                })
              }
            })
          }
          else {
            this.setState({
              connectedStateLoaded: true,
              connected: false
            })
          }
        }
      
        render() {
          const {connectedStateLoaded, connected} = this.state
          if (!connectedStateLoaded) return null
          return (
            <WrappedComponent {...this.state} {...this.props}/>
          )
        }
      }
      return withCookies(StaffConnexion)
}

export default staffConnexionLoader