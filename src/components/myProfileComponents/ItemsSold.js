//React
import React, { Component } from 'react'
//Router
import { Link, Redirect, withRouter } from 'react-router-dom'
//Components
import PopUpModifyMyProfile from '../PopUp/PopUpModifyMyProfile'
import PopUpPostItem from '../PopUp/PopUpPostItem'
import { withCookies, Cookies } from 'react-cookie'
import { instanceOf } from 'prop-types'
import axios from 'axios'
import MyItemsToSell from '../ItemsToSell/MyItemsToSell'
import Gallery from '../gallery/gallery'
import moment from 'moment'

class ItemsSold extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            newItem: null,
            showModify: false,
            showPostItem: false,
            itemsSold: [],
            itemsSoldPictures: [],
            itemsSoldVideos: [],
            notificationsData: [],
            messages: {
                messageProfilUpdated: ""
            }
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData = async e => {
        const data = await axios.get(`${process.env.REACT_APP_URI}/api/getmyitemssold`, { withCredentials: true })
        this.setState({
            itemsSold: data.data.produits,
            itemsSoldPictures: data.data.produitsPictures,
            itemsSoldVideos: data.data.produitsVideos,
            loaded: true
        })
    }

    toggleModify = (content) => {
        this.setState({ showModify: content })
    }

    closePopUpModify = () => {
        this.setState({ showModify: false })
    }

    togglePostItem = (content) => {
        this.setState({ showPostItem: content })
    }

    closePopUpPostItem = () => {
        this.setState({ showPostItem: false })
    }

    setStateMessages = e => {
        this.setState({
            messages: {
                messageProfilUpdated: e
            }
        })
        this.getData()
    }

    newItem = newItem => {
        this.myItemsToSell.getMyItemsToSell(newItem)
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.setState({
            showModify: false
        })
    }

    render() {
        if (this.state.loaded) {
            const items = this.state.itemsSold.map((item, index) => {
                return (
                    <div className="item-sold-profile" key={index}>
                        <div>
                            <img className="item-picture-profile" src={item.img} alt="" />
                            <h3 className="title-item-profile">{item.name}</h3>
                            <p className="item-price-profile">{item.price} tokens</p>
                            <p>Order placed on {moment(item.boughtOn).format('MMM DD, YYYY')}</p>
                            <a className="btn-download-shipping-label" href={item.shippingLabelLink} role="button">Download shipping label</a>
                        </div>
                    </div>
                )
            }).reverse()
            const itemsPictures = this.state.itemsSoldPictures.map((item, index) => {
                return (
                    <div className="item-sold-profile" key={index}>
                        <div>
                            <img className="item-picture-profile" src={item.img} alt="" />
                            <h3 className="title-item-profile">{item.name}</h3>
                            <p className="item-price-profile">{item.price} tokens</p>
                            <p>Sold {item.boughtBy.length} times</p>
                            <p>Order placed on {moment(item.boughtOn).format('MMM DD, YYYY')}</p>
                        </div>
                    </div>
                )
            }).reverse()
            const itemsVideos = this.state.itemsSoldVideos.map((item, index) => {
                return (
                    <div className="item-sold-profile" key={index}>
                        <div>
                            <img className="item-picture-profile" src={item.img} alt="" />
                            <h3 className="title-item-profile">{item.name}</h3>
                            <p className="item-price-profile">{item.price} tokens</p>
                            <p>Sold {item.boughtBy.length} times</p>
                            <p>Order placed on {moment(item.boughtOn).format('MMM DD, YYYY')}</p>
                        </div>
                    </div>
                )
            }).reverse()
            return (
                <div className="items-my-profile-container">
                    {items}
                    {itemsPictures}
                    {itemsVideos}
                </div>
            )
        }
        else return null
    }
}

export default withRouter(ItemsSold)