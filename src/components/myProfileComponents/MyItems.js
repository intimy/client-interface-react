//React
import React, { Component } from 'react'
//Router
import { Link, withRouter } from 'react-router-dom'
//Components
import PopUpPostItem from '../PopUp/PopUpPostItem'
import MyItemsToSell from '../ItemsToSell/MyItemsToSell'
import PopUpSuccessItemPosted from '../PopUp/PopUpSuccessItemPosted'
import axios from 'axios'

class MyItems extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showModify: false,
            showPostItem: false,
            successPopUp: false,
            profilData: [],
            messages: {
                messageProfilUpdated: ""
            }
        }
    }

    toggleModify = (content) => {
        this.setState({ showModify: content })
    }

    closePopUpModify = () => {
        this.setState({ showModify: false })
    }

    togglePostItem = (content) => {
        axios.get(`${process.env.REACT_APP_URI}/api/isverifieduser`, { withCredentials: true })
            .then((response) => {
                if (response.data === "User verified") {
                    this.setState({ showPostItem: content })
                }
                else if (response.data === "User not verified") {
                    this.setState({ showPostItem: "verifyId" })

                }
            })
    }

    closePopUpPostItem = () => {
        this.setState({ showPostItem: false })
    }

    setStateMessages = e => {
        this.setState({
            messages: {
                messageProfilUpdated: e
            }
        })
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.setState({
            showModify: false
        })
    }

    setActive = e => {
        this.setState({
            isActive: e
        })
    }

    successPopUp = () => {
        this.setState({ successPopUp: true })
    }

    closeSuccessPopUp = () => {
        this.setState({ successPopUp: false })
    }

    render() {
        return (
            <div>
                {this.props.userStatus == 'seller' &&
                    <div className="profile-container">
                        <div className="profile-bio-container">
                            <div className='my-profile-post-button-container'>
                                <button type="button" className="btn btn-primary my-profile-post-button" onClick={() => this.togglePostItem('postItem')}>Post an item</button>
                            </div>
                            <div>{this.state.messages.messageProfilUpdated}</div>
                            {this.state.showPostItem === "postItem" ?
                                <PopUpPostItem successPopUp={this.successPopUp} userStatus={this.props.userStatus} showPostItem={this.state.showPostItem} closePopUpPostItem={this.closePopUpPostItem} />
                                :
                                this.state.showPostItem === "verifyId" &&
                                <p className="text-center">You need <Link to="/verificationid" className="blue-link">to verify your ID</Link> to sell</p>
                            }
                            <div className="my-profile-image-courasel items-my-profile-container">
                                <MyItemsToSell />
                            </div>
                            {this.state.successPopUp && <PopUpSuccessItemPosted closeSuccessPopUp={this.closeSuccessPopUp} />}
                        </div>
                    </div>}
            </div>
        )
    }
}

export default withRouter(MyItems)