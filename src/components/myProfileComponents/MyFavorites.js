//React
import React, { Component } from 'react'
//Router
import { Link, Redirect, withRouter } from 'react-router-dom'
//Components
import PopUpModifyMyProfile from '../PopUp/PopUpModifyMyProfile'
import PopUpPostItem from '../PopUp/PopUpPostItem'
import { withCookies, Cookies } from 'react-cookie'
import { instanceOf } from 'prop-types'
import axios from 'axios'
import MyItemsToSell from '../ItemsToSell/MyItemsToSell'
import Gallery from '../gallery/gallery'

class myFavorites extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newItem: null,
            showModify: false,
            showPostItem: false,
            myFavorites: [],
            messages: {
                messageProfilUpdated: ""
            }
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getmyfavorites`, { withCredentials: true })
            .then((response) => {
                this.setState({
                    myFavorites: response.data
                })
            })
    }

    toggleModify = (content) => {
        this.setState({ showModify: content })
    }

    closePopUpModify = () => {
        this.setState({ showModify: false })
    }

    togglePostItem = (content) => {
        this.setState({ showPostItem: content })
    }

    closePopUpPostItem = () => {
        this.setState({ showPostItem: false })
    }

    setStateMessages = e => {
        this.setState({
            messages: {
                messageProfilUpdated: e
            }
        })
        this.getData()
    }

    newItem = newItem => {
        this.myItemsToSell.getMyItemsToSell(newItem)
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.setState({
            showModify: false
        })
    }

    render() {
        const myFavorites = this.state.myFavorites.map((favorite, index) =>
            <div className="post-home" key={favorite._id}>
                <Link to={`user/${favorite._id}`}>
                    <div className="post-home-container-picture">
                        <img src={favorite.profilPicture} alt="" className="pictures-posts-home" />
                    </div>
                    <p className="post-home-pseudo no-margin">{favorite.username}</p>
                </Link>
                {favorite.live === true &&
                    <Link to={`viewer/${favorite._id}`}>
                        <p className="text-center">On live</p>
                    </Link>
                }
            </div>
        )
        return (
            <div className="items-my-profile-container">
                {myFavorites}
            </div>
        )
    }
}

export default withRouter(myFavorites)