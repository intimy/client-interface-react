//React
import React, { Component } from 'react'
//Router
import { Link, Redirect, withRouter } from 'react-router-dom'
//Components
import PopUpModifyMyProfile from '../PopUp/PopUpModifyMyProfile'
import PopUpPostItem from '../PopUp/PopUpPostItem'
import { withCookies, Cookies } from 'react-cookie'
import { instanceOf } from 'prop-types'
import axios from 'axios'
import MyItemsToSell from '../ItemsToSell/MyItemsToSell'
import Gallery from '../gallery/gallery'

class AboutMe extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newItem: null,
            showModify: false,
            showPostItem: false,
            profilData: [],
            picture: null,
            messages: {
                messageProfilUpdated: ""
            },
            isLoaded: false
        }
    }

    componentDidMount() {
        this.getData()
    }

    toggleModify = (content) => {
        this.setState({ showModify: content })
    }

    closePopUpModify = () => {
        this.setState({ showModify: false })
    }

    togglePostItem = (content) => {
        this.setState({ showPostItem: content })
    }

    closePopUpPostItem = () => {
        this.setState({ showPostItem: false })
    }

    getData = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getprofildata`, { params: { userStatus: this.props.userStatus }, withCredentials: true })
            .then((response) => {
                this.setState({
                    profilData: response.data,
                    isLoaded: true
                })
            })
    }

    setStateMessages = e => {
        this.setState({
            messages: {
                messageProfilUpdated: e
            }
        })
        this.getData()
        this.props.profileModified()
    }

    newItem = newItem => {
        this.myItemsToSell.getMyItemsToSell(newItem)
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.setState({
            showModify: false
        })
    }

    setActive = e => {
        this.setState({
            isActive: e
        })
    }

    render() {
        if (this.state.isLoaded) {
            return (
                <div>
                    {this.props.userStatus == 'seller' &&
                        <div className="profile-container">
                            <div className="profile-bio-container">
                                <button className="btn btn-primary intimy-rectangle-button ModifyMyProfileButton" onClick={() => this.toggleModify('myInfos')}>Modify my profile</button>
                                <br />{this.state.messages.messageProfilUpdated && <div className="message-profile-updated">{this.state.messages.messageProfilUpdated}</div>}
                                <p>{this.state.profilData.username} </p>
                                <div className="profile-country-container">
                                    <p className="profile-country">{this.state.profilData.aboutMe.country}</p>
                                </div>
                                <p className="profile-bio">{this.state.profilData.aboutMe.bio}</p>
                                <p className="shipping-infos-title">About me</p>
                                <div className="infos-profile-container">
                                    <div className="my-profile-left">
                                        <div>Age <span className="floats-right">{this.state.profilData.aboutMe.age} years old</span></div>
                                        <div>Origins {this.state.profilData.aboutMe.origins ? <span className="floats-right">{this.state.profilData.aboutMe.origins}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Hairs {this.state.profilData.aboutMe.hairs ? <span className="floats-right">{this.state.profilData.aboutMe.hairs}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Height {this.state.profilData.aboutMe.height ? <span className="floats-right">{this.state.profilData.aboutMe.height}cm</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Eyes {this.state.profilData.aboutMe.eyes ? <span className="floats-right">{this.state.profilData.aboutMe.eyes}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Status {this.state.profilData.aboutMe.status ? <span className="floats-right">{this.state.profilData.aboutMe.status}</span> : <span className="floats-right">N/S</span>}</div>
                                    </div>
                                    <div className="my-profile-right">
                                        <div>Interested in {this.state.profilData.aboutMe.interestedIn.length > 0 ? <span className="ilike">{this.state.profilData.aboutMe.interestedIn.join(', ')}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Measurements {this.state.profilData.aboutMe.measurements ? <span className="ilike">{this.state.profilData.aboutMe.measurements}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Tattoos {this.state.profilData.aboutMe.tattoos ? <span className="floats-right">{this.state.profilData.aboutMe.tattoos}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>Piercings {this.state.profilData.aboutMe.piercings ? <span className="floats-right">{this.state.profilData.aboutMe.piercings}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>What I like {this.state.profilData.aboutMe.whatILike ? <span className="ilike">{this.state.profilData.aboutMe.whatILike}</span> : <span className="floats-right">N/S</span>}</div>
                                        <div>What I don't like {this.state.profilData.aboutMe.whatIDontLike ? <span className="idontlike">{this.state.profilData.aboutMe.whatIDontLike}</span> : <span className="floats-right">N/S</span>}</div>
                                    </div>
                                </div>
                                <div className="my-profile-shipping-infos-container">
                                    <p className="shipping-infos-title">My shipping informations</p>
                                    <button className="btn btn-primary intimy-rectangle-button modify-shipping-details-button" onClick={() => this.toggleModify('shippingDetails')}>Modify shipping informations</button>
                                    <div className="infos-shipping-container">
                                        {this.state.profilData.firstname && <div><span>First Name </span><span className="floats-right">{this.state.profilData.firstname}</span></div>}
                                        {this.state.profilData.lastname && <div>Last Name <span className="floats-right">{this.state.profilData.lastname}</span></div>}
                                        {this.state.profilData.streetaddress && <div>Street Address <span className="floats-right">{this.state.profilData.streetaddress}</span></div>}
                                        {this.state.profilData.apt && <div> Apt #, Floor, etc...  <span className="floats-right">{this.state.profilData.apt}</span></div>}
                                        {this.state.profilData.city && <div>City <span className="floats-right">{this.state.profilData.city}</span></div>}
                                        {this.state.profilData.state && <div>State <span className="floats-right">{this.state.profilData.state}</span></div>}
                                        {this.state.profilData.zipcode && <div>Zip Code <span className="floats-right">{this.state.profilData.zipcode}</span></div>}
                                        {this.state.profilData.country && <div>Country <span className="floats-right">{this.state.profilData.country}</span></div>}
                                    </div>
                                </div>
                            </div>
                            {this.state.showModify && <PopUpModifyMyProfile toggleModify={this.toggleModify} profilData={this.state.profilData} setStateMessages={this.setStateMessages} userStatus={this.props.userStatus} showModify={this.state.showModify} closePopUpModify={this.closePopUpModify} />}
                        </div>}
                    {this.props.userStatus == 'buyer' &&
                        <div className="profile-container">
                            <div className="profile-bio-container">
                                <button className="btn btn-primary intimy-rectangle-button ModifyMyProfileButton" onClick={() => this.toggleModify('myInfos')}>Modify my profile</button>
                                <br />{this.state.messages.messageProfilUpdated && <div className="message-profile-updated">{this.state.messages.messageProfilUpdated}</div>}
                                <p>{this.state.profilData.username} </p>
                                <p>{this.state.profilData.age} years old</p>
                                <div className="my-profile-shipping-infos-container">
                                    <p className="shipping-infos-title">My shipping informations</p>
                                    <button className="btn btn-primary intimy-rectangle-button modify-shipping-details-button" onClick={() => this.toggleModify('shippingDetails')}>Modify shipping informations</button>
                                    <div className="infos-shipping-container">
                                        {this.state.profilData.firstname && <div><span>First Name </span><span className="floats-right">{this.state.profilData.firstname}</span></div>}
                                        {this.state.profilData.lastname && <div>Last Name <span className="floats-right">{this.state.profilData.lastname}</span></div>}
                                        {this.state.profilData.streetaddress && <div>Street Address <span className="floats-right">{this.state.profilData.streetaddress}</span></div>}
                                        {this.state.profilData.apt && <div> Apt #, Floor, etc...  <span className="floats-right">{this.state.profilData.apt}</span></div>}
                                        {this.state.profilData.city && <div>City <span className="floats-right">{this.state.profilData.city}</span></div>}
                                        {this.state.profilData.state && <div>State <span className="floats-right">{this.state.profilData.state}</span></div>}
                                        {this.state.profilData.zipcode && <div>Zip Code <span className="floats-right">{this.state.profilData.zipcode}</span></div>}
                                    </div>
                                </div>
                            </div>
                            {this.state.showModify && <PopUpModifyMyProfile toggleModify={this.toggleModify} profilData={this.state.profilData} setStateMessages={this.setStateMessages} userStatus={this.props.userStatus} showModify={this.state.showModify} closePopUpModify={this.closePopUpModify} />}
                        </div>}
                </div>
            )
        }
        else return null
    }
}

export default withRouter(AboutMe)