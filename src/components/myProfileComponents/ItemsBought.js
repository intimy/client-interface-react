//React
import React, { Component } from 'react'
//Router
import { withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import moment from 'moment'

class ItemsBought extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newItem: null,
            showModify: false,
            showPostItem: false,
            itemsBought: [],
            itemsBoughtPictures: [],
            itemsBoughtVideos: [],
            messages: {
                messageProfilUpdated: ""
            }
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getmyitemsbought`, { withCredentials: true })
            .then((response) => {
                this.setState({
                    itemsBought: response.data.produits,
                    itemsBoughtPictures: response.data.produitsPictures,
                    itemsBoughtVideos: response.data.produitsVideos
                })
            })
    }

    toggleModify = (content) => {
        this.setState({ showModify: content })
    }

    closePopUpModify = () => {
        this.setState({ showModify: false })
    }

    togglePostItem = (content) => {
        this.setState({ showPostItem: content })
    }

    closePopUpPostItem = () => {
        this.setState({ showPostItem: false })
    }

    setStateMessages = e => {
        this.setState({
            messages: {
                messageProfilUpdated: e
            }
        })
        this.getData()
    }

    newItem = newItem => {
        this.myItemsToSell.getMyItemsToSell(newItem)
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.setState({
            showModify: false
        })
    }

    render() {
        const itemsBought = this.state.itemsBought.map((item, index) =>
            <div className="item-sold-profile" key={item._id}>
                <img className="item-picture-profile" src={item.img} alt="" />
                <h3 className="title-item-profile">{item.name}</h3>
                <p className="item-price-profile">{item.price} tokens</p>
                <p>Bought on {moment(item.boughtOn).format('MMM DD, YYYY')}</p>
            </div>
        ).reverse()
        const itemsBoughtPictures = this.state.itemsBoughtPictures.map((item, index) =>
            <div className="item-sold-profile" key={item._id}>
                <img className="item-picture-profile" src={item.img} alt="" />
                <h3 className="title-item-profile">{item.name}</h3>
                <p className="item-price-profile">{item.price} tokens</p>
                <p>Bought on {moment(item.boughtOn).format('MMM DD, YYYY')}</p>
                <p><a className="blue-link" href={item.pictures}>Download</a></p>
            </div>
        ).reverse()
        const itemsBoughtVideos = this.state.itemsBoughtVideos.map((item, index) =>
            <div className="item-sold-profile" key={item._id}>
                <img className="item-picture-profile" src={item.img} alt="" />
                <h3 className="title-item-profile">{item.name}</h3>
                <p className="item-price-profile">{item.price} tokens</p>
                <p>Bought on {moment(item.boughtOn).format('MMM DD, YYYY')}</p>
                <p><a className="blue-link" href={item.videos}>Download</a></p>
            </div>
        ).reverse()
        return (
            <div className="items-my-profile-container">
                {itemsBought}
                {itemsBoughtPictures}
                {itemsBoughtVideos}
            </div>
        )
    }
}

export default withRouter(ItemsBought)