//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link} from 'react-router-dom'

class Posts extends Component {

  render () {
    return (
      <div className="post-home">
        <p className="post-home-pseudo no-margin">{this.props.details.name}</p>
        <img src={this.props.details.img} alt={this.props.details.name} className="pictures-posts-home" />
        <p className="post-description no-margin">{this.props.details.description}</p>
      </div>
    )
  }
}

export default Posts