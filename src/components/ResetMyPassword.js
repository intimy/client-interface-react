import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class ResetMyPassword extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            authResetMyPassword: false,
            messageStatusPassword: null,
            forgotPassword: {
                newPassword: "",
                confirmNewPassword: ""
            }
        }
    }

    componentDidMount() {
        document.title = "Reset my password - intimy.net"
        axios.get(`${process.env.REACT_APP_URI}/api/authresetmypassword`, { params: { id: this.props.match.params.id, resetToken: this.props.match.params.token } })
            .then(data => {
                if (data.data === "Ok") {
                    this.setState({
                        authResetMyPassword: true
                    })
                }
                else {
                    this.setState({
                        authResetMyPassword: false
                    })
                }
            })
    }

    submitResetPassword = e => {
        e.preventDefault()

        axios.post(`${process.env.REACT_APP_URI}/api/resetmypassword`, { id: this.props.match.params.id, forgotPassword: this.state.forgotPassword })
            .then(data => {
                if (data.data == "Passwords doesn't match") {
                    this.setState({
                        messageStatusPassword: "Not match"
                    })
                }
                else if (data.data == "Not ok") {
                    this.setState({
                        messageStatusPassword: "Error"
                    })
                }
                else if (data.data == "Ok") {
                    this.setState({
                        messageStatusPassword: "Ok"
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    handleChange = e => {
        const { name, value } = e.target
        const { forgotPassword } = this.state
        this.setState({
            forgotPassword: {
                ...forgotPassword,
                [name]: value
            }
        })
    }

    render() {
        return (
            <div className="reset-password-container">
                {this.state.authResetMyPassword ?
                    <div>
                        <h3 className="reset-password-title">Enter your new password</h3>
                        <form className="form-reset-my-password" onSubmit={this.submitResetPassword}>
                            <input className="input-reset-password" type="password" name="newPassword" id="newPassword" placeholder="New password" value={this.state.forgotPassword.newPassword} onChange={this.handleChange} required />
                            <br />
                            <input className="input-reset-confirm-password" type="password" name="confirmNewPassword" id="confirmNewPassword" placeholder="Confirm new password" value={this.state.forgotPassword.confirmNewPassword} onChange={this.handleChange} required />
                            <br />
                            <button className="button-reset-password" type="submit">Ok</button>
                        </form>
                        <div className="messages-reset-passwords">
                            {this.state.messageStatusPassword === "Ok" && <p>Your password has been changed successfully. You can now sign in with your new password</p>}
                            {this.state.messageStatusPassword === "Not match" && <p>Passwords does not match</p>}
                            {this.state.messageStatusPassword === "Error" && <p>Something wrong happened, but we are safe! Ouf!</p>}
                        </div>
                    </div>
                    :
                    <p>Link not available anymore</p>
                }
            </div>
        )
    }
}

export default ResetMyPassword