import { decorate, observable, configure, action, reaction } from 'mobx'
configure({ enforceActions: 'observed' })

class ShoppingBagStore {

    shoppingBag = JSON.parse(localStorage.getItem("cart")) || []

    addItem(e) {
        this.shoppingBag.push(e)
        localStorage.setItem("cart", JSON.stringify(this.shoppingBag))
    }
    clearItem(e) {
        this.shoppingBag.splice(e, 1)
        localStorage.setItem("cart", JSON.stringify(this.shoppingBag))
    }
    clearAll() {
        localStorage.removeItem("cart")
    }
}

decorate(ShoppingBagStore, {
    shoppingBag: observable,
    addItem: action,
    clearItem: action,
    clearAll: action
})

const NewShoppingBagStore = new ShoppingBagStore()

export default NewShoppingBagStore