import { decorate, observable, configure, action, reaction } from 'mobx'
configure({ enforceActions: 'observed' })

class SearchBarStore {

    searchString = []

    addString(e) {
        this.searchString = e
    }
    clearString() {
        this.searchString = []
    }
}

decorate(SearchBarStore, {
    searchString: observable,
    addString: action,
    clearString: action
})

const NewSearchBarStore = new SearchBarStore()

export default NewSearchBarStore