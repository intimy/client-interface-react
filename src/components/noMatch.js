//React
import React, { Component } from 'react';
//Router
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';

const NoMatch = ({ location }) => (
  <div className="not-found">
    <h3>404</h3>
    <p>No panties here!</p>
  </div>
)

export default NoMatch;