//React
import React, { Component } from 'react'

class PrivacyPolicy extends Component {

    componentDidMount() {
        document.title = "FAQ - intimy.shop"
    }

    render() {
        return (
            <div className="content">
                <div className="faq-container">
                    <h4 className="profile-items-title">PRIVACY POLICY</h4>
                    <p className="last-update-terms">Last updated November 29, 2018</p>
                    <div>
                        <h5 className="faq-question">AGREEMENT TO TERMS</h5>
                        <p className="faq-content">
                            Thank you for choosing to be part of our community at Intimy (“Company”, “we”, “us”, or “our”). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about our policy, or our practices with regards to your personal information, please contact us at intimy.shop@gmail.com.
                            When you visit our website https://www.intimy.shop, and use our services, you trust us with your personal information. We take your privacy very seriously. In this privacy notice, we describe our privacy policy. We seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy policy that you do not agree with, please discontinue use of our Sites and our services.
                            This privacy policy applies to all information collected through our website (such as https://www.intimy.shop), and/or any related services, sales, marketing or events (we refer to them collectively in this privacy policy as the "Sites").
                            Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.
                        </p>
                        <h5 className="faq-question">WHAT INFORMATION DO WE COLLECT?</h5>
                        <div className="faq-content">
                            <p className="kind-collected-data-privacy-policy">Personal information you disclose to us</p>
                            In Short: We collect personal information that you provide to us such as name, address, contact information, passwords (passwords are encypted) and security data, and payment information.
                            We collect personal information that you voluntarily provide to us when registering at the Sites expressing an interest in obtaining information about us or our products and services, when participating in activities on the Sites (such as posting messages or entering competitions, contests or giveaways) or otherwise contacting us.
                            The personal information that we collect depends on the context of your interactions with us and the Sites, the choices you make and the products and features you use. The personal information we collect can include the following:
                            Name and Contact Data. We collect your first and last name, email address, postal address and other similar contact data.
                            Credentials. We collect passwords (passwords are encypted), and similar security information used for authentication and account access.
                            Payment Data. We collect data necessary to process your payment if you make purchases, such as your payment instrument number (such as a credit card number), and the security code associated with your payment instrument. All payment data is stored by our payment processor and you should review its privacy policies and contact the payment processor directly to respond to your questions.
                            All personal information that you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.
                        <p className="kind-collected-data-privacy-policy">Information automatically collected</p>
                            In Short: Some information – such as IP address and/or browser and device characteristics – is collected automatically when you visit our Sites.
                            We automatically collect certain information when you visit, use or navigate the Sites. This information does not reveal your specific identity (like your name or contact information) but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Sites and other technical information. This information is primarily needed to maintain the security and operation of our Sites, and for our internal analytics and reporting purposes.
                            Like many businesses, we also collect information through cookies and similar technologies.
                        </div>
                        <h5 className="faq-question">HOW DO WE USE YOUR INFORMATION?</h5>
                        <p className="faq-content">
                            In Short: We process your information for purposes based on legitimate business interests, the fulfillment of our contract with you, compliance with our legal obligations, and/or your consent.
                            We use personal information collected via our Sites for a variety of business purposes described below. We process your personal information for these purposes in reliance on our legitimate business interests, in order to enter into or perform a contract with you, with your consent, and/or for compliance with our legal obligations. We indicate the specific processing grounds we rely on next to each purpose listed below.
                            We use the information we collect or receive:
                            To send administrative information to you. We may use your personal information to send you product, service and new feature information and/or information about changes to our terms, conditions, and policies.
                            Fulfill and manage your orders. We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the Sites.
                            To protect our Sites. We may use your information as part of our efforts to keep our Sites safe and secure (for example, for fraud monitoring and prevention).
                        </p>
                        <h5 className="faq-question">WILL YOUR INFORMATION BE SHARED WITH ANYONE?</h5>
                        <p className="faq-content">
                            In Short: We do not share any of your informations with anyone.
                            Legal Obligations: We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).
                            Vital Interests: We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.
                            More specifically, we may need to process your data or share your personal information in the following situations:
                            Business Transfers. We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.
                        </p>
                        <h5 className="faq-question">DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</h5>
                        <p className="faq-content">
                            In Short: We may use cookies and other tracking technologies to collect and store your information.
                            We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information. Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Policy.
                        </p>
                        <h5 className="faq-question">HOW LONG DO WE KEEP YOUR INFORMATION?</h5>
                        <p className="faq-content">
                            In Short: We keep your information for as long as necessary to fulfill the purposes outlined in this privacy policy unless otherwise required by law.
                            We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy, unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). No purpose in this policy will require us keeping your personal information for longer than the period of time in which users have an account with us.
                            When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.
                        </p>
                        <h5 className="faq-question">HOW DO WE KEEP YOUR INFORMATION SAFE?</h5>
                        <p className="faq-content">
                            In Short: We aim to protect your personal information through a system of organizational and technical security measures.
                            We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process. However, please also remember that we cannot guarantee that the internet itself is 100% secure. Although we will do our best to protect your personal information, transmission of personal information to and from our Sites is at your own risk. You should only access the services within a secure environment.
                        </p>
                        <h5 className="faq-question">DO WE COLLECT INFORMATION FROM MINORS?</h5>
                        <p className="faq-content">
                            In Short: We do not knowingly collect data from or market to children under 18 years of age.
                            We do not knowingly solicit data from or market to children under 18 years of age. By using the Sites, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the Sites. If we learn that personal information from users less than 18 years of age has been collected, we will deactivate the account and take reasonable measures to promptly delete such data from our records. If you become aware of any data we have collected from children under age 18, please contact us at intimy.shop@gmail.com.
                        </p>
                        <h5 className="faq-question">WHAT ARE YOUR PRIVACY RIGHTS?</h5>
                        <p className="faq-content">
                            In Short: In some regions, such as the European Economic Area, you have rights that allow you greater access to and control over your personal information. You may review, change, or terminate your account at any time.
                            In some regions (like the European Economic Area), you have certain rights under applicable data protection laws. These may include the right (i) to request access and obtain a copy of your personal information, (ii) to request rectification or erasure; (iii) to restrict the processing of your personal information; and (iv) if applicable, to data portability. In certain circumstances, you may also have the right to object to the processing of your personal information. To make such a request, please use the contact details provided below. We will consider and act upon any request in accordance with applicable data protection laws.
                            If we are relying on your consent to process your personal information, you have the right to withdraw your consent at any time. Please note however that this will not affect the lawfulness of the processing before its withdrawal.
                            If you are resident in the European Economic Area and you believe we are unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority. You can find their contact details here: http://ec.europa.eu/justice/data­ protection/bodies/authorities/index_en.htm
                            Account Information
                            If you would at any time like to review or change the information in your account or terminate your account, you can:
                            Log into your account settings and update your user account.
                            Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use and/or comply with legal requirements.
                            Cookies and similar technologies: Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Sites.
                            Opting out of email marketing: You can unsubscribe from our marketing email list at any time by clicking on the unsubscribe link in the emails that we send or by contacting us using the details provided below. You will then be removed from the marketing email list – however, we will still need to send you service­related emails that are necessary for the administration and use of your account.
                        </p>
                        <h5 className="faq-question">CONTROLS FOR DO­NOT­TRACK FEATURES</h5>
                        <p className="faq-content">
                            Most web browsers and some mobile operating systems and mobile applications include a Do­Not­Track (“DNT”) feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. No uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this Privacy Policy.
                        </p>
                        <h5 className="faq-question">DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</h5>
                        <p className="faq-content">
                            In Short: Yes, if you are a resident of California, you are granted specific rights regarding access to your personal information.
                            California Civil Code Section 1798.83, also known as the “Shine The Light” law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.
                        </p>
                        <h5 className="faq-question">DO WE MAKE UPDATES TO THIS POLICY?</h5>
                        <p className="faq-content">
                            In Short: Yes, we will update this policy as necessary to stay compliant with relevant laws.
                            We may update this privacy policy from time to time. The updated version will be indicated by an updated “Revised” date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy policy, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification. We encourage you to review this privacy policy frequently to be informed of how we are protecting your information.
                        </p>
                        <h5 className="faq-question">HOW CAN YOU CONTACT US ABOUT THIS POLICY?</h5>
                        <p className="faq-content">
                            If you have questions or comments about this policy, you may email us at intimy.shop@gmail.com.
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

export default PrivacyPolicy