import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class AccountActivation extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            onMessage: ""
        }
    }

    componentDidMount() {
        document.title = "Account activation - intimy.shop"
        axios.put(`${process.env.REACT_APP_URI}/api/accountactivation`, { id: this.props.match.params.id })
            .then(data => {
                if (data.data === "Seller account activated") {
                    this.setState({
                        onMessage: "Seller",
                        loaded: true
                    })
                }
                else if (data.data === "Buyer account activated") {
                    this.setState({
                        onMessage: "Buyer",
                        loaded: true
                    })
                }
                else {
                    this.setState({
                        loaded: true
                    })
                }
            })
    }

    render() {
        if (this.state.loaded) {
            return (
                <div className="account-activated-container content">
                    {this.state.onMessage === "Seller" ?
                        <div>
                            <div className="success-activated-icon"><i className="fas fa-check-circle"></i></div>
                            <div className="account-activated-title">Your account was successfully activated</div>
                            <div className="account-activated-content">
                                <h3 className="intimy-works">How Intimy works?</h3>
                                <h4 className="start-selling">Start selling</h4>
                                <div className="how-it-works">
                                    <div className="how-it-works-steps">
                                        1. Click on your username on the header or in your profile section "My items".<br />
                                        2. Click on "Post an item".<br />
                                        3. Fill the fields.<br />
                                        4. That's it! You are ready to sell!
                                    </div>
                                    <div className="how-it-works-steps">
                                        Intimy takes 10% of the sales.
                                    </div>
                                    <div className="how-it-works-details">
                                        When someone buy one of your items you will receive a notification and an email where you can see wich item.<br />
                                        The shipping label is filled and payed automaticly.<br />
                                        All you have to do is to click on the "download the shipping label" button, print it, put it on your package, drop the package in a post office.<br />
                                        All done!
                                    </div>
                                    <div>
                                        Don't forget to complete your profile to maximize the chances to sell.
                                    </div>
                                    <div className="how-it-works-note">
                                        Note: The buyers does not see your name, your email or your address. Your privacy is safe!
                                    </div>
                                </div>
                            </div>
                        </div>
                        :
                        this.state.onMessage === "Buyer" ?
                            <div>
                                <div className="success-activated-icon"><i className="fas fa-check-circle"></i></div>
                                <div className="account-activated-title">Your account was successfully activated</div>
                                <div className="account-activated-content">
                                    <h3 className="intimy-works">How Intimy works?</h3>
                                    <h4 className="start-selling">Start buying</h4>
                                    <div className="how-it-works">
                                        <div className="how-it-works-steps">
                                            On Intimy we use tokens to trade the items.<br />
                                            To buy some tokens click on the tokens button then choose a pack of tokens, fill the secure checkout fields.<br />
                                            You are now ready to shop!<br />
                                            Note: You can save your shipping details at this moment or in your profile for a quicker checkout.
                                        </div>
                                        <div>
                                            Intimy takes 5% of the sales.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :
                            <div className="account-already-activated-content">
                                <div className="account-already-activated-title">Account already activated</div>
                            </div>
                    }
                </div>
            )
        }
        else return null
    }
}

export default AccountActivation