//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom'
//Components
import Contact from './contact'
import Profile from './profile'
import MyProfile from './myProfile'
import Home from './home'
import NoMatch from './noMatch'
import ItemToSell from './ItemsToSell/ItemToSell'
import PicturesSet from './ItemsToSell/PicturesSet'
import VideosSet from './ItemsToSell/VideosSet'
import MyItem from './ItemsToSell/MyItem'
import ShoppingBag from './ShoppingBag'
import NotificationsCenter from './NotificationsCenter'
import NotificationsMobile from './NotificationsMobile'
import Broadcast from './live/broadcast'
import Viewer from './live/viewer'
import Live from './live/live'
import VerifId from './verifId'
import AccountActivation from './AccountActivation'
import ConfirmEmail from './ConfirmEmail'
import BackOffice from './back-office/BackOffice'
import Tokens from './Tokens'
import Withdrawal from './Withdrawal'
import PrivateRoute from './PrivateRoute'
import UserConnexionLoader from './UserConnectionLoader'
import FirstTime from './FirstTime'
import Faq from './Faq'
import TermsAndConditions from './TermsAndConditions'
import PrivacyPolicy from './PrivacyPolicy'
import ResetMyPassword from './ResetMyPassword'

class MyRouter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      stateData: ""
    }
  }

  saveState = (stateData) => {
    this.setState({
      stateData: stateData
    })
  }

  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" render={() => <Home saveState={this.saveState} stateData={this.state.stateData} />} />
          <Route exact path="/contact" render={() => <Contact />} />
          <Route exact path="/user/:idseller" render={() => <Profile />} />
          <Route exact path="/myprofile" render={() => <MyProfile />} />
          <Route exact path="/item/:itemId" render={() => <ItemToSell />} />
          <Route exact path="/picturesset/:itemId" render={() => <PicturesSet />} />
          <Route exact path="/videosset/:itemId" render={() => <VideosSet />} />
          <Route exact path="/shoppingbag" render={() => <ShoppingBag />} />
          <Route exact path="/notificationscenter/:notifId?" render={() => <NotificationsCenter />} />
          <Route exact path="/notificationsmobile" render={() => <NotificationsMobile />} />
          <Route exact path="/live" render={() => <Live />} />
          <PrivateRoute isLoggedIn={this.props.connected} exact path="/broadcast" component={Broadcast} />
          <Route exact path="/viewer/:id" render={() => <Viewer />} />
          <Route exact path="/verificationid" render={() => <VerifId />} />
          <Route exact path="/confirmemail/:id" render={() => <ConfirmEmail />} />
          <Route exact path="/activation/:id" component={AccountActivation} />
          <Route exact path="/backoffice" render={() => <BackOffice />} />
          <Route exact path="/tokens" render={() => <Tokens />} />
          <PrivateRoute isLoggedIn={this.props.connected} path="/withdrawal" component={Withdrawal} />
          <Route exact path="/firsttime" render={() => <FirstTime />} />
          <Route exact path="/faq" render={() => <Faq />} />
          <Route exact path="/termsAndConditions" render={() => <TermsAndConditions />} />
          <Route exact path="/privacypolicy" render={() => <PrivacyPolicy />} />
          <Route exact path="/resetmypassword/:id/:token" component={ResetMyPassword} />
          <Route render={() => <NoMatch />} />
        </Switch>
      </div>
    )
  }
}

export default UserConnexionLoader(MyRouter)