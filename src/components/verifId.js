//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import UserConnexionLoader from './UserConnectionLoader'

class VerifId extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: true,
            idVerif: {
                idPhoto: "",
                idFace: ""
            },
            message: ""
        }
    }

    handleFileUpload = e => {
        e.preventDefault()
        const { name } = e.target
        let file = e.target.files[0]
        let reader = new FileReader()
        reader.onload = () => {
            this.setState({
                idVerif: {
                    ...this.state.idVerif,
                    [name]: reader.result
                }
            })
        }
        reader.readAsDataURL(file)
    }

    idVerif = e => {
        e.preventDefault()

        axios.post(`${process.env.REACT_APP_URI}/api/idverif`, { idVerif: this.state.idVerif, userId: this.props.userId }, {
            withCredentials: true
        })
            .then(data => {
                if (data.data == "ID already sent") {
                    this.setState({
                        message: "ID already sent"
                    })
                }
                else if (data.data == "ID sent") {
                    this.setState({
                        message: "ID sent"
                    })
                }
                else if (data.data == "A document is missing") {
                    this.setState({
                        message: "A document is missing"
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        if (!this.props.connected) {
            return <Redirect to="/" />
        }
        return (
            <div className="content">
                <div className="verifId-container">
                    <h2 className="verif-id-title">Verification Id</h2>
                    <p className="verify-id-message">
                        We need to verify your ID in order to provide a quality service with real sellers profiles
                    <br />
                        Documents accepted: ID, passport, driver license.
                </p>
                    <form onSubmit={e => this.idVerif(e)}>
                        <div className="form-group">
                            <label>ID</label>
                            <input type="file" name="idPhoto" className="form-control" onChange={this.handleFileUpload} accept="image/png, image/jpeg" required />
                        </div>
                        <br />
                        <div className="form-group">
                            <label>ID next to your face</label>
                            <input type="file" name="idFace" className="form-control" onChange={this.handleFileUpload} accept="image/png, image/jpeg" required />
                        </div>
                        <button type="submit" className="btn btn-primary intimy-rectangle-button verif-id-button">Submit</button>
                        <br />
                        <div className="verify-id-note">The verification process would be done in less than 24h</div>
                        <br />
                        <span className={`verif-id-message ${this.state.message === "ID sent" ? "success" : "error"}`}>{this.state.message}</span>
                    </form>
                </div>
            </div>
        )
    }
}

export default UserConnexionLoader(VerifId)