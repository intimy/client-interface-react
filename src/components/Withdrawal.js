import React from 'react'
//Components
import UserConnexionLoader from './UserConnectionLoader'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import PopUpLogIn from './PopUp/PopUpLogIn'

class Withdrawal extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            tokens: null,
            amount: "",
            withdrawal: false,
            paypalEmail: ""
        }
    }

    componentDidMount() {
        document.title = "Withdrawal - intimy.shop"
        this.getTokens()
    }

    getTokens = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getTokens`, { withCredentials: true })
            .then((response) => {
                if (response.data) {
                    this.setState({
                        tokens: response.data.tokens,
                        loaded: true
                    })
                }
            })
    }

    handleChange = e => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    submit = e => {
        e.preventDefault()
        axios.post(`${process.env.REACT_APP_URI}/api/withdrawalrequest`, { amount: this.state.amount, paypalEmail: this.state.paypalEmail }, { withCredentials: true })
            .then((response) => {
                if (response.data === "Ok") {
                    let newTokensAmount = this.state.tokens - this.state.amount
                    this.setState({
                        message: "Success, once your request is approuved you should receive your money within 3 business days.",
                        amount: "",
                        tokens: newTokensAmount
                    })
                }
                else if (response.data === "Request already pending") {
                    this.setState({
                        message: "You already have a request pending.",
                        amount: ""
                    })
                }
                else if (response.data === "Not enough tokens") {
                    this.setState({
                        message: "Not enough tokens.",
                        amount: ""
                    })
                }
            })
    }

    onWithdraw = () => {
        this.setState({
            withdrawal: !this.state.withdrawal
        })
    }

    render() {
        if (this.state.loaded) {
            return (
                <div className="text-center content">
                    <h3 className="ShoppingBagTitle">Withdraw informations</h3>
                    <br />
                    <p className="withdraw-infos">
                        We use Paypal to process the withdraw.<br />
                        Please enter your Paypal email address to be able to receive your money.
                    </p>
                    <br />
                    <div className="withdrawal-request-form">
                        <label htmlFor="paypal-email">Paypal email address</label>
                        <br />
                        <input className="withdrawal-request-input" type="text" name="paypalEmail" id="paypal-email" value={this.state.paypalEmail} onChange={this.handleChange} />
                        <br />
                        <button className="customButton" onClick={this.onWithdraw}>Next</button>
                    </div>
                    {this.state.withdrawal && this.state.paypalEmail &&
                        <div>
                            <p className="withdrawal-request-available">Available tokens: {this.state.tokens}</p>
                            <p className="withdrawal-request-minimum">Minimum to withdraw: 500</p>
                            <form className="withdrawal-request-form" onSubmit={this.submit}>
                                <input className="withdrawal-request-input" type="number" step="1" name="amount" id="withdraw-amount" min="500" max={this.state.tokens} value={this.state.amount} onChange={this.handleChange} required />
                                <span>{this.state.amount && <span> ${this.state.amount / 10}</span>}</span>
                                <button className="customButton">OK</button>
                            </form>
                            <div className="withdrawal-request-message" id="withdrawal-request-message">{this.state.message}</div>
                        </div>
                    }
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(Withdrawal)