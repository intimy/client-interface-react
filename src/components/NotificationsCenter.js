//React
import React, { Component } from 'react'
//Router
import { Redirect, Link, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import classNames from 'classnames'
import moment from 'moment'
import userConnexionLoader from './UserConnectionLoader'

class NotificationsCenter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      notifications: [],
      notificationsData: [],
      notificationNumber: 0,
      toggleDropdown: false,
      currentNotification: "",
      disableBtn: true,
      isLoaded: false
    }
  }

  componentDidMount() {
    document.title = "Notifications - intimy.shop"
    if (this.props.connected) this.getNotifications()
  }

  componentDidUpdate() {
    this.notificationViewed()
  }

  getNotifications() {
    axios.get(`${process.env.REACT_APP_URI}/api/getnotifications`, { withCredentials: true })
      .then(response => {
        const notificationsLenght = response.data.notifs.length
        const { buyers, produits, produitsPictures, produitsVideos, notifs, directSellProducts } = response.data
        const notificationsData = notifs.map(notification => {
          let buyer
          let produitsFound
          if (notification.notifType === "itemBought") {
            buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
            let oldProduitsFound = produits.filter(produit => {
              return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
            })
            let directSellProductsFound = directSellProducts.filter(produit => {
              return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
            })
            produitsFound = oldProduitsFound.concat(directSellProductsFound)
          }
          if (notification.notifType === "itemPicturesBought") {
            buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
            let oldProduitsFound = produitsPictures.filter(produit => {
              return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
            })
            let directSellProductsFound = directSellProducts.filter(produit => {
              return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
            })
            produitsFound = oldProduitsFound.concat(directSellProductsFound)
          }
          if (notification.notifType === "itemVideosBought") {
            buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
            let oldProduitsFound = produitsVideos.filter(produit => {
              return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
            })
            let directSellProductsFound = directSellProducts.filter(produit => {
              return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
            })
            produitsFound = oldProduitsFound.concat(directSellProductsFound)
          }
          return { ...notification, buyer, produitsFound }
        })
        this.setState({
          notifications: notifs,
          notificationNumber: notificationsLenght,
          notificationsData: notificationsData,
          isLoaded: true
        }, () => {
          this.notificationViewed()
        })
      })
  }

  notificationViewed() {
    if (this.state.notificationsData != "") {
      let data = this.state.notificationsData
      let index
      if (this.props.match.params) index = data.findIndex(p => p._id == this.props.match.params.notifId)
      else index = data.findIndex(p => p._id == data[data.length - 1]._id)
      if (index == -1) {
        this.props.history.push('/')
      }
      else {
        if (data[index].seen == false) {
          data[index].seen = true
          this.setState({
            ...this.state.notificationsData,
            notificationsData: data
          })
          axios.put(`${process.env.REACT_APP_URI}/api/notificationViewed`, { notifId: this.props.match.params.notifId }, { withCredentials: true })
        }
      }
    }
  }

  dSLClick = e => {
    if (this.state.disableBtn) e.preventDefault()
  }

  handleCheckboxShippingInfos = () => {
    this.setState({
      disableBtn: !this.state.disableBtn
    })
  }

  render() {
    if (!this.props.connected) return <Redirect to="/" />
    if (this.state.isLoaded) {
      if (this.state.notificationsData != "") {
        let currentNotification
        if (this.props.match.params.notifId) {
          currentNotification = this.state.notificationsData.find(items => items._id == this.props.match.params.notifId)
        }
        else {
          currentNotification = this.state.notificationsData.find(items => items._id == this.state.notificationsData[this.state.notificationsData.length - 1]._id)
        }
        let currentNotificationProduits
        let currentNotificationIdVerif
        if (currentNotification.notifType === "itemBought") {
          currentNotificationProduits = currentNotification.produitsFound.map((produit, index) =>
            <div key={index}>
              <img src={produit.img} alt="Profil picture buyer" className="image-current-notif" /> <p className="name-item-sold-notif-center">{produit.name} is sold</p>
              <br />
              <span>Bought by {currentNotification.buyer.username}</span>
              <br />
              <span>Order placed {moment(produit.date).format('MMMM DD, YYYY')}</span>
              <br />
              <div>Total <span>{produit.price} <i className="fas fa-coins"></i> | ${produit.price / 10}</span></div>
              <br />
              <a className={`btn-download-shipping-label ${this.state.disableBtn ? "isDisabled" : ""}`} href={this.state.disableBtn ? "" : currentNotification.itemBoughtData.shippingLabelLink} onClick={(e) => this.dSLClick(e)} role="button">Download shipping label</a>
              <br />
              <div className="read-shipping-informations-checkbox-container">
                <input type="checkbox" name="readShippingInformations" id="read-shipping-informations" onChange={this.handleCheckboxShippingInfos} checked={!this.state.disableBtn} />
                <label className="read-shipping-informations-checkbox" htmlFor="read-shipping-informations"> I have read the shipping instructions</label>
              </div>
              <div className="shipping-instructions-container">
                <h3>Shipping Instructions</h3>
                <br />
                <div className="shipping-instructions-content">
                  <p>
                    To ship your package, you need to get "Priority mail padded flat rates envelope" from USPS, they will be FREE.<br />
                    You can find them on <a className="blue-link" href="https://store.usps.com/store/product/shipping-supplies/priority-mail-padded-flat-rate-envelope-P_EP14PE">USPS website </a>
                    or any USPS post office near you.<br />
                    Make sure to choose the right size and type of envelope (check the picture below), otherwise the package could be returned or your shipping rate could be increased.
                  <br />
                    <br />
                    Download, print and put the shipping label on your package and drop it off at a post office.
                  <br />
                    <br />
                  </p>
                </div>
                <img className="shipping-instructions-img" src="/images/padded.png" alt="Padded envelope" />
              </div>
            </div>
          )
        }
        if (currentNotification.notifType === "itemPicturesBought") {
          currentNotificationProduits = currentNotification.produitsFound.map((produit, index) =>
            <div key={index}>
              <img src={produit.img} alt="Notification picture" className="image-current-notif" /> <p className="name-item-sold-notif-center">{produit.name} is sold</p>
              <br />
              <span>Bought by {currentNotification.buyer.username}</span>
              <br />
              <span>Order placed {moment(produit.date).format('MMMM DD, YYYY')}</span>
              <br />
              <div>Total <span>{produit.price} <i className="fas fa-coins"></i> | ${produit.price / 10}</span></div>
              <br />
            </div>
          )
        }
        if (currentNotification.notifType === "itemVideosBought") {
          currentNotificationProduits = currentNotification.produitsFound.map((produit, index) =>
            <div key={index}>
              <img src={produit.img} alt="Notification picture" className="image-current-notif" /> <p className="name-item-sold-notif-center">{produit.name} is sold</p>
              <br />
              <span>Bought by {currentNotification.buyer.username}</span>
              <br />
              <span>Order placed {moment(produit.date).format('MMMM DD, YYYY')}</span>
              <br />
              <div>Total <span>{produit.price} <i className="fas fa-coins"></i> | ${produit.price / 10}</span></div>
              <br />
            </div>
          )
        }
        const notifications = this.state.notificationsData.map((items, index) => {
          const notificationStyle = classNames('notifications', {
            'notifications-background': items.seen == false,
          })
          return (
            <div key={index}>
              {items.notifType === "itemBought" &&
                <Link to={`/notificationscenter/${items._id}`}>
                  <div className={notificationStyle}>
                    {items.produitsFound.map(produits => <span key={produits._id}>
                      <div className="img-notif">
                        <img src={produits.img} alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">{produits.name} is sold</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </span>)}
                  </div>
                </Link>
              }
              {items.notifType === "itemPicturesBought" &&
                <Link to={`/notificationscenter/${items._id}`}>
                  <div className={notificationStyle}>
                    {items.produitsFound.map(produits => <span key={produits._id}>
                      <div className="img-notif">
                        <img src={produits.img} alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">{produits.name} is sold</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </span>)}
                  </div>
                </Link>
              }
              {items.notifType === "itemVideosBought" &&
                <Link to={`/notificationscenter/${items._id}`}>
                  <div className={notificationStyle}>
                    {items.produitsFound.map(produits => <span key={produits._id}>
                      <div className="img-notif">
                        <img src={produits.img} alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">{produits.name} is sold</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </span>)}
                  </div>
                </Link>
              }
              {items.notifType === "verifId" &&
                <Link to={`/notificationscenter/${items._id}`}>
                  {items.verifId.stateVerification ?
                    <div className={notificationStyle}>
                      <div className="img-notif">
                        <img src="/images/logo.svg" alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">Id approuved</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </div>
                    :
                    <div className={notificationStyle}>
                      <div className="img-notif">
                        <img src="/images/logo.svg" alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">Id rejected for reason:<br />{items.verifId.reason}</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </div>
                  }
                </Link>
              }
              {items.notifType === "withdrawRequest" &&
                <Link to={`/notificationscenter/${items._id}`}>
                  {items.withdrawRequest.validated ?
                    <div className={notificationStyle}>
                      <div className="img-notif">
                        <img src="/images/logo.svg" alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">Withdrawal approuved</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </div>
                    :
                    <div className={notificationStyle}>
                      <div className="img-notif">
                        <img src="/images/logo.svg" alt="Notification picture" className="image-notif" />
                      </div>
                      <div className="text-notif">
                        <div className="text-notif-content"><p className="text-notif-content-description">Withdrawal rejected for reason: {items.withdrawRequest.reason}</p><span className="notifs-time-ago">{moment(items.date).short()}</span></div>
                      </div>
                    </div>
                  }
                </Link>
              }
            </div>
          )
        }).reverse()
        return (
          <div className="container-notifs content">
            <div className="container-notifs-center">
              {notifications}
            </div>
            {currentNotification.produitsFound &&
              <div className="container-notif-center">
                {currentNotificationProduits}
              </div>
            }
            {currentNotification.notifType === "verifId" &&
              <div className="container-notif-center">
                {currentNotification.verifId.stateVerification ?
                  <div>
                    <div className="tokens-added-icon">
                      <i className="fas fa-check-circle"></i>
                    </div>
                    <p className="tokens-added-message">Success!<br />Your ID has been verified<br />You can now sell and broadcast!<br />Enjoy!</p>
                  </div>
                  :
                  <div>
                    <div className="rejected-notif-icon">
                      <i className="fas fa-times-circle"></i>
                    </div>
                    <p className="tokens-added-message">Sorry!<br />Your ID has been rejected for reason:<br />{currentNotification.verifId.reason}</p>
                  </div>
                }
              </div>
            }
            {currentNotification.notifType === "withdrawRequest" &&
              <div className="container-notif-center">
                {currentNotification.withdrawRequest.validated ?
                  <div>
                    <div className="tokens-added-icon">
                      <i className="fas fa-check-circle"></i>
                    </div>
                    <p className="tokens-added-message">Success!<br />Your withdrawal request has been approuved<br />You should receive the money on your bank account within 2 business days!</p>
                  </div>
                  :
                  <div>
                    <div className="rejected-notif-icon">
                      <i className="fas fa-times-circle"></i>
                    </div>
                    <p className="tokens-added-message">Sorry!<br />Your withdrawal request has been rejected for reason:<br />{currentNotification.withdrawRequest.reason}</p>
                  </div>
                }
              </div>
            }
          </div>
        )
      }
      else {
        return <div className="content">
          <p className="no-notifs-center-yet">No notifications yet</p>
        </div>
      }
    }
    else return null
  }
}

export default userConnexionLoader(withRouter(NotificationsCenter))