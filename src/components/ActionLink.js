//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

const HOC = Comp => {
  return class HOC extends Component {
    render() {
      return <Route children={props => <Comp {...props} {...this.props} />} />
    }
  }
}

class ActionLink extends Component {
  redirectTo = () => {
    const {history} = this.props
    if (this.props.action) this.props.action()
    history.push(this.props.to)
  }

  render() {
    return (
      <a href="javascript:void(0)" onClick={this.redirectTo}>
        {this.props.children}
      </a>
    )
  }
}

export default HOC(ActionLink)