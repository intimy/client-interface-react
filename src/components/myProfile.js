//React
import React, { Component } from 'react'
//Router
import { Link, Redirect, withRouter } from 'react-router-dom'
//Components
import PopUpModifyMyProfile from './PopUp/PopUpModifyMyProfile'
import PopUpPostItem from './PopUp/PopUpPostItem'
import UserConnexionLoader from './UserConnectionLoader'
import { withCookies, Cookies } from 'react-cookie'
import { instanceOf } from 'prop-types'
import axios from 'axios'
import MyItemsToSell from './ItemsToSell/MyItemsToSell'
import Gallery from './gallery/gallery'
import AboutMe from './myProfileComponents/AboutMe'
import MyItems from './myProfileComponents/MyItems'
import ItemsSold from './myProfileComponents/ItemsSold'
import ItemsBought from './myProfileComponents/ItemsBought'
import MyFavorites from './myProfileComponents/MyFavorites'

class MyProfile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      showModify: false,
      profilData: [],
      picture: null,
      messages: {
        messageProfilUpdated: ""
      },
      isLoaded: false,
      options: {
        shareEl: false,
        bgOpacity: 0.5
      },
      isActive: "aboutMe"
    }
  }

  componentDidMount() {
    document.title = "Account - intimy.shop"
    this.getData()
  }

  toggleModify = (content) => {
    this.setState({ showModify: content })
  }

  closePopUpModify = () => {
    this.setState({ showModify: false })
  }

  togglePostItem = (content) => {
    this.setState({ showPostItem: content })
  }

  getData = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getprofildata`, { params: { userStatus: this.props.userStatus }, withCredentials: true })
      .then((response) => {
        this.setState({
          profilData: response.data,
          isLoaded: true
        })
      })
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => {
    this.setState({
      showModify: false
    })
  }

  setActive = e => {
    this.setState({
      isActive: e
    })
  }

  profileModified = e => {
    this.getData()
  }

  render() {
    if (!this.props.connected) this.props.history.push("/")
    if (this.state.isLoaded) {
      return (
        <div className="content">
          {this.props.connected && this.props.userStatus == 'seller' &&
            <div className="profile-container">
              <div className="profile-bio-container">
                <div className="profile-picture-container">
                  <img src={this.state.profilData.profilPicture} alt="Profil picture" className="picture-profile" />
                </div>
                {this.state.profilData.pictures.length > 0 &&
                  <div className="pictures-user-container">
                    <Gallery items={this.state.profilData} userId={this.props.userId} isAdmin={true} />
                  </div>
                }
                <ul className="navbar-my-profile-container">
                  <li className={`navbar-my-profile-item ${this.state.isActive === "aboutMe" ? "active" : ""}`} onClick={() => this.setActive("aboutMe")}>About me</li>
                  <li className={`navbar-my-profile-item ${this.state.isActive === "myItems" ? "active" : ""}`} onClick={() => this.setActive("myItems")}>My Items</li>
                  <li className={`navbar-my-profile-item ${this.state.isActive === "itemsSold" ? "active" : ""}`} onClick={() => this.setActive("itemsSold")}>Items sold</li>
                  <li className={`navbar-my-profile-item ${this.state.isActive === "itemsBought" ? "active" : ""}`} onClick={() => this.setActive("itemsBought")}>Items bought</li>
                  <li className={`navbar-my-profile-item ${this.state.isActive === "myFavorites" ? "active" : ""}`} onClick={() => this.setActive("myFavorites")}>My favorites</li>
                </ul>
              </div>

              {this.state.isActive === "aboutMe" && <AboutMe userStatus={this.props.userStatus} profileModified={this.profileModified} />}
              {this.state.isActive === "myItems" && <MyItems userStatus={this.props.userStatus} verifiedUser={this.state.profilData.verifiedUser} />}
              {this.state.isActive === "itemsSold" && <ItemsSold userStatus={this.props.userStatus} />}
              {this.state.isActive === "itemsBought" && <ItemsBought userStatus={this.props.userStatus} />}
              {this.state.isActive === "myFavorites" && <MyFavorites userStatus={this.props.userStatus} />}

            </div>
          }
          {this.props.connected && this.props.userStatus == 'buyer' &&
            <div className="profile-container">
              <div className="profile-bio-container">
                <div className="profile-picture-container">
                  <img src={this.state.profilData.profilPicture} alt="Profil picture" className="picture-profile" />
                </div>
                <ul className="navbar-my-profile-container">
                  <li className={`navbar-my-profile-item ${this.state.isActive === "aboutMe" ? "active" : ""}`} onClick={() => this.setActive("aboutMe")}>About me</li>
                  <li className={`navbar-my-profile-item ${this.state.isActive === "itemsBought" ? "active" : ""}`} onClick={() => this.setActive("itemsBought")}>Items bought</li>
                  <li className={`navbar-my-profile-item ${this.state.isActive === "myFavorites" ? "active" : ""}`} onClick={() => this.setActive("myFavorites")}>My favorites</li>
                </ul>
              </div>

              {this.state.isActive === "aboutMe" && <AboutMe userStatus={this.props.userStatus} profileModified={this.profileModified} />}
              {this.state.isActive === "itemsBought" && <ItemsBought userStatus={this.props.userStatus} />}
              {this.state.isActive === "myFavorites" && <MyFavorites userStatus={this.props.userStatus} />}

            </div>
          }
        </div>
      )
    }
    else return null
  }
}

export default UserConnexionLoader(withRouter(MyProfile))