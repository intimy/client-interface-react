//React
import React from 'react'
//Router
import { withRouter, Link } from 'react-router-dom'
//Components
import HeaderNotConnected from './HeaderNotConnected'
import HeaderConnected from './HeaderConnected'
import LogoImg from '../images/logo.svg'
import userConnexionLoader from './UserConnectionLoader'
import SocketContext from './react-context/SocketContext'

class Header extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      header: true,
      mobileSearchOn: false
    }
    this.header = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('scroll', this.scroll, false)
    if (this.props.history.location.pathname === "/") {
      this.setState({
        header: false
      })
    }
  }

  componentWillReceiveProps() {
    if (this.props.history.location.pathname === "/" && !this.state.mobileSearchOn) {
      this.setState({
        header: false
      })
    }
    else {
      this.setState({
        header: true
      })
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.scroll, false)
  }

  scroll = e => {
    if (this.props.history.location.pathname === "/" && !this.state.mobileSearchOn) {
      if (window.scrollY > 45 || window.pageYOffset > 45 || document.body.scrollTop > 45 || document.documentElement.scrollTop > 45) {
        this.setState({
          header: true
        })
      }
      else {
        this.setState({
          header: false
        })
      }
    }
    else {
      this.setState({
        header: true
      })
    }
  }

  mobileSearchOn = e => {
    if (this.props.history.location.pathname === "/") {
      if (window.scrollY > 45 || window.pageYOffset > 45 || document.body.scrollTop > 45 || document.documentElement.scrollTop > 45) {
        this.setState({
          mobileSearchOn: !this.state.mobileSearchOn
        })
      }
      else {
        this.setState({
          header: e,
          mobileSearchOn: !this.state.mobileSearchOn
        })
      }
    }
    else {
      this.setState({
        mobileSearchOn: !this.state.mobileSearchOn
      })
    }
  }

  render() {
    return (
      <div>
        <header className={`header ${this.state.header ? "" : "header-up"}`} ref={this.header}>
          <div className="header-content">
            <div className="header-title"><Link className="link-header" to="/">Intimy</Link></div>
            <div className="logo-container">
              <Link className="link-header" to="/"><img src={LogoImg} alt="logo" className="LogoImg" /></Link>
            </div>
            {!this.props.connected && <HeaderNotConnected mobileSearchOn={this.mobileSearchOn} />}
            {this.props.connected && <SocketContext.Consumer>{socket => <HeaderConnected mobileSearchOn={this.mobileSearchOn} socket={socket} />}</SocketContext.Consumer>}
          </div>
        </header>
      </div>
    )
  }
}

export default userConnexionLoader(withRouter(Header))