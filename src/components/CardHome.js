import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import SearchBarStore from "./stores/SearchBarStore"
import { observer } from 'mobx-react'
import moment from 'moment-shortformat'

class CardHome extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      totalLength: null,
      cards: [],
      idAlreadyGot: [],
    }
    this.bio = React.createRef()
  }

  componentDidUpdate(prevProps, nextProps) {
    if (prevProps != this.props) {
      this.getData()
    }
  }

  componentDidMount() {
    if (this.props.stateData) {
      this.setState({
        loaded: this.props.stateData.loaded,
        totalLength: this.props.stateData.totalLength,
        cards: this.props.stateData.cards,
        idAlreadyGot: this.props.stateData.idAlreadyGot,
      })
    }
    else this.getData()
  }

  componentWillUnmount() {
    this.props.saveData(this.state)
  }

  getData = e => {
    let { cards } = this.state
    let { idAlreadyGot } = this.state
    axios.get(`${process.env.REACT_APP_URI}/api/getHomeCards`, { params: { skipSize: cards.length, idAlreadyGot: idAlreadyGot } })
      .then(async response => {
        if (response.data.sellers) {
          let idAlreadyGot = await response.data.sellers.map(data => data._id)
          this.setState({
            cards: [...cards, ...response.data.sellers],
            totalLength: response.data.totalLength,
            loaded: true,
            idAlreadyGot: [...this.state.idAlreadyGot, ...idAlreadyGot]
          })
        }
      })
  }

  backResults = e => {
    this.props.searchForm([])
    SearchBarStore.clearString()
  }

  moreResults() {
    this.getData()
  }

  render() {
    if (this.state.loaded) {
      const cards = this.state.cards
        .map((data) =>
          <div className="post-home" id={data._id} key={data._id}>
            <Link to={`user/${data._id}`}>
              <div className="post-home-container-picture">
                {moment(moment()).diff(data.date, 'days') < 7 && <div className="new-badge"><span className="text-new-badge">NEW</span></div>}
                <img src={data.profilPicture} alt="" className="pictures-posts-home" />
              </div>
              <p className="post-home-pseudo no-margin">{data.username}</p>
            </Link>
            {
              data.live === true &&
              <Link to={`viewer/${data._id}`}>
                <p className="text-center">On live</p>
              </Link>
            }
          </div >
        )
      let testSearch = []
      if (this.props.searchFormProps.length) testSearch = this.props.searchFormProps
      if (SearchBarStore.searchString.length) testSearch = SearchBarStore.searchString

      const searchFormProps = testSearch.map((data) =>
        <div className="post-home" id={data._id} key={data._id}>
          <Link to={`user/${data._id}`}>
            <div className="post-home-container-picture">
              {moment(moment()).diff(data.date, 'days') < 7 && <div className="new-badge"><span className="text-new-badge">NEW</span></div>}
              <img src={data.profilPicture} alt="" className="pictures-posts-home" />
            </div>
            <p className="post-home-pseudo no-margin">{data.username}</p>
          </Link>
          {data.live === true &&
            <Link to={`viewer/${data._id}`}>
              <p className="text-center">On live</p>
            </Link>
          }
        </div>
      )
      return <div>
        {(this.props.searchFormProps.length > 0 || SearchBarStore.searchString.length) > 0 && <span onClick={this.backResults} className="left-arrow-search-results"><i className="fas fa-arrow-circle-left left-arrow-searchForm"></i></span>}
        <div>
          {this.props.searchFormProps.length || SearchBarStore.searchString.length ? searchFormProps : cards}
          {this.state.cards.length < this.state.totalLength &&
            <div className="more-button-home-container">
              <button onClick={() => this.moreResults()} className="intimy-rectangle-button">More</button>
            </div>
          }
        </div>
      </div>
    }
    else return null
  }
}

export default withRouter(observer(CardHome))