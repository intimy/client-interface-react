//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import axios from 'axios'

class ItemsToSell extends Component {

  constructor(props) {
    super(props)
    this.state = {
      itemsToSell: [],
      itemsToSellPictures: [],
      itemsToSellVideos: []
    }
  }

  componentDidMount() {
    this._mounted = true
    this.getItemsToSell()
  }

  componentWillUnmount() {
    this._mounted = false
  }

  getItemsToSell = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemstoSell`, { params: { sellerId: this.props.sellerId, directSell: false } })
      .then((response) => {
        if (this._mounted) {
          this.setState({
            itemsToSell: response.data.produits,
            itemsToSellPictures: response.data.produitsPictures,
            itemsToSellVideos: response.data.produitsVideos,
          })
        }
      })
  }

  render() {
    const itemsToSell = this.state.itemsToSell.map((data) =>
      <div className="post-user" key={data._id}>
        <Link to={`/item/${data._id}`}>
          <div className="post-user-container-picture">
            <img src={data.img} alt="" className="pictures-posts-home" />
          </div>
          <p className="item-name-user no-margin">{data.name}</p>
          <p className="item-price-user">{data.price} tokens</p>
        </Link>
        {data.live === true &&
          <Link to={`viewer/${data._id}`}>
            <p className="text-center">On live</p>
          </Link>
        }
      </div>
    ).reverse()
    const itemsToSellPictures = this.state.itemsToSellPictures.map((data) =>
      <div className="post-user" key={data._id}>
        <Link to={`/picturesset/${data._id}`}>
          <div className="post-user-container-picture">
            <img src="http://localhost:8080/images/picturesSet.svg" alt="Item Picture" className="pictures-posts-home" />
            <p className="item-pictures-title-user">{data.numberPictures} Picture{data.numberPictures > 1 && "s"}</p>
          </div>
          <p className="item-name-user no-margin">{data.name}</p>
          <p className="item-price-user">{data.price} <i className="fas fa-coins"></i></p>
        </Link>
        {data.live === true &&
          <Link to={`viewer/${data._id}`}>
            <p className="text-center">On live</p>
          </Link>
        }
      </div>
    ).reverse()
    const itemsToSellVideos = this.state.itemsToSellVideos.map((data) =>
      <div className="post-user" key={data._id}>
        <Link to={`/videosset/${data._id}`}>
          <div className="post-user-container-picture">
            <img src="http://localhost:8080/images/videosSet.svg" alt="Item Picture" className="pictures-posts-home" />
            <p className="item-pictures-title-user">{data.numberVideos} Video{data.numberVideos > 1 && "s"}</p>
          </div>
          <p className="item-name-user no-margin">{data.name}</p>
          <p className="item-price-user">{data.price} <i className="fas fa-coins"></i></p>
        </Link>
        {data.live === true &&
          <Link to={`viewer/${data._id}`}>
            <p className="text-center">On live</p>
          </Link>
        }
      </div>
    ).reverse()
    return <div className="items-to-sell-profile">
      <h3 className="profile-items-title">Items</h3>
      <div>
        {this.state.itemsToSell.length > 0 || this.state.itemsToSellPictures.length > 0 || this.state.itemsToSellVideos.length > 0 ?
          <div>
            {itemsToSell}
            {itemsToSellPictures}
            {itemsToSellVideos}
          </div>
          :
          <p className="text-center">No items yet</p>
        }
      </div>
    </div>
  }
}

export default ItemsToSell;