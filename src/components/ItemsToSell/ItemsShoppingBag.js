import React from 'react'
import { Link } from 'react-router-dom'
import UserConnexionLoader from '../UserConnectionLoader'
import axios from 'axios'
import { observer } from 'mobx-react'
import ShoppingBagStore from "../stores/ShoppingBagStore"

class ShoppingBag extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      messages: {
        itemRemove: ""
      }
    }
  }

  removeItem = e => {
    if (this.props.connected) {
      let self = this
      axios.put(`${process.env.REACT_APP_URI}/api/removefrommyshoppingbag`, { itemId: e }, {
        withCredentials: true
      })
        .then(data => {
          if (data.data === 'Item removed') {
            let dataItems = this.props.dataItems
            var index = dataItems.findIndex(p => p._id === e)
            dataItems.splice(index, 1)
            this.props.newDataItems(dataItems)
          }
        })
    }
    else {
      let dataItems = this.props.dataItems
      var index = dataItems.findIndex(p => p._id === e)
      dataItems.splice(index, 1)
      this.props.newDataItems(dataItems)
      ShoppingBagStore.clearItem(index)
    }
  }

  render() {
    const items = this.props.dataItems.map((items) =>
      <div key={items._id}>
        <div className="DetailsShoppingBagItem">
          <div className="PictureShoppingBagItems">
            <img src={items.img} alt="Item picture" className="ItemPicture" />
          </div>
          <li className="TitleShoppingBagItem">{items.name}</li>
          <li className="PriceShoppingBagItem">{items.price} tokens</li>
          <span onClick={() => this.removeItem(items._id)}><i className="fa fa-times fa-lg RemoveItemShoppingBag" aria-hidden="true"></i></span>
        </div>
        {this.props.itemNotAvailable && items.bought === true && <p className="itemNotAvailable">This item is not available anymore</p>}
        <hr className="ShoppingBagHr" />
      </div>
    )
    return <div>
      {items}
    </div>
  }
}

export default UserConnexionLoader(observer(ShoppingBag))