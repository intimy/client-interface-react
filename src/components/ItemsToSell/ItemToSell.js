//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import UserConnexionLoader from '../UserConnectionLoader'
import moment from 'moment-shortformat'
import PopUpLogIn from '../PopUp/PopUpLogIn'
import Gallery from '../gallery/gallery'
import { observer } from 'mobx-react'
import ShoppingBagStore from "../stores/ShoppingBagStore"

class ItemToSell extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      itemToSell: "",
      messages: {
        itemShoppingBag: ""
      },
      comments: null,
      comment: "",
      popUpLogIn: false
    }
    this.postInput = React.createRef()
    this.blocMessages = React.createRef()
  }

  async componentDidMount() {
    await this.getItemToSell()
    this.getItemComments()
  }

  updateScroll() {
    this.blocMessages.current.scrollTop = this.blocMessages.current.scrollHeight
  }

  componentDidUpdate() {
    if (this.props.isActive === "timeline") this.updateScroll()
  }

  handleChange = e => {
    this.setState({
      comment: e.target.value
    })
  }

  getItemToSell = async e => {
    await axios.get(`${process.env.REACT_APP_URI}/api/getitemtoSell`, { params: { itemId: this.props.match.params.itemId } })
      .then((response) => {
        if (response.data == "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          this.setState({
            itemToSell: response.data.produits,
            itemToSellPictures: response.data.produitsPictures
          })
          document.title = `${response.data.produits.name} - intimy.shop`
        }
      })
  }

  getItemComments = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemcomments`, { params: { itemId: this.props.match.params.itemId } })
      .then(comments => {
        this.setState({
          comments: comments.data,
          loaded: true
        }, () => {
          this.updateScroll()
        })
      })
  }

  addToMyShoppingBag = e => {
    if (this.props.connected) {
      window.gtag('event', `Add to shopping bag`, {
        'event_category': 'user',
        'event_label': `Connected user`
      })
      axios.post(`${process.env.REACT_APP_URI}/api/addtomyshoppingbag`, { itemId: e, connected: true }, {
        withCredentials: true
      })
        .then(data => {
          if (data.data == 'This item is already in your shopping bag') {
            this.setState({
              messages: {
                itemShoppingBag: data.data
              }
            })
          }
          else {
            this.setState({
              messages: {
                itemShoppingBag: 'Item added'
              }
            })
          }
        })
        .catch(err => {
          throw err
        })
    }
    else {
      if (!ShoppingBagStore.shoppingBag.includes(e)) {
        ShoppingBagStore.addItem(e)
        this.setState({
          messages: {
            itemShoppingBag: 'Item added'
          }
        })
      }
      else {
        this.setState({
          messages: {
            itemShoppingBag: "This item is already in your shopping bag"
          }
        })
      }
      window.gtag('event', `Add to shopping bag`, {
        'event_category': 'user',
        'event_label': `Not connected user`
      })
    }
  }

  addPost = e => {
    e.preventDefault()
    if (this.props.connected) {
      axios.post(`${process.env.REACT_APP_URI}/api/additemcomment`, { itemId: this.props.match.params.itemId, comment: this.state.comment, sellerId: this.state.itemToSell.sellerId }, { withCredentials: true })
        .then(response => {
          if (response.data.status === 'Comment added') {
            this.setState({
              comment: "",
              comments: [...this.state.comments, response.data.comment]
            })
            this.postInput.current.blur()
            this.updateScroll()
            window.gtag('event', `Item comment posted`, {
              'event_category': 'user'
            })
          }
          else if (response.data === 'Comment not added') {

          }
        })
    }
    else {
      this.setState({
        popUpLogIn: "to post"
      })
    }
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    popUpLogIn: false
  })

  render() {
    if (this.state.loaded) {
      const comments = this.state.comments.map(comment =>
        <div key={comment._id} className="bloc-messages-content">
          {comment.userId === this.props.userId ?
            <div>
              <div className="bloc-message-container-uploader">
                <div className="bloc-message-uploader">
                  <span className="username-wall">{comment.username}</span>
                  <p className="message-wall">{comment.comment}</p>
                </div>
                <div className="date-wall"> {moment(comment.date).short()}</div>
              </div>
            </div>
            :
            <div>
              <div className="bloc-message-container-users">
                <div className="bloc-message-users">
                  <span className="username-wall">{comment.username}</span>
                  <p className="message-wall">{comment.comment}</p>
                </div>
                <div className="date-wall"> {moment(comment.date).short()}</div>
              </div>
            </div>
          }
        </div>
      )
      return (
        <div className="content">
          {this.state.itemToSell &&
            <div>
              <div className="item-container">
                <div className="profile-picture-container">
                  <img src={this.state.itemToSell.img} alt="Item picture" className="item-picture" />
                </div>
                {this.state.itemToSell.pictures.length > 0 &&
                  <div className="pictures-user-container">
                    <Gallery handleOutsideClick={this.handleOutsideClick} items={this.state.itemToSell} userId={this.props.userId} />
                  </div>
                }
                <br />
                <p className="item-to-sell-name">{this.state.itemToSell.name}</p>
                <p className="item-to-sell-description">{this.state.itemToSell.description}</p>
                <p className="item-price">{this.state.itemToSell.price} tokens</p>
                {this.state.itemToSell.sellerId !== this.props.userId && <button onClick={() => this.addToMyShoppingBag(this.state.itemToSell._id)} className="btn btn-primary intimy-rectangle-button">Add to my bag</button>}
                <br />
                <p className="item-to-sell-message">{this.state.messages.itemShoppingBag}</p>
              </div>
              <div className="wall-container">
                <div className="bloc-messages" ref={this.blocMessages}>
                  {comments}
                </div>
                <div>
                  <form className="send-comment-form" onSubmit={this.addPost}>
                    <input className="post-input" ref={this.postInput} type="text" onChange={this.handleChange} value={this.state.comment} placeholder="What's on your mind?" required />
                    <button className="send-post-button intimy-rectangle-button" type="submit">Send</button>
                  </form>
                </div>
                {this.state.popUpLogIn &&
                  <div className="logInHeader">
                    <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction={this.state.popUpLogIn} />
                  </div>
                }
              </div>
            </div>
          }
          {this.state.itemToSellPictures &&
            <div>
              <div className="item-container">
                <div className="profile-picture-container">
                  <img src={this.state.itemToSellPictures.img} alt="Item picture" className="item-picture" />
                </div>
                {this.state.itemToSellPictures.pictures.length > 0 &&
                  <div className="pictures-user-container">
                    <Gallery handleOutsideClick={this.handleOutsideClick} items={this.state.itemToSellPictures} userId={this.props.userId} />
                  </div>
                }
                <br />
                <p className="item-to-sell-name">{this.state.itemToSellPictures.name}</p>
                <p className="item-to-sell-description">{this.state.itemToSellPictures.description}</p>
                <p className="item-price">{this.state.itemToSellPictures.price} <i className="fas fa-coins"></i></p>
                {this.state.itemToSellPictures.sellerId !== this.props.userId && <button onClick={() => this.addToMyShoppingBag(this.state.itemToSellPictures._id)} className="btn btn-primary intimy-rectangle-button">Add to my bag</button>}
                <br />
                <p className="item-to-sell-message">{this.state.messages.itemShoppingBag}</p>
              </div>
              <div className="wall-container">
                <div className="bloc-messages" ref={this.blocMessages}>
                  {comments}
                </div>
                <div>
                  <form className="send-comment-form" onSubmit={this.addPost}>
                    <input className="post-input" ref={this.postInput} type="text" onChange={this.handleChange} value={this.state.comment} placeholder="What's on your mind?" required />
                    <button className="send-post-button intimy-rectangle-button" type="submit">Send</button>
                  </form>
                </div>
                {this.state.popUpLogIn &&
                  <div className="logInHeader">
                    <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction={this.state.popUpLogIn} />
                  </div>
                }
              </div>
            </div>
          }

        </div>
      )
    }
    else return null
  }
}

export default UserConnexionLoader(withRouter(observer(ItemToSell)))