import React from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'

class MyItem extends React.Component {
	
  constructor(props) {
    super(props)
    this.state = {
      myItemToSell: []
    } 
  }

  componentDidMount() {

  }

  getMyItemsToSell = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getmyitemstoSell`, {params: {userId: this.props.userId}})
    .then((response) => {
      this.setState({
        myItemsToSell: response.data
      })
    })
  }

 render() {

 	return
 }
}

export default MyItem