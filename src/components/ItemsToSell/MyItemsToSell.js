import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import UserConnexionLoader from '../UserConnectionLoader'
import PopUpModifyMyItem from '../PopUp/PopUpModifyMyItem'
import PopUpModifyMyItemPictures from '../PopUp/PopUpModifyMyItemPictures'
import PopUpModifyMyItemVideos from '../PopUp/PopUpModifyMyItemVideos'
import io from 'socket.io-client'

class MyItemsToSell extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      myItemsToSell: [],
      myItemsToSellPictures: [],
      myItemsToSellVideos: [],
      showModify: false,
      itemId: "",
      messages: {
        messageItemUpdated: ""
      }
    }
  }

  componentDidMount() {
    this.getMyItemsToSell()
    this.socket = io(`${process.env.REACT_APP_URI}/postProduit`, {
      transports: ['websocket']
    })
    this.socket.on('Produit posted', () => {
      this.getMyItemsToSell()
    })
  }

  componentWillUnmount() {
    this.socket.close()
  }

  getMyItemsToSell = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getmyitemstoSell`, { params: { userId: this.props.userId } })
      .then((response) => {
        this.setState({
          myItemsToSell: response.data.produits,
          myItemsToSellPictures: response.data.produitsPictures,
          myItemsToSellVideos: response.data.produitsVideos
        })
      })
  }

  toggleModify = content => {
    this.setState({
      showModify: content.content,
      itemId: content.itemId,
      typeItem: content.typeItem
    })
  }

  closePopUpModify = () => {
    this.setState({ showModify: !this.state.showModify })
  }

  setStateMessages = e => {
    this.setState({
      messages: {
        messageItemUpdated: e
      }
    })
    this.getMyItemsToSell()
  }

  render() {
    const myItemsToSell = this.state.myItemsToSell.map((data, key) =>
      <div className="item-profile" key={key}>
        <Link to={`/item/${data._id}`}>
          <img className="item-picture-profile" src={data.img} alt="Item Picture" />
          <h3 className="title-item-profile">{data.name}</h3>
          <p className="item-price-profile">{data.price} tokens</p>
        </Link>
        <p onClick={() => this.toggleModify({ content: 'modifyItem', itemId: data._id })}><i className="far fa-edit pen-icon" aria-hidden="true"></i></p>
      </div>
    ).reverse()
    const myItemsToSellPictures = this.state.myItemsToSellPictures.map((data, key) =>
      <div className="item-profile" key={key}>
        <Link to={`/picturesset/${data._id}`}>
          <div className="item-pictures-picture">
            <img className="item-picture-profile" src="http://localhost:8080/images/picturesSet.svg" alt="Item picture" />
            <p className="item-pictures-title">{data.numberPictures} Picture{data.numberPictures > 1 && "s"}</p>
          </div>
          <h3 className="title-item-profile">{data.name}</h3>
          <p className="item-price-profile">{data.price} tokens</p>
        </Link>
        <p onClick={() => this.toggleModify({ content: 'modifyItemPictures', itemId: data._id })}><i className="far fa-edit pen-icon" aria-hidden="true"></i></p>
      </div>
    ).reverse()
    const myItemsToSellVideos = this.state.myItemsToSellVideos.map((data, key) =>
      <div className="item-profile" key={key}>
        <Link to={`/videosset/${data._id}`}>
          <div className="item-pictures-picture">
            <img className="item-picture-profile" src="http://localhost:8080/images/videosSet.svg" alt="Item video" />
            <p className="item-pictures-title">{data.numberVideos} Video{data.numberVideos > 1 && "s"}</p>
          </div>
          <h3 className="title-item-profile">{data.name}</h3>
          <p className="item-price-profile">{data.price} tokens</p>
        </Link>
        <p onClick={() => this.toggleModify({ content: 'modifyItemVideos', itemId: data._id })}><i className="far fa-edit pen-icon" aria-hidden="true"></i></p>
      </div>
    ).reverse()
    return <div>
      {myItemsToSell}
      {myItemsToSellPictures}
      {myItemsToSellVideos}
      {this.state.showModify == "modifyItem" && <PopUpModifyMyItem itemId={this.state.itemId} setStateMessages={this.setStateMessages} showModify={this.state.showModify} closePopUpModify={this.closePopUpModify} />}
      {this.state.showModify == "modifyItemPictures" && <PopUpModifyMyItemPictures itemId={this.state.itemId} setStateMessages={this.setStateMessages} showModify={this.state.showModify} closePopUpModify={this.closePopUpModify} />}
      {this.state.showModify == "modifyItemVideos" && <PopUpModifyMyItemVideos itemId={this.state.itemId} setStateMessages={this.setStateMessages} showModify={this.state.showModify} closePopUpModify={this.closePopUpModify} />}
    </div>
  }
}

export default UserConnexionLoader(MyItemsToSell)