//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import UserConnexionLoader from '../UserConnectionLoader'
import moment from 'moment-shortformat'
import PopUpLogIn from '../PopUp/PopUpLogIn'
import PopUpBillingPicturesSet from '../PopUp/PopUpBillingPicturesSet'
import Gallery from '../gallery/gallery'

class PicturesSet extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      itemToSell: "",
      messages: {
        picturesSet: ""
      },
      comments: null,
      comment: "",
      popUpLogIn: false,
      doubleChecked: false,
      itemBought: false
    }
    this.postInput = React.createRef()
    this.blocMessages = React.createRef()
  }

  async componentDidMount() {
    await this.getItemToSell()
    this.getItemComments()
    if (this.state.itemToSell.boughtBy.includes(this.props.userId)) this.getItemLink()
  }

  updateScroll() {
    this.blocMessages.current.scrollTop = this.blocMessages.current.scrollHeight
  }

  componentDidUpdate() {
    if (this.props.isActive === "timeline") this.updateScroll()
  }

  handleChange = e => {
    this.setState({
      comment: e.target.value
    })
  }

  getItemToSell = async e => {
    await axios.get(`${process.env.REACT_APP_URI}/api/getitemtosellpictures`, { params: { itemId: this.props.match.params.itemId } })
      .then((response) => {
        if (response.data == "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          this.setState({
            itemToSell: response.data
          })
          document.title = `${response.data.name} - intimy.shop`
        }
      })
  }

  getItemLink = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemlink`, { params: { itemId: this.props.match.params.itemId } })
      .then((response) => {
        if (response.data == "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          this.setState({
            contentLink: response.data.pictures
          })
        }
      })
  }

  contentLink = link => {
    this.setState({
      contentLink: link
    })
  }

  getItemComments = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getitemcomments`, { params: { itemId: this.props.match.params.itemId } })
      .then(comments => {
        this.setState({
          comments: comments.data,
          loaded: true
        }, () => {
          this.updateScroll()
        })
      })
  }

  itemBought = e => {
    this.setState({
      itemBought: true,
    })
  }

  addPost = e => {
    e.preventDefault()
    if (this.props.connected) {
      axios.post(`${process.env.REACT_APP_URI}/api/additemcomment`, { itemId: this.props.match.params.itemId, comment: this.state.comment, sellerId: this.state.itemToSell.sellerId }, { withCredentials: true })
        .then(response => {
          if (response.data.status === 'Comment added') {
            this.setState({
              comment: "",
              comments: [...this.state.comments, response.data.comment]
            })
            this.postInput.current.blur()
            this.updateScroll()
            window.gtag('event', `Item comment posted`, {
              'event_category': 'user'
            })
          }
          else if (response.data === 'Comment not added') {

          }
        })
    }
    else {
      this.setState({
        popUpLogIn: "to post"
      })
    }
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    popUpLogIn: false,
    doubleChecked: false
  })

  doubleCheck = () => this.setState({
    doubleChecked: !this.state.doubleChecked
  })

  render() {
    if (this.state.loaded) {
      const comments = this.state.comments.map(comment =>
        <div key={comment._id} className="bloc-messages-content">
          {comment.userId === this.props.userId ?
            <div>
              <div className="bloc-message-container-uploader">
                <div className="bloc-message-uploader">
                  <span className="username-wall">{comment.username}</span>
                  <p className="message-wall">{comment.comment}</p>
                </div>
                <div className="date-wall"> {moment(comment.date).short()}</div>
              </div>
            </div>
            :
            <div>
              <div className="bloc-message-container-users">
                <div className="bloc-message-users">
                  <span className="username-wall">{comment.username}</span>
                  <p className="message-wall">{comment.comment}</p>
                </div>
                <div className="date-wall"> {moment(comment.date).short()}</div>
              </div>
            </div>
          }
        </div>
      )
      return (
        <div className="content">
          <div className="item-container">
            <div className="item-pictures-picture-page">
              <img src="http://localhost:8080/images/picturesSet.svg" alt="Item picture" className="item-picture" />
              <p className="item-pictures-title">{this.state.itemToSell.numberPictures} Picture{this.state.itemToSell.numberPictures > 1 && "s"}</p>
            </div>
            <br />
            <p className="item-to-sell-name">{this.state.itemToSell.name}</p>
            <p className="item-to-sell-description">{this.state.itemToSell.description}</p>
            <p className="item-price">{this.state.itemToSell.price} tokens</p>
            {this.state.itemToSell.sellerId !== this.props.userId && !this.state.itemToSell.boughtBy.includes(this.props.userId) && !this.state.itemBought && <button onClick={this.doubleCheck} className="btn btn-primary intimy-rectangle-button">BUY NOW</button>}
            {(this.state.itemToSell.boughtBy.includes(this.props.userId) || this.state.itemBought) && <div><a className="blue-link" href={this.state.contentLink} download>Download</a></div>}
            {this.state.doubleChecked && <PopUpBillingPicturesSet contentLink={this.contentLink} itemBought={this.itemBought} connected={this.props.connected} item={this.state.itemToSell} handleClickOutside={this.handleClickOutside} toAction={this.state.popUpLogIn} />}
            <br />
            <p className="item-to-sell-message">{this.state.messages.itemShoppingBag}</p>
          </div>
          <div className="wall-container">
            <div className="bloc-messages" ref={this.blocMessages}>
              {comments}
            </div>
            <div>
              <form className="send-comment-form" onSubmit={this.addPost}>
                <input className="post-input" ref={this.postInput} type="text" onChange={this.handleChange} value={this.state.comment} placeholder="What's on your mind?" required />
                <button className="send-post-button intimy-rectangle-button" type="submit">Send</button>
              </form>
            </div>
            {this.state.popUpLogIn &&
              <div className="logInHeader">
                <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction={this.state.popUpLogIn} />
              </div>
            }
          </div>
        </div>
      )
    }
    else return null
  }
}

export default UserConnexionLoader(withRouter(PicturesSet))