//React
import React from 'react'
//Router
import { withRouter, Link } from 'react-router-dom'
import axios from 'axios'

class PopUpBuyMoreTokens extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount() {
        document.body.classList.add('noScrollBody')
    }

    componentWillUnmount() {
        document.body.classList.remove('noScrollBody')
    }

    handleClickOutside = e => {
        e.stopPropagation()
        e.preventDefault()

        this.props.handleClickOutside()
    }

    render() {
        const { toggleBuyMoreTokens } = this.props
        return (
            <div className="popUpLog">
                <div className="container-pop-up-login">
                    <div className="background-pop-up-login" onClick={toggleBuyMoreTokens}></div>
                    <div className="pop-up-login">
                        <i className="fas fa-times close-pop-up-login" onClick={toggleBuyMoreTokens}></i>
                        <div className="more-tokens-icon">
                            <i className="fas fa-coins coins-coins"></i>
                            <i className="fas fa-times coins-cross"></i>
                        </div>
                        <p className="tokens-added-message">Not enough tokens <br /> Don't miss it <br/> <Link to="/tokens" className="blue-link">Buy more tokens now</Link></p>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(PopUpBuyMoreTokens)