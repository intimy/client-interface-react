//React
import React, { Component } from 'react'
import axios from 'axios'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

class PopUpModifyMyItemVideos extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      containerOn: this.props.showModify,
      formData: {
        name: "",
        description: "",
        price: ""
      },
      messages: {
        messageStatus: ""
      }
    }
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
    this.getItemToSell()
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  getItemToSell = async e => {
    await axios.get(`${process.env.REACT_APP_URI}/api/getitemtoSell`, { params: { itemId: this.props.itemId } })
      .then((response) => {
        if (response.data == "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          const { formData } = this.state
          this.setState({
            formData: {
              ...formData,
              name: response.data.produitsVideos.name,
              description: response.data.produitsVideos.description,
              price: response.data.produitsVideos.price
            },
            loaded: true
          })
          document.title = `${response.data.name} - intimy.shop`
        }
      })
  }

  handleChange = e => {
    const { name } = e.target
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: value
      }
    })
  }

  handleFileUpload = e => {
    e.preventDefault()
    const { name } = e.target
    const { formData } = this.state
    let reader = new FileReader()
    let file = e.target.files[0]
    reader.onload = () => {
      this.setState({
        formData: {
          ...formData,
          [name]: reader.result
        }
      })
    }
    reader.readAsDataURL(file)
  }

  handleFilesUpload = e => {
    e.preventDefault()
    const { name } = e.target
    const { formData } = this.state
    let files = Array.from(e.target.files)
    let filesResults = []
    let imgs = [...this.state.formData.imgs]
    let promise = files.map(file => new Promise(resolve => {
      let reader = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }

      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        formData: {
          ...formData,
          [name]: filesResults
        }
      })
    })
  }

  modifyMyItem = e => {
    e.preventDefault()

    axios.put(`${process.env.REACT_APP_URI}/api/modifymyitemvideos/${this.props.itemId}`, { formData: this.state.formData, itemId: this.props.itemId }, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Item updated!") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  removeItem = () => {
    axios.put(`${process.env.REACT_APP_URI}/api/deleteitemvideos`, { itemId: this.props.itemId }, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Item deleted") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  deleteExtraPicture = (picture, index) => {
    let newPicturesArray = [...this.state.formData.imgs]
    newPicturesArray.splice(index, 1)
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        imgs: newPicturesArray
      }
    })
  }

  render() {
    let { containerOn } = this.state
    if (this.state.loaded) {
      return (
        <div className="popUpLog">
          {containerOn == 'modifyItemVideos' && <div className="container-pop-up-modify-my-item">
            <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}></div>
            <div className="pop-up-modify-my-item">
              <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpModify()}></i>
              <h2 className="text-align-center clear">Modify item</h2>
              <form className="post-form-modify-item" ref={input => this.postFormItem = input} onSubmit={e => this.modifyMyItem(e)}>
                <div className="form-group modify-my-item-input">
                  <input type="text" name="name" className="form-control" value={this.state.formData.name} placeholder="Name" onChange={this.handleChange} />
                </div>
                <div className="form-group modify-my-item-input">
                  <input type="text" name="description" className="form-control" value={this.state.formData.description} placeholder="Description" onChange={this.handleChange} />
                </div>
                <div className="input-group modify-my-item-input">
                  <input type="number" className="form-control" name="price" step="any" min="0" value={this.state.formData.price} placeholder="Price" aria-label="Amount (to the nearest dollar)" onChange={this.handleChange} />
                </div>
                <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
                <button type="button" className="btn btn-danger float-right delete-item-button" onClick={() => this.removeItem()}>Delete</button>
              </form>
              <div className="error-message-signup">{this.state.messages.messageStatus && this.state.messages.messageStatus}</div>
            </div>
          </div>}
        </div>
      )
    }
    else return null
  }
}

export default PopUpModifyMyItemVideos;