//React
import React from 'react'
//Router
import { withRouter } from 'react-router-dom'
import axios from 'axios'

class PopUpSuccessTokens extends React.Component {

  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  handleClickOutside = e => {
    e.stopPropagation()
    e.preventDefault()

    this.props.handleClickOutside()
  }

  render() {
    const { closeSuccessPopUp } = this.props
    return (
      <div className="popUpLog">
        <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={closeSuccessPopUp}></div>
          <div className="pop-up-login">
            <i className="fas fa-times close-pop-up-login" onClick={closeSuccessPopUp}></i>
            <div className="tokens-added-icon">
              <i className="fas fa-check-circle"></i>
            </div>
            <p className="tokens-added-message">Success!<br/>Your item is online!</p>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(PopUpSuccessTokens)