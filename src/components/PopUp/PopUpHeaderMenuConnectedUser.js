//React
import React from 'react'
//Router
import { withCookies } from 'react-cookie'
import { withRouter } from 'react-router-dom'
import UserConnexionLoader from '../UserConnectionLoader'
import axios from 'axios'

//Components
import ActionLink from '../ActionLink'

class PopUpHeaderMenuConnectedUser extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      idVerifiedneeded: false
    }
    this.childRef = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClick, false)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClick, false)
  }

  togglePostItem = (content) => {
    axios.get(`${process.env.REACT_APP_URI}/api/isverifieduser`, { withCredentials: true })
      .then((response) => {
        if (response.data === "User verified") {
          this.props.togglePostItem(content)
          this.handleOutsideClick()
        }
        else if (response.data === "User not verified") {
          this.props.history.push("/verificationid")
          this.handleOutsideClick()
        }
      })
  }

  handleClick = e => {
    if (this.childRef.current.contains(e.target)) {
      return
    }
    this.handleOutsideClick()
  }

  handleOutsideClick = () => {
    this.props.menuConnectedUser()
  }

  logOut = async e => {
    e.preventDefault()
    const { cookies } = await this.props
    await cookies.remove('authtoken', { path: '/' })
    window.location.reload()
  }

  handleFunctions = e => {
    this.props.menuConnectedUser()
  }

  render() {
    return (
      <div>
        <div className="ContainerMenuConnectedUser" ref={this.childRef}>
          <ul className="ListItemsMenuConnectedUser">
            {this.props.userStatus === "seller" && <li className="post-item-link" onClick={() => this.togglePostItem('postItem')}>Post an item</li>}
            <li><ActionLink to="/myProfile" action={this.handleFunctions}>Account</ActionLink></li>
            {this.props.userStatus === "seller" && <li><ActionLink to="/withdrawal" action={this.handleFunctions}>Withdrawal</ActionLink></li>}
            <li><a href="#" onClick={this.logOut}>Log out</a></li>
          </ul>
        </div>
      </div>

    )
  }
}

export default UserConnexionLoader(withCookies(withRouter(PopUpHeaderMenuConnectedUser)))