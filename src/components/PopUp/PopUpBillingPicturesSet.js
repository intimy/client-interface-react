import React from 'react'
//Components
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios'
import countriesList from 'countries-list/dist/data.json'
import spinner from '../../images/spinner.gif'
import USStates from '../../files/states_hash.json'
import PopUpBuyMoreTokens from './PopUpBuyMoreTokens'

class PopUpBillingPicturesSet extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isloading: false,
            popUp: "billing",
            message: "",
            totalPrice: 0,
            fees: 0,
            focused: false,
            contentLink: ""
        }
    }

    async componentDidMount() {
        let totalPrice = await this.props.item.price

        const fees = await Math.ceil(totalPrice * 0.05)
        document.body.classList.add('noScrollBody')
        this.setState({
            totalPrice: totalPrice,
            fees: fees
        })
    }

    componentWillUnmount() {
        document.body.classList.remove('noScrollBody')
    }

    checkOut = e => {
        if (this.props.connected) {
            axios.post(`${process.env.REACT_APP_URI}/api/picturessetcheckout`, { itemId: this.props.item._id }, {
                withCredentials: true
            })
                .then(data => {
                    if (data.data == 'Not ok') {
                        this.setState({
                            messages: {
                                picturesSet: data.data
                            }
                        })
                    }
                    else if (data.data == 'Not enough tokens') {
                        this.setState({
                            popUp: 'buyMoreTokens'
                        })
                    }
                    else {
                        this.setState({
                            popUp: "success",
                            contentLink: data.data
                        })
                        this.props.itemBought()
                        this.props.contentLink(data.data)
                    }
                })
                .catch(err => {
                    throw err
                })
        }
        else {
            this.setState({
                popUpLogIn: "to shop"
            })
        }
    }

    focused = element => {
        this.setState({
            focused: element
        })
    }

    toggleBuyMoreTokens = e => {
        this.props.handleClickOutside()
    }

    render() {
        return <div className="container-pop-up-billing text-left">
            <div className="background-pop-up-login"></div>
            {this.state.popUp === "billing" &&
                <div className="pop-up-billing">
                    <i className="fas fa-times close-pop-up-billing" onClick={this.props.handleClickOutside}></i>
                    {this.state.isloading ?
                        <div className="spinner-loader-container"><img src={spinner} alt="Spinner loader" className="spinner-loader" /></div>
                        :
                        <>
                            <h2 className="billing-details-title">Billing details</h2>
                            <div className="billing-details-content">
                                <p className="billing-detail-label">Price: <span className="billing-detail">{this.state.totalPrice} <i className="fas fa-coins"></i></span></p>
                                <p className="billing-detail-label">Fees: <span className="billing-detail">{this.state.fees} <i className="fas fa-coins"></i></span></p>
                                <p className="billing-detail-label">Total: <span className="billing-detail">{this.state.totalPrice + this.state.fees} <i className="fas fa-coins"></i></span></p>
                            </div>
                            <button className="pay-button intimy-rectangle-button" onClick={this.checkOut}>Pay</button>
                            {this.state.uncomplete && <p className="transaction-success-message">{this.state.uncomplete}</p>}
                        </>
                    }
                </div>
            }
            {this.state.popUp === "success" &&
                <div className="popUpLog">
                    <div className="container-pop-up-login">
                        <div className="background-pop-up-login" onClick={this.props.handleClickOutside}></div>
                        <div className="pop-up-login">
                            <i className="fas fa-times close-pop-up-login" onClick={this.props.handleClickOutside}></i>
                            <div className="tokens-added-icon">
                                <i className="fas fa-check-circle"></i>
                            </div>
                            <p className="pictures-bought-message">Success!<br />You can now download your content!<br /><a className="blue-link" href={this.state.contentLink} download>Download</a></p>
                        </div>
                    </div>
                </div>
            }
            {this.state.popUp === 'buyMoreTokens' && <PopUpBuyMoreTokens toggleBuyMoreTokens={this.toggleBuyMoreTokens} />}
        </div>
    }
}

export default PopUpBillingPicturesSet