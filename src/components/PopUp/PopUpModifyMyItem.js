//React
import React, { Component } from 'react'
import axios from 'axios'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

class PopUpModifyMyItem extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      containerOn: this.props.showModify,
      formData: {
        name: "",
        description: "",
        img: "",
        imgs: "",
        pictures: [],
        price: ""
      },
      messages: {
        messageStatus: ""
      },
      tests: {
        test: []
      }
    }
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
    this.getItemToSell()
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  getItemToSell = async e => {
    await axios.get(`${process.env.REACT_APP_URI}/api/getitemtoSell`, { params: { itemId: this.props.itemId } })
      .then((response) => {
        if (response.data == "Nothing here") {
          this.props.history.push("/404")
        }
        else {
          const { formData, formDataPictures } = this.state
          this.setState({
            formData: {
              ...formData,
              name: response.data.produits.name,
              description: response.data.produits.description,
              price: response.data.produits.price,
              imgs: response.data.produits.pictures
            },
            loaded: true
          })
          document.title = `${response.data.name} - intimy.shop`
        }
      })
  }

  handleChange = e => {
    const { name } = e.target
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: value
      }
    })
  }

  handleFileUpload = e => {
    e.preventDefault()
    const { name } = e.target
    const { formData } = this.state
    let reader = new FileReader()
    let file = e.target.files[0]
    reader.onload = () => {
      this.setState({
        formData: {
          ...formData,
          [name]: reader.result
        }
      })
    }
    reader.readAsDataURL(file)
  }

  handleFilesUpload = e => {
    e.preventDefault()
    const { name } = e.target
    const { formData } = this.state
    let files = Array.from(e.target.files)
    let filesResults = []
    let imgs = [...this.state.formData.imgs]
    let promise = files.map(file => new Promise(resolve => {
      let reader = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }

      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        formData: {
          ...formData,
          [name]: filesResults
        }
      })
    })
  }

  modifyMyItem = e => {
    e.preventDefault()

    axios.put(`${process.env.REACT_APP_URI}/api/modifymyitem/${this.props.itemId}`, { formData: this.state.formData, itemId: this.props.itemId }, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Item updated!") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
        }
        else if (data.data == "Pictures limit reached") {
          this.setState({
            messages: {
              messageStatus: "Pictures limit reached"
            }
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  removeItem = () => {
    axios.put(`${process.env.REACT_APP_URI}/api/deleteitem`, { itemId: this.props.itemId }, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Item deleted") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  deleteExtraPicture = (picture, index) => {
    let newPicturesArray = [...this.state.formData.imgs]
    newPicturesArray.splice(index, 1)
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        imgs: newPicturesArray
      }
    })
  }

  render() {
    let { containerOn } = this.state
    if (this.state.loaded) {
      const extraPictures = this.state.formData.imgs.map((img, index) =>
        <div className="extra-picture-container" key={index}>
          <img id="extra-pictures-modify-id" className="extra-pictures-modify" src={img.thumbnail} alt="Extra pictures" />
          <i onClick={() => this.deleteExtraPicture(img, index)} className="fas fa-times-circle delete-extra-picture-button"></i>
        </div>
      )
      return (
        <div className="popUpLog">
          {containerOn == 'modifyItem' && <div className="container-pop-up-modify-my-item">
            <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}></div>
            <div className="pop-up-modify-my-item">
              <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpModify()}></i>
              <h2 className="text-align-center clear">Modify item</h2>
              <form className="post-form-modify-item" ref={input => this.postFormItem = input} onSubmit={e => this.modifyMyItem(e)}>
                <div className="form-group modify-my-item-input">
                  <input type="text" name="name" className="form-control" value={this.state.formData.name} placeholder="Name" onChange={this.handleChange} />
                </div>
                <div className="form-group modify-my-item-input">
                  <input type="text" name="description" className="form-control" value={this.state.formData.description} placeholder="Description" onChange={this.handleChange} />
                </div>
                <div className="form-group modify-my-item-input ModifyItemPictureForm">
                  <label>Picture</label>
                  <input type="file" name="img" className="form-control" placeholder="Item picture" onChange={this.handleFileUpload} accept="image/png, image/jpeg" />
                </div>
                <div className="form-group modify-my-item-input ModifyItemPictureForm">
                  <label>Extra pictures</label>
                  <div className="extra-pictures-modify-container">
                    {extraPictures}
                  </div>
                  <input type="file" name="pictures" className="form-control" placeholder="Item picture" onChange={this.handleFilesUpload} accept="image/png, image/jpeg" multiple />
                </div>
                <div className="input-group modify-my-item-input">
                  <input type="number" className="form-control" name="price" step="any" min="0" value={this.state.formData.price} placeholder="Price" aria-label="Amount (to the nearest dollar)" onChange={this.handleChange} />
                </div>
                <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
                <button type="button" className="btn btn-danger float-right delete-item-button" onClick={() => this.removeItem()}>Delete</button>
              </form>
              <div className="error-message-signup">{this.state.messages.messageStatus && this.state.messages.messageStatus}</div>
            </div>
          </div>}
        </div>
      )
    }
    else return null
  }
}

export default PopUpModifyMyItem;