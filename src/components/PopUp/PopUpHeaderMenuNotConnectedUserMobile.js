//React
import React from 'react'
//Router
import { withCookies, Cookies } from 'react-cookie'
import onClickOutside from 'react-onclickoutside'
import UserConnexionLoader from '../UserConnectionLoader'
import { Redirect, withRouter } from 'react-router-dom'
import axios from 'axios'

//Components
import ActionLink from '../ActionLink'

class PopUpHeaderMenuNotConnectedUserMobile extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: true,
            containerOn: 'logIn',
            notifsNumber: 0,
            username: "",
            bagNumber: 0,
            tokens: 0,
            showMobileMenu: false
        }
        this.childRef = React.createRef()
        this.childRef2 = React.createRef()
    }

    componentDidMount() {
        this.getData()
    }

    componentWillUnmount() {
        this.childRef.current.removeEventListener('click', this.handleClick, false)
    }

    handleClick = e => {
        if (this.childRef2.current.contains(e.target)) {
            return
        }
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.props.showMobileMenu()
    }

    logOut = (e) => {
        e.preventDefault()
        const { cookies } = this.props
        cookies.remove('authtoken', { path: '/' })
        window.location.reload()
    }

    handleFunctions = e => {
        this.props.showMobileMenu()
    }

    getData = async e => {
        const bagNumber = await axios.get(`${process.env.REACT_APP_URI}/api/getitemsfrommyshoppingbagnotconnected`, { params: { itemsId: this.props.ShoppingBagStore.shoppingBag } })
        this.setState({
            bagNumber: bagNumber.data.length,
            loaded: true
        }, () => {
            this.childRef.current.addEventListener('click', this.handleClick, false)
        })
    }

    render() {
        if (this.state.loaded) {
            return (
                <div>
                    <div className={`ContainerMenuConnectedUserMBackground ${this.props.show ? "show" : ""}`} ref={this.childRef}></div>
                    <div className={`ContainerMenuConnectedUserM ${this.props.show ? "show" : ""}`} ref={this.childRef2}>
                        <ul className="ListItemsMenuConnectedUser">
                            <div className="mobile-menu-icon-panel"><i onClick={this.props.showMobileMenu} className="fas fa-bars"></i></div>
                            <div className="ListItemsMenuNotConnectedUserContent">
                                <li><a href="#" onClick={this.props.toggleMenuConnectionUser} className="toggle">Log in</a></li>
                                <li><ActionLink to="/tokens" action={this.handleFunctions} className="token-icon-header-mobile">Tokens</ActionLink></li>
                                <li> <ActionLink to="/live" action={this.handleFunctions} className="live-button-header-mobile">Live </ActionLink></li>
                                <li>
                                    <ActionLink to="/shoppingBag" action={this.handleFunctions} className="container-shop-bag-link-mobile">
                                        <span className="notifications-bubble-mobile toggle2">
                                            <i className="fas fa-shopping-bag ShoppingBagIcon-mobile"></i>
                                            {this.props.ShoppingBagStore.shoppingBag.length > 0 && <span className="notification-number-shopping-bag">{this.props.ShoppingBagStore.shoppingBag.length}</span>}
                                        </span>
                                        <span className="shopping-bag-header-mobile-title"> Shopping bag</span>
                                    </ActionLink>
                                </li>
                            </div>
                        </ul>
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(withCookies(withRouter(PopUpHeaderMenuNotConnectedUserMobile)))