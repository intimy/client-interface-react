import React from 'react'
//Components
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios'
import countriesList from 'countries-list/dist/data.json'
import spinner from '../../images/spinner.gif'
import USStates from '../../files/states_hash.json'

class PopUpBilling extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isloading: false,
            complete: false,
            uncomplete: false,
            empty: true,
            popUp: "shipping",
            profilData: [],
            useMyAddress: false,
            sameAsShipping: false,
            saveShippingInfos: false,
            message: "",
            shippingRates: 0,
            countries: "",
            totalPrice: 0,
            fees: 0,
            focused: false,
            myAddress: {
                firstname: "",
                lastname: "",
                streetaddress: "",
                apt: "",
                city: "",
                state: "",
                zipcode: "",
                country: ""
            },
            myBillingAddress: {
                firstname: "",
                lastname: "",
                streetaddress: "",
                apt: "",
                city: "",
                state: "",
                zipcode: ""
            },
            myAddressMissing: {
                firstname: false,
                lastname: false,
                streetaddress: false,
                apt: false,
                city: false,
                state: false,
                zipcode: false
            },
            myBillingAddressMissing: {
                firstname: false,
                lastname: false,
                streetaddress: false,
                apt: false,
                city: false,
                state: false,
                zipcode: false
            }
        }
    }

    componentWillMount() {
        this.setState({
            countries: countriesList
        })
    }

    async componentDidMount() {
        let totalPrice
        if (Array.isArray(this.props.items)) {
            totalPrice = await this.props.items.reduce((sum, i) => {
                return sum + i.price
            }, 0)
        }
        else {
            totalPrice = await this.props.items.price
        }
        const fees = await Math.ceil(totalPrice * 0.05)
        document.body.classList.add('noScrollBody')
        axios.get(`${process.env.REACT_APP_URI}/api/getprofildata`, { params: { userStatus: this.props.userStatus }, withCredentials: true })
            .then(response => {
                this.setState({
                    profilData: response.data,
                    totalPrice: totalPrice,
                    fees: fees
                })
            })
    }

    componentWillUnmount() {
        document.body.classList.remove('noScrollBody')
    }

    handleChecked = e => {
        const { value, name } = e.target
        if (e.target.checked) {
            this.setState({
                [name]: !this.state.useMyAddress,
                myAddress: {
                    firstname: this.state.profilData.firstname,
                    lastname: this.state.profilData.lastname,
                    streetaddress: this.state.profilData.streetaddress,
                    apt: this.state.profilData.apt,
                    city: this.state.profilData.city,
                    state: this.state.profilData.state,
                    zipcode: this.state.profilData.zipcode,
                    country: this.state.profilData.country
                }
            })
        }
        else {
            this.setState({
                [name]: !this.state.useMyAddress,
                myAddress: {
                    firstname: "",
                    lastname: "",
                    streetaddress: "",
                    apt: "",
                    city: "",
                    state: "",
                    zipcode: "",
                    country: ""
                }
            })
        }
    }

    handleCheckedSave = e => {
        const { value, name } = e.target
        this.setState({
            [name]: !this.state.saveShippingInfos
        })
    }
    handleCheckedBilling = e => {
        const { value, name } = e.target
        const { myBillingAddress } = this.state
        if (e.target.checked) {
            this.setState({
                [name]: !this.state.sameAsShipping,
                myBillingAddress: {
                    firstname: this.state.myAddress.firstname,
                    lastname: this.state.myAddress.lastname,
                    streetaddress: this.state.myAddress.streetaddress,
                    apt: this.state.profilData.apt,
                    city: this.state.myAddress.city,
                    state: this.state.myAddress.state,
                    zipcode: this.state.myAddress.zipcode
                }
            })
        }
        else {
            this.setState({
                [name]: !this.state.sameAsShipping,
                myBillingAddress: {
                    firstname: "",
                    lastname: "",
                    streetaddress: "",
                    apt: "",
                    city: "",
                    state: "",
                    zipcode: ""
                }
            })
        }
    }

    handleChangeShippingDetails = e => {
        const { value, name } = e.target
        const { myAddress } = this.state
        const { myAddressMissing } = this.state
        this.setState({
            myAddress: {
                ...myAddress,
                [name]: value
            },
            myAddressMissing: {
                ...myAddressMissing,
                [name]: value ? false : true
            }
        })
    }

    handleChangBillingDetails = e => {
        const { value, name } = e.target
        const { myBillingAddress } = this.state
        const { myBillingAddressMissing } = this.state
        this.setState({
            myBillingAddress: {
                ...myBillingAddress,
                [name]: value
            },
            myBillingAddressMissing: {
                ...myBillingAddressMissing,
                [name]: value ? false : true
            }
        })
    }

    checkOut = e => {
        if (this.state.myAddress.firstname && this.state.myAddress.lastname && this.state.myAddress.streetaddress && this.state.myAddress.city && (this.state.myAddress.state || this.state.myAddress.country !== "US") && this.state.myAddress.zipcode && this.state.myAddress.country) {
            let { myAddress, sameAsShipping, saveShippingInfos, shippingRates, fees } = this.state
            this.props.checkOut({ myAddress, sameAsShipping, saveShippingInfos, shippingRates, fees })
        }
        else {
            let myAddressMissing = {}
            if (!this.state.myAddress.firstname) myAddressMissing.firstname = true
            if (!this.state.myAddress.lastname) myAddressMissing.lastname = true
            if (!this.state.myAddress.streetaddress) myAddressMissing.streetaddress = true
            if (!this.state.myAddress.city) myAddressMissing.city = true
            if (!this.state.myAddress.state && this.state.myAddress.country === "US") myAddressMissing.state = true
            if (!this.state.myAddress.zipcode) myAddressMissing.zipcode = true
            if (!this.state.myAddress.country) myAddressMissing.country = true
            this.setState({
                myAddressMissing: myAddressMissing
            })
        }
    }

    popUp = e => {
        if (e === "billing") {
            this.setState({
                isloading: true
            })
        }
        if (this.state.myAddress.firstname && this.state.myAddress.lastname && this.state.myAddress.streetaddress && (this.state.myAddress.state || this.state.myAddress.country !== "US") && this.state.myAddress.city && this.state.myAddress.zipcode && this.state.myAddress.country) {
            this.setState({
                popUp: e
            })
            axios.post(`${process.env.REACT_APP_URI}/api/getshippingrates`, { items: this.props.items, myAddress: this.state.myAddress })
                .then(response => {
                    const shippingRates = Math.ceil(response.data * 10)
                    this.setState({
                        shippingRates: shippingRates,
                        isloading: false
                    })
                })
        }
        else {
            let myAddressMissing = {}
            if (!this.state.myAddress.firstname) myAddressMissing.firstname = true
            if (!this.state.myAddress.lastname) myAddressMissing.lastname = true
            if (!this.state.myAddress.streetaddress) myAddressMissing.streetaddress = true
            if (!this.state.myAddress.city) myAddressMissing.city = true
            if (!this.state.myAddress.state && this.state.myAddress.country === "US") myAddressMissing.state = true
            if (!this.state.myAddress.zipcode) myAddressMissing.zipcode = true
            if (!this.state.myAddress.country) myAddressMissing.country = true
            this.setState({
                myAddressMissing: myAddressMissing
            })
        }
    }

    focused = element => {
        this.setState({
            focused: element
        })
    }

    render() {
        const statesUS = Object.keys(USStates).map((state, index) =>
            <option key={index} value={state}>{state}</option>
        )
        const countries = Object.entries(this.state.countries.countries).map(([key, value]) => {
            return this.state.countries.countries[key].name
        })
        var mapped = countries.map(function (el, i) {
            return { index: i, value: el.toLowerCase() }
        })
        mapped.sort(function (a, b) {
            if (a.value > b.value) {
                return 1
            }
            if (a.value < b.value) {
                return -1
            }
            return 0
        })
        var result = mapped.map(el =>
            <option key={el.index} value={countries[el.index]}>{countries[el.index]}</option>
        )
        return <div className="container-pop-up-billing">
            <div className="background-pop-up-login"></div>
            {this.state.popUp === "shipping" &&
                <div className="pop-up-billing">
                    <i className="fas fa-times close-pop-up-billing" onClick={this.props.toggleMenuConnectedUser}></i>
                    <h2 className="billing-details-title">Shipping details</h2>
                    {this.state.profilData.streetaddress &&
                        <div>
                            <input type="checkbox" className="use-my-address-input" id="my-address" name="useMyAddress" checked={this.state.useMyAddress} onChange={this.handleChecked} />
                            <label className="use-my-address-label" htmlFor="my-address"> Use my address</label>
                            <p className="use-my-address-address">{this.state.profilData.streetaddress}</p>
                        </div>
                    }
                    <div>
                        <input type="text" className={`form-control modify-my-profile-input ${this.state.myAddressMissing.firstname ? "empty-field" : ""}`} id="first-name" name="firstname" placeholder="First name" value={this.state.myAddress.firstname} onChange={this.handleChangeShippingDetails} required />
                        {this.state.myAddressMissing.firstname && <span className="please-fill">Please fill this field</span>}
                        <input type="text" className={`form-control modify-my-profile-input ${this.state.myAddressMissing.lastname ? "empty-field" : ""}`} id="last-name" name="lastname" placeholder="Last name" value={this.state.myAddress.lastname} onChange={this.handleChangeShippingDetails} required />
                        {this.state.myAddressMissing.lastname && <span className="please-fill">Please fill this field</span>}
                        <input type="text" className={`form-control modify-my-profile-input ${this.state.myAddressMissing.streetaddress ? "empty-field" : ""}`} id="street-address" name="streetaddress" placeholder="Street Address" value={this.state.myAddress.streetaddress} onChange={this.handleChangeShippingDetails} required />
                        {this.state.myAddressMissing.streetaddress && <span className="please-fill">Please fill this field</span>}
                        <input type="text" className="form-control modify-my-profile-input" id="apt" name="apt" placeholder="Apt #, Floor, etc..." value={this.state.myAddress.apt} onChange={this.handleChangeShippingDetails} />
                        <div className={`selectWrapper ${this.state.focused === "select2" ? "focused" : ""} ${this.state.myAddressMissing.country ? "empty-field" : ""}`}>
                            <select onFocus={() => this.focused("select2")} onBlur={() => this.focused(false)} name="country" id="country" className={`form-control modify-my-profile-select`} value={this.state.myAddress.country} onChange={this.handleChangeShippingDetails} required>
                                <option value="" disabled hidden>Country *</option>
                                <option value="FR">France</option>
                                <option value="US">United States</option>
                            </select>
                        </div>
                        {this.state.myAddressMissing.city && <span className="please-fill">Please fill this field</span>}
                        {this.state.myAddress.country === "US" &&
                            <div className={`selectWrapper ${this.state.focused === "select3" ? "focused" : ""} ${this.state.myAddressMissing.state ? "empty-field" : ""}`}>
                                <select onFocus={() => this.focused("select3")} onBlur={() => this.focused(false)} className={`form-control modify-my-profile-select`} name="state" id="statesUS" value={this.state.myAddress.state} onChange={this.handleChangeShippingDetails} required>
                                    <option value="" disabled hidden>State *</option>
                                    {statesUS}
                                </select>
                            </div>
                        }
                        <input type="text" className={`form-control modify-my-profile-input ${this.state.myAddressMissing.city ? "empty-field" : ""}`} id="city" name="city" placeholder="City" value={this.state.myAddress.city} onChange={this.handleChangeShippingDetails} required />
                        <input type="text" className={`form-control modify-my-profile-input ${this.state.myAddressMissing.zipcode ? "empty-field" : ""}`} id="zipcode" name="zipcode" placeholder="Zip Code" value={this.state.myAddress.zipcode} onChange={this.handleChangeShippingDetails} required />
                        {this.state.myAddressMissing.zipcode && <span className="please-fill">Please fill this field</span>}
                    </div>
                    <input type="checkbox" name="saveShippingInfos" id="save-shipping-infos" checked={this.state.saveShippingInfos} onChange={this.handleCheckedSave} />
                    <label htmlFor="save-shipping-infos" className="save-shipping-details-label">Save shipping infos to buy faster next time</label>
                    <button className="pay-button intimy-rectangle-button" onClick={() => this.popUp("billing")}>Next</button>
                    {this.state.uncomplete && <p className="transaction-success-message">{this.state.uncomplete}</p>}
                </div>
            }
            {this.state.popUp === "billing" &&
                <div className="pop-up-billing">
                    <span onClick={() => this.popUp("shipping")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
                    <i className="fas fa-times close-pop-up-billing" onClick={this.props.toggleMenuConnectedUser}></i>
                    {this.state.isloading ?
                        <div className="spinner-loader-container"><img src={spinner} alt="Spinner loader" className="spinner-loader" /></div>
                        :
                        <>
                            <h2 className="billing-details-title">Billing details</h2>
                            <div className="billing-details-content">
                                <p className="billing-detail-label">Price: <span className="billing-detail">{this.state.totalPrice} <i className="fas fa-coins"></i></span></p>
                                <p className="billing-detail-label">Shipping: <span className="billing-detail">{this.state.shippingRates} <i className="fas fa-coins"></i></span></p>
                                <p className="billing-detail-label">Fees: <span className="billing-detail">{this.state.fees} <i className="fas fa-coins"></i></span></p>
                                <p className="billing-detail-label">Total: <span className="billing-detail">{this.state.totalPrice + this.state.shippingRates + this.state.fees} <i className="fas fa-coins"></i></span></p>
                            </div>
                            <button className="pay-button intimy-rectangle-button" onClick={this.checkOut}>Pay</button>
                            {this.state.uncomplete && <p className="transaction-success-message">{this.state.uncomplete}</p>}
                        </>
                    }
                </div>
            }
        </div>
    }
}

export default PopUpBilling