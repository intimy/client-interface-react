//React
import React from 'react'
//Router
import { withRouter } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'
import axios from 'axios'
import countriesList from 'countries-list/dist/data.json'
import ShoppingBagStore from "../stores/ShoppingBagStore"

class PopUpLogIn extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      containerOn: 'logIn',
      focused: false,
      resetPasswordEmail: "",
      formDataSignIn: {
        email: "",
        password: "",
        shoppingBag: ShoppingBagStore.shoppingBag
      },
      formDataSignUp: {
        email: "",
        username: "",
        password: "",
        retypePassword: "",
        country: "",
        origins: "",
        age: "",
        bio: "No bio yet",
        profilPicture: "",
        facebook: "",
        twitter: "",
        instagram: "",
        chaturbate: "",
        cam4: "",
        website: "",
        termsConditions: false,
        firstname: "",
        lastname: "",
        streetaddress: "",
        apt: "",
        city: "",
        state: "",
        zipcode: ""
      },
      formDataSignUpBuyer: {
        email: "",
        username: "",
        password: "",
        retypePassword: "",
        age: "",
        termsConditions: false,
        firstname: "",
        lastname: "",
        streetaddress: "",
        apt: "",
        city: "",
        state: "",
        zipcode: ""
      },
      messages: {
        messageStatusSignIn: "",
        messageStatusSignUp: "",
        messageStatusSignUpBuyer: "",
        messageStatusPassword: ""
      },
      countries: ""
    }
  }

  componentWillMount() {
    this.setState({
      countries: countriesList
    })
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  handleClickOutside = e => {
    e.stopPropagation()
    e.preventDefault()

    this.props.handleClickOutside()
  }

  setContainer = container => {
    this.setState({
      containerOn: container
    })
  }

  handleResetPassword = e => {
    const { value, name } = e.target
    this.setState({
      [name]: value
    })
  }

  handleChangeSignIn = e => {
    const { value, name } = e.target
    const { formDataSignIn } = this.state
    this.setState({
      formDataSignIn: {
        ...formDataSignIn,
        [name]: value
      }
    })
  }

  handleChangeSignUp = e => {
    const { name } = e.target
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    const { formDataSignUp } = this.state
    this.setState({
      formDataSignUp: {
        ...formDataSignUp,
        [name]: value
      }
    })
  }

  handleChangeSignUpBuyer = e => {
    const { name } = e.target
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    const { formDataSignUpBuyer } = this.state
    this.setState({
      formDataSignUpBuyer: {
        ...formDataSignUpBuyer,
        [name]: value
      }
    })
  }

  handleFileUpload = e => {
    const { name } = e.target
    const { formDataSignUp } = this.state
    let reader = new FileReader()
    let file = e.target.files[0]
    reader.onload = () => {
      this.setState({
        formDataSignUp: {
          ...formDataSignUp,
          [name]: reader.result
        }
      })
    }
    reader.readAsDataURL(file)
  }

  signInForm = (e) => {
    e.preventDefault()

    axios.post(`${process.env.REACT_APP_URI}/api/signin`, this.state.formDataSignIn, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "A required field is missing") {
          this.setState({
            messages: {
              messageStatusSignIn: data.data
            }
          })
        }
        if (data.data == "Wrong password") {
          this.setState({
            messages: {
              messageStatusSignIn: data.data
            }
          })
        }
        if (data.data == "No account founded") {
          this.setState({
            messages: {
              messageStatusSignIn: data.data
            }
          })
        }
        if (data.data == "Confirm email address") {
          this.setState({
            messages: {
              messageStatusSignIn: "Please confirm your email address to activate your account"
            }
          })
        }
        if (data.data == "Ok") {
          if (window.location.href.indexOf("activation") > -1) window.location.pathname = '/'
          else {
            window.location.reload()
          }
          this.props.handleClickOutside()
          ShoppingBagStore.clearAll()
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  signUpForm = (e) => {
    e.preventDefault()

    axios.post(`${process.env.REACT_APP_URI}/api/signup`, this.state.formDataSignUp, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Ok") {
          this.setContainer("confirmEmail")
          window.gtag('event', `Sign up`, {
            'event_category': 'user',
            'event_label': `Seller signed up`
          })
        }
        else {
          this.setState({
            messages: {
              messageStatusSignUp: data.data
            }
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  signUpBuyerForm = (e) => {
    e.preventDefault()

    axios.post(`${process.env.REACT_APP_URI}/api/signupbuyer`, this.state.formDataSignUpBuyer, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Ok") {
          this.setContainer("confirmEmail")
          window.gtag('event', `Sign up`, {
            'event_category': 'user',
            'event_label': `Buyer signed up`
          })
        }
        else {
          this.setState({
            messages: {
              messageStatusSignUpBuyer: data.data
            }
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  focused = element => {
    this.setState({
      focused: element
    })
  }

  resetpasswordForm = (e) => {
    e.preventDefault()

    axios.get(`${process.env.REACT_APP_URI}/api/forgotpassword`, { params: { email: this.state.resetPasswordEmail } })
      .then(data => {
        if (data.data == "No account found") {
          this.setState({
            messages: {
              messageStatusPassword: data.data
            }
          })
        }
        else if (data.data == "Ok") {
          this.setState({
            messages: {
              messageStatusPassword: "Sent! You should receive the email soon!"
            }
          })
          window.gtag('event', `Password reset`, {
            'event_category': 'user',
            'event_label': `Sent`
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    let { containerOn } = this.state
    const { handleClickOutside } = this.props
    const countries = Object.entries(this.state.countries.countries).map(([key, value]) => {
      return this.state.countries.countries[key].name
    })
    var mapped = countries.map(function (el, i) {
      return { index: i, value: el.toLowerCase() }
    })
    mapped.sort(function (a, b) {
      if (a.value > b.value) {
        return 1
      }
      if (a.value < b.value) {
        return -1
      }
      return 0
    })
    var result = mapped.map(el =>
      <option key={el.index} value={countries[el.index]}>{countries[el.index]}</option>
    )
    return (
      <div className="popUpLog">
        {containerOn == 'buysell' && <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={handleClickOutside}></div>
          <div className="pop-up-login">
            <span onClick={() => this.setContainer("logIn")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
            <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
            <div className="container-buysell">
              <button className="i-want-to-buy" onClick={() => this.setContainer("buyerForm")}>I want to buy</button>
              <button className="i-want-to-sell" onClick={() => this.setContainer("sellerForm")}>I want to sell</button>
            </div>
            <p className="note-buysell-registration">Note: Seller registration is only available for womens at the moment.</p>
          </div>
        </div>}

        {containerOn == 'logIn' && <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={handleClickOutside}></div>
          <div className="pop-up-login">
            <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
            <h2 className="text-align-center log-in-title">Log in {this.props.toAction}</h2>
            <form className="form-group" ref={input => this.signInInput = input}
              onSubmit={e => this.signInForm(e)}>
              <input type="text" className="form-control modify-my-profile-input" name="email" placeholder="Email / Username" value={this.state.formDataSignIn.email} onChange={this.handleChangeSignIn} required />
              <input type="password" className="form-control modify-my-profile-input" name="password" placeholder="Password" value={this.state.formDataSignIn.password} onChange={this.handleChangeSignIn} required />
              <div className="button-form-container">
                <button type="submit" className="btn btn-primary button-pop-up">Log in</button>
                <p className="link-sign-up" onClick={() => this.setContainer("buysell")}>Sign up</p>
                <p className="link-forgot-password no-margin" onClick={() => this.setContainer("forgotPassword")}>Forgot your password?</p>
              </div>
            </form>
            <p className="error-message-signup">{this.state.messages.messageStatusSignIn}</p>
          </div>
        </div>}

        {containerOn == 'forgotPassword' && <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={handleClickOutside}></div>
          <div className="pop-up-login">
            <span onClick={() => this.setContainer("logIn")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
            <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
            <h2 className="text-align-center log-in-title">You forgot your password?<br />No worries!</h2>
            <p className="forgot-password-text">Please enter your email address and we will send you instructions on how to reset your password</p>
            <form className="form-forgot-password form-group" ref={input => this.resetpasswordInput = input}
              onSubmit={e => this.resetpasswordForm(e)}>
              <input type="text" className="form-control modify-my-profile-input" id="pseudo" placeholder="Email" name="resetPasswordEmail" value={this.state.resetPasswordEmail} onChange={this.handleResetPassword} required />
              <div className="button-form-container">
                <button type="submit" className="btn btn-primary button-pop-up">Send</button>
              </div>
            </form>
            <p className="error-message-signup">{this.state.messages.messageStatusPassword}</p>
          </div>
        </div>}

        {containerOn == 'sellerForm' && <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={handleClickOutside}></div>
          <div className="pop-up-sign-up">
            <span onClick={() => this.setContainer("buysell")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
            <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
            <h2 className="text-align-center log-in-title">Sign up</h2>
            <form className="form-group"
              ref={input => this.signUpInput = input}
              onSubmit={e => this.signUpForm(e)}
            >
              <input type="text" className="form-control modify-my-profile-input" id="email" name="email" placeholder="Email *" value={this.state.formDataSignUp.email} onChange={this.handleChangeSignUp} required />
              <input type="text" className="form-control modify-my-profile-input" id="usernameSignUp" name="username" placeholder="Username *" value={this.state.formDataSignUp.username} onChange={this.handleChangeSignUp} maxLength="14" required />
              <input type="password" minLength="6" className="form-control modify-my-profile-input" id="passwordSignUp" name="password" placeholder="Password *" value={this.state.formDataSignUp.password} onChange={this.handleChangeSignUp} required />
              <input type="password" className="form-control modify-my-profile-input" id="retype-password" name="retypePassword" placeholder="Retype password *" value={this.state.formDataSignUp.retypePassword} onChange={this.handleChangeSignUp} required />
              <input type="number" min="18" max="99" className="form-control modify-my-profile-input" id="age-signup" name="age" placeholder="Age *" value={this.state.formDataSignUp.age} onChange={this.handleChangeSignUp} required />
              <label htmlFor="profilPicture">Profil picture *</label>
              <input type="file" name="profilPicture" className="form-control modify-my-profile-input" onChange={this.handleFileUpload} accept="image/png, image/jpeg" required />
              <div className={`selectWrapper ${this.state.focused === "select1" ? "focused" : ""}`}>
                <select onFocus={() => this.focused("select1")} onBlur={() => this.focused(false)} name="country" id="country" className="form-control modify-my-profile-select" value={this.state.formDataSignUp.country} onChange={this.handleChangeSignUp} required>
                  <option value="" disabled hidden>Country *</option>
                  <option value="United States">United States</option>
                </select>
              </div>
              <br />
              <div className={`selectWrapper ${this.state.focused === "select2" ? "focused" : ""}`}>
                <select onFocus={() => this.focused("select2")} onBlur={() => this.focused(false)} name="origins" id="origins-signup" className="form-control modify-my-profile-select" value={this.state.formDataSignUp.origins} onChange={this.handleChangeSignUp}>
                  <option value="" disabled hidden>Origins</option>
                  <option value="asian">Asian</option>
                  <option value="russian">Russian</option>
                  <option value="european">European</option>
                  <option value="latina">Latina</option>
                  <option value="indian">Indian</option>
                  <option value="metis">Metis</option>
                  <option value="arabic">Arabic</option>
                </select>
              </div>
              <br />
              <input type="text" className="form-control modify-my-profile-input" id="facebook" name="facebook" placeholder="Facebook" value={this.state.formDataSignUp.facebook} onChange={this.handleChangeSignUp} />
              <input type="text" className="form-control modify-my-profile-input" id="twitter" name="twitter" placeholder="Twitter" value={this.state.formDataSignUp.twitter} onChange={this.handleChangeSignUp} />
              <input type="text" className="form-control modify-my-profile-input" id="instagram" name="instagram" placeholder="Instagram" value={this.state.formDataSignUp.instagram} onChange={this.handleChangeSignUp} />
              <input type="text" className="form-control modify-my-profile-input" id="chaturbate" name="chaturbate" placeholder="Chaturbate" value={this.state.formDataSignUp.chaturbate} onChange={this.handleChangeSignUp} />
              <input type="text" className="form-control modify-my-profile-input" id="cam4" name="cam4" placeholder="Cam4" value={this.state.formDataSignUp.cam4} onChange={this.handleChangeSignUp} />
              <input type="text" className="form-control modify-my-profile-input" id="website" name="website" placeholder="Website" value={this.state.formDataSignUp.website} onChange={this.handleChangeSignUp} />
              <input type="checkbox" id="termsconditions" name="termsConditions" checked={this.state.formDataSignUp.termsConditions} onChange={this.handleChangeSignUp} required />
              <label htmlFor="termsconditions" className="label-terms-conditions">I agree to the terms and conditions.</label>
              <button type="submit" className="btn btn-primary button-pop-up">Sign up</button>
            </form>
            <p className="error-message-signup">{this.state.messages.messageStatusSignUp}</p>
          </div>
        </div>}

        {containerOn == 'buyerForm' && <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={handleClickOutside}></div>
          <div className="pop-up-sign-up-buyer">
            <span onClick={() => this.setContainer("buysell")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
            <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
            <h2 className="text-align-center log-in-title">Sign up</h2>
            <form className="form-group" ref={input => this.signUpBuyerInput = input} onSubmit={e => this.signUpBuyerForm(e)}>
              <input type="text" className="form-control modify-my-profile-input" id="email" name="email" placeholder="Email *" value={this.state.formDataSignUpBuyer.email} onChange={this.handleChangeSignUpBuyer} required />
              <input type="text" className="form-control modify-my-profile-input" id="username" name="username" placeholder="Username *" value={this.state.formDataSignUpBuyer.username} onChange={this.handleChangeSignUpBuyer} maxLength="14" required />
              <input type="password" minLength="6" className="form-control modify-my-profile-input" id="password" name="password" placeholder="Password *" value={this.state.formDataSignUpBuyer.password} onChange={this.handleChangeSignUpBuyer} required />
              <input type="password" className="form-control modify-my-profile-input" id="retype-password" name="retypePassword" placeholder="Retype password *" value={this.state.formDataSignUpBuyer.retypePassword} onChange={this.handleChangeSignUpBuyer} required />
              <input type="number" min="18" max="99" className="form-control modify-my-profile-input" id="age-signup" name="age" placeholder="Age *" value={this.state.formDataSignUpBuyer.age} onChange={this.handleChangeSignUpBuyer} required />
              <input type="checkbox" id="termsconditions" name="termsConditions" checked={this.state.formDataSignUpBuyer.termsConditions} onChange={this.handleChangeSignUpBuyer} required />
              <label htmlFor="termsconditions" className="label-terms-conditions">I agree to the terms and conditions.</label>
              <button type="submit" className="btn btn-primary button-pop-up">Sign up</button>
            </form>
            <p className="error-message-signup">{this.state.messages.messageStatusSignUpBuyer}</p>
          </div>
        </div>}
        {containerOn == 'confirmEmail' && <div className="container-pop-up-login">
          <div className="background-pop-up-login" onClick={handleClickOutside}></div>
          <div className="pop-up-sign-up-buyer">
            <span onClick={() => this.setContainer("logIn")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
            <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
            <div className="success-button">
              <i className="fas fa-check-circle"></i>
              <h4 className="title-confirm-email">Hi, {this.state.formDataSignUp.username ? this.state.formDataSignUp.username : this.state.formDataSignUpBuyer.username}</h4>
              <p className="text-confirm-email">Please confirm your address email <br />
                to make sure we got it right.
              </p>
              <span>See you soon!</span>
            </div>
          </div>
        </div>}
      </div>
    )
  }
}

export default withRouter(PopUpLogIn)