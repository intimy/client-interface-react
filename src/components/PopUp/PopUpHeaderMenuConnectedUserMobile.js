//React
import React from 'react'
//Router
import { withCookies, Cookies } from 'react-cookie'
import onClickOutside from 'react-onclickoutside'
import UserConnexionLoader from '../UserConnectionLoader'
import { Redirect, withRouter } from 'react-router-dom'
import axios from 'axios'

//Components
import ActionLink from '../ActionLink'

class PopUpHeaderMenuConnectedUserMobile extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            containerOn: 'logIn',
            notifsNumber: 0,
            username: "",
            bagNumber: 0,
            tokens: 0,
            showMobileMenu: false,
            idVerifiedneeded: false
        }
        this.childRef = React.createRef()
        this.childRef2 = React.createRef()
    }

    componentDidMount() {
        this.getData()
    }

    componentWillUnmount() {
        this.childRef.current.removeEventListener('click', this.handleClick, false)
    }

    handleClick = e => {
        if (this.childRef2.current.contains(e.target)) {
            return
        }
        this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.props.showMobileMenu()
    }

    logOut = (e) => {
        e.preventDefault()
        const { cookies } = this.props
        cookies.remove('authtoken', { path: '/' })
        window.location.reload()
    }

    handleFunctions = e => {
        this.props.showMobileMenu()
    }

    getData = async e => {
        const tokens = await axios.get(`${process.env.REACT_APP_URI}/api/getTokens`, { withCredentials: true })
        const bagNumber = await axios.get(`${process.env.REACT_APP_URI}/api/getTokens`, { withCredentials: true })
        this.setState({
            tokens: tokens.data.tokens,
            bagNumber: bagNumber.data.length,
            loaded: true
        }, () => {
            this.childRef.current.addEventListener('click', this.handleClick, false)
        })
    }

    getTokens = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getTokens`, { withCredentials: true })
            .then((response) => {
                if (response.data) {
                    this.setState({
                        tokens: response.data.tokens
                    })
                }
            })
    }

    getitemsfrommyshoppingbag() {
        axios.get(`${process.env.REACT_APP_URI}/api/getitemsfrommyshoppingbag`, { withCredentials: true })
            .then((response) => {
                this.setState({
                    bagNumber: response.data.length
                })
            })
    }

    togglePostItem = (content) => {
        axios.get(`${process.env.REACT_APP_URI}/api/isverifieduser`, { withCredentials: true })
            .then((response) => {
                if (response.data === "User verified") {
                    this.props.togglePostItem(content)
                    this.handleOutsideClick()
                }
                else if (response.data === "User not verified") {
                    this.props.history.push("/verificationid")
                    this.handleOutsideClick()
                }
            })
    }

    render() {
        if (this.state.loaded) {
            return (
                <div>
                    <div className={`ContainerMenuConnectedUserMBackground ${this.props.show ? "show" : ""}`} ref={this.childRef}></div>
                    <div className={`ContainerMenuConnectedUserM ${this.props.show ? "show" : ""}`} ref={this.childRef2}>
                        <ul className="ListItemsMenuConnectedUser">
                            <div className="mobile-menu-icon-panel"><i onClick={this.props.showMobileMenu} className="fas fa-bars"></i></div>
                            <li className="picture-profil-panel-m-r"><img src={this.props.profilPicture} alt="" /></li>
                            <li><ActionLink to="/tokens" action={this.handleFunctions} className="token-icon-header-mobile">Tokens<span className="tokens-number-header">{this.state.tokens}</span></ActionLink></li>
                            {this.props.userStatus === "seller" && <li className="post-item-link-m" onClick={() => this.togglePostItem('postItem')}>Post an item</li>}
                            <li><ActionLink to="/myProfile" action={this.handleFunctions}>Account</ActionLink></li>
                            <li> <ActionLink to="/live" action={this.handleFunctions} className="live-button-header-mobile">Live </ActionLink></li>
                            {this.props.userStatus === "seller" && <li><ActionLink to="/withdrawal" action={this.handleFunctions}>Withdrawal</ActionLink></li>}
                            <li>
                                <ActionLink to="/notificationsmobile" action={this.handleFunctions}>
                                    <span className="notifications-bubble-mobile toggle2">
                                        <i className="fas fa-bell notif-circle-header-mobile"></i>
                                        {this.props.notifsNumber > 0 && <span className="notification-number">{this.props.notifsNumber}</span>}
                                    </span>
                                    <span className="notifications-header-mobile-title"> Notifications</span>
                                </ActionLink>
                            </li>
                            <li>
                                <ActionLink to="/shoppingBag" action={this.handleFunctions} className="container-shop-bag-link-mobile">
                                    <span className="notifications-bubble-mobile toggle2">
                                        <i className="fas fa-shopping-bag ShoppingBagIcon-mobile"></i>
                                        {this.props.bagNumber > 0 && <span className="notification-number-shopping-bag">{this.props.bagNumber}</span>}
                                    </span>
                                    <span className="shopping-bag-header-mobile-title"> Shopping bag</span>
                                </ActionLink>
                            </li>
                            <li><a href="#" onClick={this.logOut}>Log out</a></li>
                        </ul>
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(withCookies(withRouter(PopUpHeaderMenuConnectedUserMobile)))