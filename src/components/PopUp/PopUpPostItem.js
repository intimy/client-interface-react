//React
import React from 'react'
import axios from 'axios'
//Components
import UserConnexionLoader from '../UserConnectionLoader'
import { Redirect } from 'react-router-dom'
import onClickOutside from 'react-onclickoutside'
import spinner from '../../images/spinner.gif'
import Resumable from 'resumablejs'
import userConnexionLoader from '../UserConnectionLoader';

class PopUpPostItem extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      containerOn: 'whatItemSelling',
      loaded: false,
      isLoading: false,
      isLoadingPictures: false,
      isLoadingVideos: false,
      fileName: [],
      fileNameArray: [],
      errorMessage: "",
      errorMessagePictures: "",
      errorMessageVideos: "",
      formData: {
        name: "",
        description: "",
        img: "",
        pictures: [],
        price: "",
        directSell: false,
        shippingOption: "paddedEnvelope"
      },
      formDataPictures: {
        name: "",
        description: "",
        pictures: [],
        price: ""
      },
      formDataVideos: {
        name: "",
        description: "",
        pictures: [],
        price: ""
      },
      messages: {
        messageStatus: ""
      },
      rates: "",
      showRestriction: false
    }
    this.clickOnAddVideoBtnRef = React.createRef()
    this.clickOnAddVideoDivRef = React.createRef()
    this.modifyPopUp = React.createRef()
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
    axios.post(`${process.env.REACT_APP_URI}/api/getrates`, {
      withCredentials: true
    })
      .then(data => {
        this.setState({
          rates: data.data.shippingRates,
          loaded: true
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  checkboxChange = e => {
    const { name } = e.target
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: !formData[name]
      }
    })
  }

  handleChange = e => {
    const { value, name } = e.target
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: value
      }
    })
  }

  handleChangePictures = e => {
    const { value, name } = e.target
    const { formDataPictures } = this.state
    this.setState({
      formDataPictures: {
        ...formDataPictures,
        [name]: value
      }
    })
  }

  handleChangeVideos = e => {
    const { value, name } = e.target
    const { formDataVideos } = this.state
    this.setState({
      formDataVideos: {
        ...formDataVideos,
        [name]: value
      }
    })
  }

  handleFileUpload = e => {
    if (e.target.files[0]) {
      const { name } = e.target
      const { formData } = this.state
      let reader = new FileReader()
      let file = e.target.files[0]
      reader.onload = () => {
        this.setState({
          formData: {
            ...formData,
            [name]: reader.result
          }
        })
      }
      reader.readAsDataURL(file)
    }
  }

  handleFilesUpload = (e) => {
    e.preventDefault()
    const { name } = e.target
    const { formData } = this.state
    let files = Array.from(e.target.files)
    let filesResults = []
    let promise = files.map(file => new Promise(resolve => {
      let reader = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }
      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        formData: {
          ...formData,
          [name]: filesResults
        }
      })
    })
  }

  handleFilesUploadPictures = (e, item) => {
    e.preventDefault()
    const { name } = e.target
    const { formDataPictures } = this.state
    let files = Array.from(e.target.files)
    let filesResults = []
    let promise = files.map(file => new Promise(resolve => {
      let reader = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }
      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        formDataPictures: {
          ...formDataPictures,
          [name]: filesResults
        }
      })
    })
  }

  handleFilesUploadVideos = (e, item) => {
    console.log(item)
    e.preventDefault()
    const { name } = e.target
    const { formDataVideos } = this.state
    let files = Array.from(e.target.files)
    let filesResults = []
    let promise = files.map(file => new Promise(resolve => {
      let reader = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }

      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        formDataVideos: {
          ...formDataVideos,
          [name]: filesResults
        }
      })
    })
  }

  createPost = e => {
    e.preventDefault()

    axios.post(`${process.env.REACT_APP_URI}/api/postproduit`, this.state.formData, {
      withCredentials: true
    })
      .then(data => {
        this.props.successPopUp()
        if (this.props.newItem) this.props.newItem(this.state.formData)
        this.props.closePopUpPostItem()
        window.gtag('event', `Item posted`, {
          'event_category': 'user',
          'event_label': `Item clothes posted`
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  createPostPictures = e => {
    e.preventDefault()
    this.setState({
      isLoadingPictures: true
    })
    axios.post(`${process.env.REACT_APP_URI}/api/postproduitpictures`, this.state.formDataPictures, {
      withCredentials: true
    })
      .then(data => {
        this.setContainer("successPictures")
        if (this.props.newItem) this.props.newItem(this.state.formDataPictures)
        window.gtag('event', `Item posted`, {
          'event_category': 'user',
          'event_label': `Item pictures posted`
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  createPostVideos = e => {
    e.preventDefault()
    if (this.state.fileName.length > 0) {
      this.setState({
        isLoadingVideos: true
      })
      this.state.r.upload()
    }
    else {
      this.setState({
        errorMessageVideos: "Please select at least 1 video"
      }, () => {
        this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
      })
    }
  }

  toggleRestrictions = e => {
    this.setState({
      showRestriction: !this.state.showRestriction
    })
  }

  setContainer = container => {
    this.setState({
      containerOn: container
    }, () => {
      if (container === "Videos") this.addVideos()
    })
  }

  clickOnAddVideoBtn = e => {
    if (e.target.contains(this.clickOnAddVideoDivRef.current)) this.clickOnAddVideoBtnRef.current.click()
  }

  addVideos = e => {
    let button = document.getElementsByClassName("add-video")
    let r = new Resumable({
      target: `${process.env.REACT_APP_TRANSCODING_SERVER_URI}/api/uploadvideovideoset`,
      chunkSize: 1 * 1024 * 1024,
      maxFiles: 100,
      simultaneousUploads: 5,
      fileType: ['mp4', 'avi', 'ogg', 'mov', 'wave'],
      query: { userId: this.props.userId }
    })
    r.assignBrowse(button)
    this.setState({
      r: r
    }, () => {
      this.state.r.on('progress', () => {

      })
      this.state.r.on('complete', () => {
        axios.post(`${process.env.REACT_APP_TRANSCODING_SERVER_URI}/api/postvideoset`, { seller: this.props.userId, formDataVideos: this.state.formDataVideos, videosLength: this.state.fileName })
          .then(data => {
            if (data.data == "Ok") {
              this.setContainer("successVideos")
              window.gtag('event', `Item online`, {
                'event_category': 'user',
                'event_label': `Video set`
              })
            }
            else if (data.data === "No videos") {
              this.setState({
                errorMessageVideos: "Please select at least 1 video"
              })
              this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
            }
            else if (data.data == "Wrong ext") {
              this.setState({
                errorMessageVideos: "Only jpeg, png, gif files are allowed"
              })
              this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
            }
            else {
              this.setState({
                errorMessageVideos: data.data
              })
              this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
            }
          })
          .catch(error => {
            console.log(error)
          })
      })
      this.state.r.on('fileAdded', function (file, event) {
        const { fileName } = this.state
        let fileNameArray = fileName
        fileNameArray.push(file.fileName)
        this.setState({
          fileName: fileNameArray
        })
      }.bind(this))
      this.state.r.on('fileSuccess', function (file, event) {

      }.bind(this))
    })
  }

  render() {
    if (this.state.loaded) {
      const { name, description, img, price, notif } = this.state.formData
      return (
        <div className="popUpLog">
          {this.state.containerOn == 'whatItemSelling' && <div className="container-pop-up-modify-my-item">
            <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpPostItem()}></div>
            <div className="pop-up-modify-my-item">
              <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpPostItem()}></i>
              <h2 className="text-align-center clear">What item are you selling</h2>
              <div className="what-item-selling-box-container">
                <div className="what-item-selling-box what-item-selling-box-clothes" onClick={() => this.setContainer('Clothes')}>
                  <img className="what-item-selling-box-icon-svg" src={`${process.env.REACT_APP_URI}/images/bikini.svg`} alt="Bikini" />
                  <p className="what-item-selling-box-title">Worn lingerie</p>
                </div>
                <div className="what-item-selling-box what-item-selling-box-pictures" onClick={() => this.setContainer('Pictures')}>
                  <i className="far fa-images what-item-selling-box-icon"></i>
                  <p className="what-item-selling-box-title">Pictures</p>
                </div>
                <div className="what-item-selling-box what-item-selling-box-pictures" onClick={() => this.setContainer('Videos')}>
                  <i className="fas fa-film what-item-selling-box-icon"></i>
                  <p className="what-item-selling-box-title">Videos</p>
                </div>
              </div>
            </div>
          </div>}
          {this.state.containerOn == 'Clothes' && <div className="container-pop-up-modify-my-item">
            <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpPostItem()}></div>
            <div className="pop-up-modify-my-item">
              <span onClick={() => this.setContainer("whatItemSelling")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
              <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpPostItem()}></i>
              <h2 className="text-align-center clear">Post your item</h2>
              <form className="post-form" ref={input => this.postForm = input} onSubmit={e => this.createPost(e)}>
                <div className="form-group modify-my-item-input">
                  <input type="text" name="name" className="form-control" value={name} onChange={this.handleChange} placeholder="Name *" required />
                </div>
                <div className="form-group modify-my-item-input">
                  <input type="text" name="description" className="form-control" value={description} onChange={this.handleChange} placeholder="Description *" required />
                </div>
                <div className="form-group modify-my-item-input ModifyItemPictureForm">
                  <label htmlFor="img">Picture *</label>
                  <input type="file" name="img" id="img" className="form-control" onChange={this.handleFileUpload} accept="image/x-png,image/gif,image/jpeg" required />
                </div>
                <label htmlFor="pictures">Upload up to 8 pictures (hold cmd/alt to select multiple)</label>
                <input type="file" name="pictures" className="form-control modify-my-profile-input" onChange={this.handleFilesUpload} accept="image/png, image/jpeg" multiple />
                <div className="input-group modify-my-item-input">
                  <input type="number" className="form-control" name="price" step="any" min="0" value={price} onChange={this.handleChange} placeholder="Price in tokens *" aria-label="Amount (to the nearest dollar)" required />
                </div>
                <span className="price-item-value">Value: ${price / 10}</span>
                <div className="shipping-options">
                  <h3 className="shipping-options-title">Shipping options</h3>
                  <input type="radio" name="paddedEnvelope" id="paddedEnvelope" value={this.state.formData.shippingOption} defaultChecked={this.state.formData.shippingOption} />
                  <label htmlFor="paddedEnvelope" className="padded-envelope-title"> Flate rate padded envelope </label>
                  <span onClick={this.toggleRestrictions} className="restriction-padded-envelope"> Restrictions</span>
                  {this.state.showRestriction &&
                    <p>
                      <span>Size: 9-1/2 in x 12-1/2 in</span> <br />
                      When sealing, the container flaps must be able to close within the normal folds. <br />
                      Tape may be applied to the flaps and seams to reinforce the container, provided the design of the container is not enlarged by opening the sides and the container is not reconstructed in any way.
                    </p>
                  }
                </div>
                <button className="btn btn-primary intimy-rectangle-button submit-button-post-item" type="submit">Submit</button>
              </form>
            </div>
          </div>}
          {this.state.containerOn == 'Pictures' &&
            <div className="container-pop-up-modify-my-item">
              <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpPostItem()}></div>
              <div className="pop-up-modify-my-item">
                <span onClick={() => this.setContainer("whatItemSelling")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
                <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpPostItem()}></i>
                {this.state.isLoadingPictures ?
                  <div className="spinner-loader-container">
                    <img src={spinner} alt="Spinner loader" className="spinner-loader" />
                    <p className="uplaoding-the-pictures-loading">Uploading the pictures...</p>
                  </div>
                  :
                  <>
                    <h2 className="text-align-center clear">Post a set of pictures</h2>
                    <form className="post-form" ref={input => this.postForm = input} onSubmit={e => this.createPostPictures(e)}>
                      <div className="form-group modify-my-item-input">
                        <input type="text" name="name" className="form-control" value={this.state.formDataPictures.name} onChange={this.handleChangePictures} placeholder="Name *" required />
                      </div>
                      <div className="form-group modify-my-item-input">
                        <input type="text" name="description" className="form-control" value={this.state.formDataPictures.description} onChange={this.handleChangePictures} placeholder="Description *" required />
                      </div>
                      <label htmlFor="pictures">Upload up to 100 pictures (hold cmd/alt to select multiple)</label>
                      <input type="file" name="pictures" className="form-control modify-my-profile-input" onChange={this.handleFilesUploadPictures} accept="image/png, image/jpeg" multiple required />
                      <div className="input-group modify-my-item-input">
                        <input type="number" className="form-control" name="price" step="any" min="0" value={this.state.formDataPictures.price} onChange={this.handleChangePictures} placeholder="Price in tokens *" aria-label="Amount (to the nearest dollar)" required />
                      </div>
                      <span className="price-item-value">Value: ${this.state.formDataPictures.price / 10}</span>
                      <button className="btn btn-primary intimy-rectangle-button submit-button-post-item" type="submit">Submit</button>
                    </form>
                  </>}
              </div>
            </div>}
          {this.state.containerOn == 'Videos' && <div className="container-pop-up-modify-my-item">
            <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpPostItem()}></div>
            <div className="pop-up-modify-my-item" ref={this.modifyPopUp}>
              <span onClick={() => this.setContainer("whatItemSelling")}><i className="fas fa-chevron-left chevron-left-pop-up"></i></span>
              <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpPostItem()}></i>
              {this.state.isLoadingVideos ?
                <div className="spinner-loader-container">
                  <img src={spinner} alt="Spinner loader" className="spinner-loader" />
                  <p className="uplaoding-the-pictures-loading">Uploading the videos...</p>
                </div>
                :
                <>
                  <h2 className="text-align-center clear">Post a set of videos</h2>
                  <form className="post-form" onSubmit={e => this.createPostVideos(e)}>
                    <div className="form-group modify-my-item-input">
                      <input type="text" name="name" className="form-control" value={this.state.formDataVideos.name} onChange={this.handleChangeVideos} placeholder="Name *" required />
                    </div>
                    <div className="form-group modify-my-item-input">
                      <input type="text" name="description" className="form-control" value={this.state.formDataVideos.description} onChange={this.handleChangeVideos} placeholder="Description *" required />
                    </div>
                    <label htmlFor="pictures">Upload videos (hold cmd/alt to select multiple)</label>
                    <div className="modify-my-profile-input-video" tabIndex="0" onClick={(e) => this.clickOnAddVideoBtn(e)} ref={this.clickOnAddVideoDivRef}>
                      <button type="button" className="add-video" ref={this.clickOnAddVideoBtnRef}>Choose Files</button>
                    </div>
                    {this.state.fileName.length > 0 && <div className="results">{this.state.fileName.length} Files</div>}
                    <div className="input-group modify-my-item-input">
                      <input type="number" className="form-control" name="price" step="any" min="0" value={this.state.formDataVideos.price} onChange={this.handleChangeVideos} placeholder="Price in tokens *" aria-label="Amount (to the nearest dollar)" required />
                    </div>
                    <span className="price-item-value">Value: ${this.state.formDataVideos.price / 10}</span>
                    <button className="btn btn-primary intimy-rectangle-button submit-button-post-item" type="submit">Submit</button>
                    {this.state.errorMessageVideos && <p className="error-message-modify-profile">{this.state.errorMessageVideos}</p>}
                  </form>
                </>}
            </div>
          </div>}
          {this.state.containerOn === "successPictures" &&
            <div className="popUpLog">
              <div className="container-pop-up-login">
                <div className="background-pop-up-login" onClick={() => this.props.closePopUpPostItem()}></div>
                <div className="pop-up-login">
                  <i className="fas fa-times close-pop-up-login" onClick={() => this.props.closePopUpPostItem()}></i>
                  <div className="tokens-added-icon">
                    <i className="fas fa-check-circle"></i>
                  </div>
                  <p className="tokens-added-message">Your pictures set is online.</p>
                </div>
              </div>
            </div>
          }
          {this.state.containerOn === "successVideos" &&
            <div className="popUpLog">
              <div className="container-pop-up-login">
                <div className="background-pop-up-login" onClick={() => this.props.closePopUpPostItem()}></div>
                <div className="pop-up-login">
                  <i className="fas fa-times close-pop-up-login" onClick={() => this.props.closePopUpPostItem()}></i>
                  <div className="tokens-added-icon">
                    <i className="fas fa-check-circle"></i>
                  </div>
                  <p className="tokens-added-message">Your videos set is online. <br /> It can takes few minutes to appear.</p>
                </div>
              </div>
            </div>
          }
        </div>
      )
    }
    else {
      return null
    }
  }
}

export default userConnexionLoader(onClickOutside(PopUpPostItem))