//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import axios from 'axios'
import onClickOutside from 'react-onclickoutside'
import ImageCompressor from 'image-compressor.js'
import USStates from '../../files/states_hash.json'
import spinner from '../../images/spinner.gif'
import Resumable from 'resumablejs'

class PopUpModifyMyProfile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      isLoadingVideo: false,
      focused: false,
      popUpModifyMyProfile: false,
      errorMessage: "",
      focused: false,
      picturesLimitError: false,
      picturesLimitLength: "",
      fileName: [],
      r: "",
      modifyMyProfileSeller: {
        profilPicture: "",
        pictures: [],
        videos: [],
        username: this.props.profilData.username || "",
        bio: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.bio : "",
        hairs: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.hairs : "",
        height: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.height : "",
        eyes: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.eyes : "",
        interestedIn: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.interestedIn : "",
        status: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.status : "",
        measurements: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.measurements : "",
        tattoos: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.tattoos : "",
        piercings: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.piercings : "",
        whatILike: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.whatILike : "",
        whatIDontLike: this.props.profilData.aboutMe ? this.props.profilData.aboutMe.whatIDontLike : "",
        notif: false,
        itemPurchased: this.props.profilData.emailPreferences ? this.props.profilData.emailPreferences.itemPurchased : false,
        itemCommentPosted: this.props.profilData.emailPreferences ? this.props.profilData.emailPreferences.itemCommentPosted : false,
      },
      modifyMyShippingDetails: {
        firstname: this.props.profilData.firstname || "",
        lastname: this.props.profilData.lastname || "",
        streetaddress: this.props.profilData.streetaddress || "",
        apt: this.props.profilData.apt || "",
        city: this.props.profilData.city || "",
        state: this.props.profilData.state || "",
        zipcode: this.props.profilData.zipcode || "",
        country: this.props.profilData.country || ""
      },
      modifyMyProfileBuyer: {
        profilPicture: "",
        username: ""
      }
    }
    this.modifyPopUp = React.createRef()
    this.clickOnAddVideoBtnRef = React.createRef()
    this.clickOnAddVideoDivRef = React.createRef()
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
    let button = document.getElementsByClassName("add-video")

    let r = new Resumable({
      target: `${process.env.REACT_APP_TRANSCODING_SERVER_URI}/api/uploadvideo`,
      chunkSize: 1 * 1024 * 1024,
      maxFiles: 4,
      simultaneousUploads: 5,
      fileType: ['mp4', 'avi', 'ogg', 'mov', 'wave'],
      query: { userId: this.props.profilData._id }
    })
    r.assignBrowse(button)
    this.setState({
      r: r
    }, () => {
      this.state.r.on('progress', () => {
        this.setState({
          isLoadingVideo: true
        })
      })
      this.state.r.on('fileAdded', function (file, event) {
        const { fileName } = this.state
        let fileNameArray = fileName
        fileNameArray.push(file.fileName)
        this.setState({
          fileName: fileNameArray
        })
      }.bind(this))
      this.state.r.on('fileSuccess', function (file, event) {
        let editedProfil

        if (this.props.userStatus == "seller") {
          editedProfil = this.state.modifyMyProfileSeller
        }
        if (this.props.userStatus == "buyer") {
          editedProfil = this.state.modifyMyProfileBuyer
        }
        axios.put(`${process.env.REACT_APP_URI}/api/modifyMyProfile`, { editedProfil }, {
          withCredentials: true
        })
          .then(data => {
            if (data.data == "Profile updated!") {
              this.props.setStateMessages(data.data)
              this.props.toggleModify('successVideo')
              window.gtag('event', `Profile modified`, {
                'event_category': 'user',
                'event_label': `Profile modified`
              })
            }
            else if (data.data == "Wrong ext") {
              this.setState({
                errorMessage: "Only jpeg, png, gif files are allowed"
              })
            }
            else if (data.data.picturesLength) {
              this.setState({
                picturesLimitError: true,
                picturesLimitLength: (8 - data.data.picturesLength)
              })
              this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
            }
            else {
              this.setState({
                errorMessage: data.data
              })
            }
          })
          .catch(error => {
            console.log(error)
          })
        axios.post(`${process.env.REACT_APP_TRANSCODING_SERVER_URI}/api/transcodevideointimy`, { seller: this.props.profilData._id, file: file.file.uniqueIdentifier }, {
          withCredentials: true
        })
          .then(data => {
            if (data.data == "Profile updated!") {
              this.props.setStateMessages(data.data)
              this.props.closePopUpModify()
              window.gtag('event', `Profile modified`, {
                'event_category': 'user',
                'event_label': `Profile modified`
              })
            }
            else if (data.data == "Wrong ext") {
              this.setState({
                errorMessage: "Only jpeg, png, gif files are allowed"
              })
            }
            else if (data.data.picturesLength) {
              this.setState({
                picturesLimitError: true,
                picturesLimitLength: (8 - data.data.picturesLength)
              })
              this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
            }
            else {
              this.setState({
                errorMessage: data.data
              })
            }
          })
          .catch(error => {
            console.log(error)
          })
        console.log("success")
      }.bind(this))
    })
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  focused = element => {
    this.setState({
      focused: element
    })
  }

  handleChangeSeller = e => {
    const { value, name, checked } = e.target
    const { modifyMyProfileSeller } = this.state
    let interestedInArray = [...modifyMyProfileSeller.interestedIn]
    if (e.target.type == "checkbox") {
      if (!e.target.checked) {
        if (e.target.name === "interestedIn") {
          var index = interestedInArray.indexOf(e.target.value)
          interestedInArray.splice(index, 1)
        }
      }
      else {
        if (e.target.name === "interestedIn") {
          interestedInArray = [...modifyMyProfileSeller.interestedIn, value]
        }
      }
    }
    this.setState({
      modifyMyProfileSeller: {
        ...modifyMyProfileSeller,
        [name]: e.target.type === "checkbox" ? e.target.name === "interestedIn" ? interestedInArray : checked : value
      }
    })
  }

  handleChangeBuyer = e => {
    const { value, name } = e.target
    const { modifyMyProfileBuyer } = this.state
    this.setState({
      modifyMyProfileBuyer: {
        ...modifyMyProfileBuyer,
        [name]: value
      }
    })
  }

  handleChangeShippingDetails = e => {
    const { value, name } = e.target
    const { modifyMyShippingDetails } = this.state
    this.setState({
      modifyMyShippingDetails: {
        ...modifyMyShippingDetails,
        [name]: value
      }
    })
  }

  modifyMyProfile = e => {
    e.preventDefault()
    if (this.state.modifyMyProfileBuyer.profilPicture || this.state.modifyMyProfileSeller.profilPicture) {
      this.setState({
        isLoading: true
      })
    }
    let editedProfil
    if (this.props.userStatus == "seller") {
      editedProfil = this.state.modifyMyProfileSeller
    }
    if (this.props.userStatus == "buyer") {
      editedProfil = this.state.modifyMyProfileBuyer
    }

    if (this.state.fileName.length <= 0) {
      axios.put(`${process.env.REACT_APP_URI}/api/modifyMyProfile`, { editedProfil }, {
        withCredentials: true
      })
        .then(data => {
          if (data.data == "Profile updated!") {
            this.props.setStateMessages(data.data)
            this.props.closePopUpModify()
            window.gtag('event', `Profile modified`, {
              'event_category': 'user',
              'event_label': `Profile modified`
            })
          }
          else if (data.data == "Wrong ext") {
            this.setState({
              errorMessage: "Only jpeg, png, gif files are allowed"
            })
          }
          else if (data.data.picturesLength) {
            this.setState({
              picturesLimitError: true,
              picturesLimitLength: (8 - data.data.picturesLength)
            })
            this.modifyPopUp.current.scrollTop = this.modifyPopUp.current.scrollHeight
          }
          else {
            this.setState({
              errorMessage: data.data
            })
          }
        })
        .catch(error => {
          console.log(error)
        })
    }
    else if (this.state.fileName) {
      this.state.r.upload()
    }
  }

  modifyMyShippingDetails = e => {
    e.preventDefault()
    let editedProfil

    editedProfil = this.state.modifyMyShippingDetails

    axios.put(`${process.env.REACT_APP_URI}/api/modifymyshippingdetails`, editedProfil, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Shipping details updated!") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
          window.gtag('event', `Shipping details updated`, {
            'event_category': 'user',
            'event_label': `Shipping details updated`
          })
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  handleFileUpload = e => {
    e.preventDefault()
    const { name } = e.target
    let files = Array.from(e.target.files)
    let filesResults = []
    let attributeName = (() => {
      if (this.props.userStatus == "seller") return "modifyMyProfileSeller"
      else if (this.props.userStatus == "buyer") return "modifyMyProfileBuyer"
      return null
    })()
    let promise = files.map(file => new Promise(resolve => {
      let reader = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }

      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        [attributeName]: {
          ...this.state[attributeName],
          [name]: filesResults
        }
      })
    })
  }

  checkboxChange = e => {
    const { name } = e.target
    const { modifyMyProfileSeller } = this.state
    this.setState({
      modifyMyProfileSeller: {
        ...modifyMyProfileSeller,
        [name]: !modifyMyProfileSeller[name]
      }
    })
  }

  focused = element => {
    this.setState({
      focused: element
    })
  }

  clickOnAddVideoBtn = e => {
    if (e.target.contains(this.clickOnAddVideoDivRef.current)) this.clickOnAddVideoBtnRef.current.click()
  }

  render() {
    const statesUS = Object.keys(USStates).map((state, index) =>
      <option key={index} value={state}>{state}</option>
    )
    const fileName = this.state.fileName.map((fileName, index) =>
      <div key={index}>
        <span>{fileName}</span>
      </div>
    )
    return (
      <div>
        {this.props.showModify === "shippingDetails" &&
          <div className="popUpLog">
            <div className="container-pop-up-modify-my-buyer-profil">
              <div className="background-pop-up-modify-my-buyer-profil" onClick={() => this.props.closePopUpModify()}></div>
              <div className="pop-up-sign-up-buyer">
                <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpModify()}></i>
                <h2 className="text-align-center clear">Edit my profile</h2>
                <form className="form-modify form-group post-form" ref={input => this.postFormSeller = input} onSubmit={e => this.modifyMyShippingDetails(e)}>
                  <h3>My shipping informations</h3>
                  <input type="text" name="firstname" className="form-control modify-my-profile-input" value={this.state.modifyMyShippingDetails.firstname} onChange={this.handleChangeShippingDetails} placeholder="First Name" required />
                  <input type="text" name="lastname" className="form-control modify-my-profile-input" value={this.state.modifyMyShippingDetails.lastname} onChange={this.handleChangeShippingDetails} placeholder="Last Name" required />
                  <input type="text" name="streetaddress" className="form-control modify-my-profile-input" value={this.state.modifyMyShippingDetails.streetaddress} onChange={this.handleChangeShippingDetails} placeholder="Street Address" required />
                  <input type="text" name="apt" className="form-control modify-my-profile-input" value={this.state.modifyMyShippingDetails.apt} onChange={this.handleChangeShippingDetails} placeholder="Apt #, Floor, etc..." />
                  <input type="text" name="city" className="form-control modify-my-profile-input" value={this.state.modifyMyShippingDetails.city} onChange={this.handleChangeShippingDetails} placeholder="City" required />
                  <div className={`selectWrapper ${this.state.focused === "select2" ? "focused" : ""}`}>
                    <select onFocus={() => this.focused("select2")} onBlur={() => this.focused(false)} name="country" id="country" className={`form-control modify-my-profile-select`} value={this.state.modifyMyShippingDetails.country} onChange={this.handleChangeShippingDetails} required>
                      <option value="" disabled hidden>Country *</option>
                      <option value="FR">France</option>
                      <option value="US">United States</option>
                    </select>
                  </div>
                  {this.state.modifyMyShippingDetails.country === "US" &&
                    <div className={`selectWrapper ${this.state.focused === "select3" ? "focused" : ""}`}>
                      <select onFocus={() => this.focused("select3")} onBlur={() => this.focused(false)} className={`form-control modify-my-profile-select`} name="state" id="statesUS" value={this.state.modifyMyShippingDetails.state} onChange={this.handleChangeShippingDetails} required>
                        <option value="" disabled hidden>State *</option>
                        {statesUS}
                      </select>
                    </div>
                  }
                  <input type="text" name="zipcode" className="form-control modify-my-profile-input" value={this.state.modifyMyShippingDetails.zipcode} onChange={this.handleChangeShippingDetails} placeholder="Zip Code" required />
                  <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
                </form>
              </div>
            </div>
          </div>
        }

        {this.props.showModify === "myInfos" &&
          <div className="popUpLog">
            <div className="container-pop-up-modify-my-buyer-profil">
              <div className="background-pop-up-modify-my-buyer-profil" onClick={() => this.props.closePopUpModify()}></div>
              <div className="pop-up-sign-up-buyer" ref={this.modifyPopUp}>
                <i className="fas fa-times close-pop-up-billing" onClick={() => this.props.closePopUpModify()}></i>
                {this.state.isLoadingVideo ?
                  <div className="spinner-loader-container">
                    <img src={spinner} alt="Spinner loader" className="spinner-loader" />
                    <p className="uplaoding-the-pictures-loading">Uploading the videos...</p>
                  </div>
                  :
                  this.state.isLoading ?
                    <div className="spinner-loader-container">
                      <img src={spinner} alt="Spinner loader" className="spinner-loader" />
                      <p className="uplaoding-the-pictures-loading">Uploading the pictures...</p>
                    </div>
                    :
                    <>
                      <h2 className="text-align-center clear">Edit my profile</h2>
                      {this.props.userStatus == "seller" && <form className="form-modify form-group post-form" ref={input => this.postFormSeller = input} onSubmit={e => this.modifyMyProfile(e)}>
                        <label htmlFor="profil-picture">Profile picture</label>
                        <input type="file" name="profilPicture" className="form-control modify-my-profile-input" onChange={this.handleFileUpload} accept="image/png, image/jpeg" />
                        <label htmlFor="pictures">Upload pictures (hold cmd/alt to select multiple)</label>
                        <input type="file" name="pictures" className="form-control modify-my-profile-input" onChange={this.handleFileUpload} accept="image/png, image/jpeg" multiple />
                        <label htmlFor="videos">Upload videos (hold cmd/alt to select multiple)</label>
                        <div className="modify-my-profile-input-video" tabIndex="0" onClick={(e) => this.clickOnAddVideoBtn(e)} ref={this.clickOnAddVideoDivRef}>
                          <button type="button" className="add-video" ref={this.clickOnAddVideoBtnRef}>Choose Files</button>
                        </div>
                        <div id="results" className="results">{fileName}</div>
                        <input type="text" name="username" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileSeller.username} onChange={this.handleChangeSeller} placeholder="Username" maxLength="14" />
                        <textarea type="text" name="bio" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileSeller.bio} onChange={this.handleChangeSeller} placeholder="Bio" maxLength="140"></textarea>
                        <div className={`selectWrapper ${this.state.focused === "select1" ? "focused" : ""}`}>
                          <select onFocus={() => this.focused("select1")} onBlur={() => this.focused(false)} name="hairs" id="hairs" className="form-control modify-my-profile-select" value={this.state.modifyMyProfileSeller.hairs} onChange={this.handleChangeSeller}>
                            <option value="" disabled hidden>Hairs</option>
                            <option value="Blonde">Blonde</option>
                            <option value="Brown">Brown</option>
                            <option value="Black">Black</option>
                            <option value="Red">Red</option>
                            <option value="Silver">Silver</option>
                            <option value="Colored">Colored</option>
                          </select>
                        </div>
                        <input type="number" name="height" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileSeller.height} onChange={this.handleChangeSeller} placeholder="Height" />
                        <div className={`selectWrapper ${this.state.focused === "select2" ? "focused" : ""}`}>
                          <select onFocus={() => this.focused("select2")} onBlur={() => this.focused(false)} name="eyes" id="eyes" className="form-control modify-my-profile-select" value={this.state.modifyMyProfileSeller.eyes} onChange={this.handleChangeSeller}>
                            <option value="" disabled hidden>Eyes</option>
                            <option value="Blue">Blue</option>
                            <option value="Brown">Brown</option>
                            <option value="Green">Green</option>
                            <option value="Hazel">Hazel</option>
                            <option value="Silver">Silver</option>
                            <option value="Amber">Amber</option>
                          </select>
                        </div>
                        <div className={`selectWrapper ${this.state.focused === "select3" ? "focused" : ""}`}>
                          <select onFocus={() => this.focused("select3")} onBlur={() => this.focused(false)} name="status" id="status" className="form-control modify-my-profile-select" value={this.state.modifyMyProfileSeller.status} onChange={this.handleChangeSeller}>
                            <option value="" disabled hidden>Status</option>
                            <option value="Single">Single</option>
                            <option value="Couple">Couple</option>
                            <option value="Nothing serious">Nothing serious</option>
                          </select>
                        </div>
                        <div className="radio-group">
                          <p>Interested in:</p>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input radio" type="checkbox" id="interestedInMen" name="interestedIn" value="Men" defaultChecked={this.state.modifyMyProfileSeller.interestedIn.includes("Men")} onChange={this.handleChangeSeller} />
                            <label className="form-check-label" htmlFor="interestedInMen">Men</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input radio" type="checkbox" id="interestedInWomen" name="interestedIn" value="Women" defaultChecked={this.state.modifyMyProfileSeller.interestedIn.includes("Women")} onChange={this.handleChangeSeller} />
                            <label className="form-check-label" htmlFor="interestedInWomen">Women</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input radio" type="checkbox" id="interestedInCouple" name="interestedIn" value="Couple" defaultChecked={this.state.modifyMyProfileSeller.interestedIn.includes("Couple")} onChange={this.handleChangeSeller} />
                            <label className="form-check-label" htmlFor="interestedInCouple">Couple</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input radio" type="checkbox" id="interestedIntrans" name="interestedIn" value="Trans" defaultChecked={this.state.modifyMyProfileSeller.interestedIn.includes("Trans")} onChange={this.handleChangeSeller} />
                            <label className="form-check-label" htmlFor="interestedIntrans">Trans</label>
                          </div>
                        </div>
                        <input type="text" name="measurements" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileSeller.measurements} onChange={this.handleChangeSeller} placeholder="Measurements" maxLength="20" />
                        <div className="radio-group">
                          <div className="form-check form-check-inline">
                            <span className="legend-radio-form">Tattoos?</span>
                            <label htmlFor="tattoos-yes" className="form-check-label"><input type="radio" name="tattoos" className="form-check-input radio" id="tattoos-yes" value="Yes" defaultChecked={this.state.modifyMyProfileSeller.tattoos === "Yes"} onChange={this.handleChangeSeller} /> Yes</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <label htmlFor="tattoos-no" className="form-check-label"><input type="radio" name="tattoos" className="form-check-input radio" id="tattoos-no" value="No" defaultChecked={this.state.modifyMyProfileSeller.tattoos === "No"} onChange={this.handleChangeSeller} /> No</label>
                          </div>
                        </div>
                        <div className="radio-group">
                          <div className="form-check form-check-inline">
                            <span className="legend-radio-form">Piercings?</span>
                            <input type="radio" name="piercings" className="form-check-input radio" id="piercings-yes" value="Yes" defaultChecked={this.state.modifyMyProfileSeller.piercings === "Yes"} onChange={this.handleChangeSeller} />
                            <label htmlFor="piercings-yes" className="form-check-label">Yes</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input type="radio" name="piercings" className="form-check-input radio" id="piercings-no" value="No" defaultChecked={this.state.modifyMyProfileSeller.piercings === "No"} onChange={this.handleChangeSeller} />
                            <label htmlFor="piercings-no" className="form-check-label">No</label>
                          </div>
                        </div>
                        <input type="text" name="whatILike" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileSeller.whatILike} onChange={this.handleChangeSeller} placeholder="What I like" />
                        <input type="text" name="whatIDontLike" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileSeller.whatIDontLike} onChange={this.handleChangeSeller} placeholder="What I don't like" />
                        <div className="form-check post-item-form-check">
                          <div className="email-prefs-title">Email preferences</div>
                          <div className="email-prefs-container">
                            <input type="checkbox" name="itemPurchased" id="itemPurchased" defaultChecked={this.state.modifyMyProfileSeller.itemPurchased} onChange={this.handleChangeSeller} className="intimy-checkbox" />
                            <label htmlFor="itemPurchased" className="get-notified-item"> An item was purchased</label>
                          </div>
                          <div className="email-prefs-container">
                            <input type="checkbox" name="itemCommentPosted" id="itemCommentPosted" defaultChecked={this.state.modifyMyProfileSeller.itemCommentPosted} onChange={this.handleChangeSeller} className="intimy-checkbox" />
                            <label htmlFor="itemCommentPosted" className="get-notified-item"> Someone post a comment on your items</label>
                          </div>
                        </div>
                        <br />
                        <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
                        <br />
                        <div className="error-message-modify-profile">{this.state.errorMessage}</div>
                        {this.state.picturesLimitError && <p className="picturesLimitError">Pictures limit reached<br />Remaining: {this.state.picturesLimitLength}</p>}
                      </form>}

                      {this.props.userStatus == "buyer" && <form className="form-modify form-group post-form" ref={input => this.postFormBuyer = input} onSubmit={e => this.modifyMyProfile(e)}>
                        <label htmlFor="profil-picture">Profile picture</label>
                        <input type="file" name="profilPicture" className="form-control modify-my-profile-input" onChange={this.handleFileUpload} />
                        <input type="text" name="username" className="form-control modify-my-profile-input" value={this.state.modifyMyProfileBuyer.username} onChange={this.handleChangeBuyer} placeholder="Username" maxLength="14" />
                        <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
                        <br />
                        <div className="error-message-modify-profile">{this.state.errorMessage}</div>
                      </form>}
                    </>}
              </div>
            </div>
          </div>
        }
        {this.props.showModify === "successVideo" &&
          <div className="popUpLog">
            <div className="container-pop-up-login">
              <div className="background-pop-up-login" onClick={() => this.props.closePopUpModify()}></div>
              <div className="pop-up-login">
                <i className="fas fa-times close-pop-up-login" onClick={() => this.props.closePopUpModify()}></i>
                <div className="tokens-added-icon">
                  <i className="fas fa-check-circle"></i>
                </div>
                <p className="tokens-added-message">Your videos are transcoding. They will appear soon on your profile.</p>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default onClickOutside(PopUpModifyMyProfile)