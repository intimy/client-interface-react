//React
import React from 'react'
//Components
import Footer from './footer'
import Header from './header'
import MyRouter from './myRouter'
import PopUpLogIn from './PopUp/PopUpLogIn'
import socketIOClient from 'socket.io-client'
import axios from 'axios'
//Router
import { BrowserRouter as Router, Route } from 'react-router-dom'
import UserConnexionLoader from './UserConnectionLoader'
import SocketContext from './react-context/SocketContext'

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showLogin: false
    }
    this.socket = socketIOClient(`${process.env.REACT_APP_URI}/notifications`, {
      query: { userId: this.props.userId },
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionAttempts: Infinity,
      transports: ['websocket']
    })
  }

  toggleLogIn = () => {
    this.setState({ showLogin: !this.state.showLogin })
  }

  closePopUpLogIn = () => {
    this.setState({ showLogin: !this.state.showLogin })
  }

  render() {
    return (
      <div className="app">
        <SocketContext.Provider value={this.socket}>
          <Header className="header" toggleLogIn={this.toggleLogIn} />
          <div className="app-content">
            <MyRouter />
          </div>
          {this.state.showLogin && <PopUpLogIn closePopUpLogIn={this.closePopUpLogIn} />}
          <Footer />
        </SocketContext.Provider>
      </div>
    )
  }
}

export default UserConnexionLoader(App)