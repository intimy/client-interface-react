//React
import React from 'react'
//Router
import axios from 'axios'
import userConnexionLoader from '../UserConnectionLoader'
import { spawn } from 'child_process';

class Gallery extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            picture: null,
            profilData: this.props.items,
            openedGallery: false,
            index: null,
            messages: null
        }
        this.divGalleryFocus = React.createRef()
    }

    componentDidUpdate(prevProps) {
        if (this.props.items !== prevProps.items) {
            this.setState({
                profilData: this.props.items
            })
        }
        this.preloadImage(this.state.profilData.pictures)
    }

    eventHandler = e => {
        if (e.keyCode === 37) {
            this.previousPicture()
        }
        else if (e.keyCode === 39) {
            this.nextPicture()
        }
        else if (e.keyCode === 27) {
            this.closeGalerry()
        }
    }

    deletePicture = (item, index) => {
        axios.delete(`${process.env.REACT_APP_URI}/api/deletePicture`, {
            params: {
                userId: this.props.userId,
                posterId: this.state.profilData._id,
                id: item._id
            },
            withCredentials: true
        })
            .then(data => {
                if (data.data === "You are not authorized") {
                    this.setState({
                        messages: "You are not authorized"
                    })
                }
                else {
                    const pictures = [...this.state.profilData.pictures]
                    pictures.splice(index, 1)
                    const { profilData } = this.state
                    this.setState({
                        profilData: {
                            ...profilData,
                            pictures: pictures
                        }
                    })
                }
            })
    }

    openGallery = index => {
        this.setState({
            openedGallery: !this.state.openedGallery,
            index: index
        }, () => {
            this.divGalleryFocus.current.focus()
        })
    }

    closeGalerry() {
        this.setState({
            openedGallery: !this.state.openedGallery
        })
    }

    previousPicture = () => {
        if (this.state.profilData.pictures.length > 1) {
            const index = this.state.index === 0 && this.state.profilData.pictures.length - 1 || this.state.index - 1
            this.setState({
                index: index
            })
        }
    }

    nextPicture = () => {
        const nextIndex = this.state.index + 1
        const index = nextIndex === this.state.profilData.pictures.length ? 0 : this.state.index + 1
        this.setState({
            index: index
        })
    }

    preloadImage = url => {
        for (let index = 0; index < url.length; index++) {
            const element = url[index];
            let img = new Image()
            img.src = element.src
        }
    }

    render() {
        const pictures = this.state.profilData.pictures.map((picture, index) =>
            <div key={index} className="thumbnail-picture-gallery-container">
                <img onClick={() => this.openGallery(index)} src={picture.thumbnail} alt="Picture gallery" className="thumbnail-picture-gallery" />
                {this.props.isAdmin && <i className="fas fa-times-circle delete-picture-button" onClick={() => this.deletePicture(picture, index)}></i>}
                {picture.kind === "video" && <i onClick={() => this.openGallery(index)} className="fas fa-play play-icon-thumbnails"></i> }
            </div>
        
    )
        return (
            <div>
                {pictures}

                {this.state.messages}
                {this.state.openedGallery &&
                    <div className="opened-gallery" onKeyDown={(e) => this.eventHandler(e)} tabIndex="0" ref={this.divGalleryFocus}>
                        <div className="top-bar-gallery"></div>
                        <i onClick={() => this.closeGalerry()} className="fas fa-times close-gallery"></i>
                        {!this.state.profilData.pictures[this.state.index].kind && <img rel="preload" src={this.state.profilData.pictures[this.state.index].src} alt="Picture gallery" />}
                        {this.state.profilData.pictures[this.state.index].kind === "video" && <video width="auto" alt="Picture gallery" controls controlsList="nodownload" src={this.state.profilData.pictures[this.state.index].src}></video>}
                        <div className="gallery-arrows-container">
                            <i onClick={() => this.previousPicture()} className="fas fa-arrow-left arrow-left-gallery"></i>
                            <i onClick={() => this.nextPicture()} className="fas fa-arrow-right arrow-right-gallery"></i>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default userConnexionLoader(Gallery)