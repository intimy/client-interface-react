import React from 'react'
import axios from 'axios'
import { observer } from 'mobx-react'
import SearchBarStore from "./stores/SearchBarStore"
import { withRouter } from 'react-router-dom'

class SearchFormMobile extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            searchForm: {
                origins: "",
                username: "",
                age: ""
            }
        }
        this.childRef = React.createRef()
        this.childRef2 = React.createRef()
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClick, false)
    }

    componentWillReact() {
        if (this.props.showSearch) document.addEventListener('click', this.handleClick, false)
        else document.removeEventListener('click', this.handleClick, false)
    }

    handleClick = e => {
        if (e.target == this.props.self || this.childRef.current.contains(e.target)) return
        else if (this.props.showSearch) this.handleOutsideClick()
    }

    handleOutsideClick = () => {
        this.props.showMobileSearch()
    }

    searchForm = (e) => {
        e.preventDefault()
        const { origins, username, age } = this.state.searchForm
        if (origins || username || age) {
            axios.get(`${process.env.REACT_APP_URI}/api/searchForm`, { params: { dataSearchForm: this.state.searchForm } })
                .then(response => {
                    if (response.data !== "No data") {
                        SearchBarStore.addString(response.data)
                        this.props.history.push("/")
                    }
                })
        }
    }

    handleChangeSearchForm = e => {
        const { name, value } = e.target
        const { searchForm } = this.state
        this.setState({
            searchForm: {
                ...searchForm,
                [name]: value
            }
        })
    }

    render() {
        return <div className={`${this.props.showSearch ? "show" : ""}`} ref={this.childRef}>
            <div className={`search-mobile form-group ${this.props.showSearch ? "show" : ""}`} ref={this.childRef2}>
                <h3 className="title-search-home">Find your perfect fit</h3>
                <form ref={input => this.searchInput = input} onSubmit={e => this.searchForm(e)}>
                    <div className="form-row">
                        <div className="col">
                            <select name="origins" id="origins-mobile" className="form-control" value={this.state.searchForm.origins} onChange={this.handleChangeSearchForm}>
                                <option value="" disabled hidden>Origins</option>
                                <option value="">- Doesn't matter -</option>
                                <option value="asian">Asian</option>
                                <option value="russian">Russian</option>
                                <option value="european">European</option>
                                <option value="latina">Latina</option>
                                <option value="indian">Indian</option>
                                <option value="metis">Metis</option>
                                <option value="arabic">Arabic</option>
                            </select>
                        </div>
                        <div className="col">
                            <input type="text" name="username" className="form-control" placeholder="Username" value={this.state.searchForm.name} onChange={this.handleChangeSearchForm} />
                        </div>
                    </div>
                    <br />
                    <div className="form-row age-row-search">
                        <div className="col">
                            <input className="form-control age-input-search" name="age" type="number" min="18" max="99" id="age-mobile" placeholder="Age" value={this.state.searchForm.age} onChange={this.handleChangeSearchForm} />
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary intimy-rectangle-button search-button">Search</button>
                </form>
            </div>
        </div>
    }
}

export default withRouter(observer(SearchFormMobile))