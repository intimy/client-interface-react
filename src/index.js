//React
import React from 'react'
import ReactDOM from 'react-dom'
//Css
import './themes/dist/index.css'
import './themes/font-awesome-4.7.0/css/font-awesome.min.css'
//Components
import App from './components/App'
import { CookiesProvider } from 'react-cookie'
//Router
import { BrowserRouter as Router, Route} from 'react-router-dom'
import ScrollToTop from './components/scrollTop'

ReactDOM.render(<Router><ScrollToTop><CookiesProvider><App /></CookiesProvider></ScrollToTop></Router>, document.getElementById('root'))